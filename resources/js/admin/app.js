
require('bootstrap');
require('jquery-datepicker');
require('perfect-scrollbar');
require('moment-mini');
require('rickshaw');
require('jquery.flot');
require('jquery-sparkline');
require('echarts');
require('select2');
require('gmaps');
require('./bracket');
require('./map.shiftworker');
require('./ResizeSensor');
require('./dashboard');
