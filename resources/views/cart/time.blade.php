<fieldset id="timeslot_section">
    <div class="col-lg-12 p-md-0">
        <div class="row">
            <div class="col-md-12">
                <h4 id="head_title_plumbing"
                    class="font-weight-bolder header text-brown title mt-3">What is your
                    preferred timing?</h4>
            </div>
        </div>


        <div class="radio-toolbar-round">
            <div class="row">
                @php $today = Carbon\Carbon::today() @endphp
                @php $day1 =Carbon\Carbon::today()->addDays(1) @endphp
                @php $day2 = Carbon\Carbon::today()->addDays(2) @endphp
                @php $day3 = Carbon\Carbon::today()->addDays(3) @endphp

                @if (time() < strtotime('18:00:00'))
                    <div class="col-lg-3 col-md-3 col-sm-3"
                         onclick="clickday('{{ $today}}','{{$today->isoFormat('dddd')}}')">
                        <input type="radio" id="TODAY" name="radioFruit3" checked>
                        <label for="TODAY">TODAY</label>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <input type="radio" id="Day1" name="radioFruit3" onclick="clickday('{{$day1}}','{{$day1->isoFormat('dddd')}}')">
                        <label
                            for="Day1">{{ strtoupper( $day1->isoFormat('ddd')).' '. $day1->day }}</label>
                    </div>
                @else
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <input type="radio" checked id="Day1" name="radioFruit3" onclick="clickday('{{$day1}}','{{$day1->isoFormat('dddd')}}')">
                        <label
                            for="Day1">{{ strtoupper( $day1->isoFormat('ddd')).' '. $day1->day }}</label>
                    </div>

                @endif




                <div class="col-lg-3 col-md-3 col-sm-3">
                    <input type="radio" id="Day2" name="radioFruit3"
                           onclick="clickday('{{$day2}}','{{$day2->isoFormat('dddd')}}')">
                    <label
                        for="Day2">{{ strtoupper( $day2->isoFormat('ddd')).' '. $day2->day }}</label>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3">
                    <input type="radio" id="Day3" name="radioFruit3"
                           onclick="clickday('{{$day3}}','{{$day3->isoFormat('dddd')}}')">
                    <label
                        for="Day3">{{ strtoupper( $day3->isoFormat('ddd')).' '. $day3->day }}</label>
                </div>
            </div>
        </div>
        <br>


        <div class="radio-toolbar-time">
            <div class="row" id="timeslot">

            </div>
        </div>
    </div>
    <br>


    <div class="row">
        <div class="col-md-12">
            <h4 id="head_title_plumbing"
                class="font-weight-bolder header text-brown title mt-3">Instructions for
                technician</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <input class="form-control input-border" name="instruction"
                       placeholder="Please call my door from the security gate1."
                       required>
            </div>
        </div>
    </div>


    <input type="button" name="previous" class="previous btn-brown_3nd mt-3"
           value="BACK"/>
    <input onclick="submitForm()" type="button" name="make_payment" class="next btn-brown_2nd mt-3"
           value="NEXT"/>
</fieldset>
