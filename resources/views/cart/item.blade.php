<fieldset id="item_section">
    <div class="col-lg-12 p-md-0">
        <div class="row">
            <div class="col-md-12">
                <h4 id="head_title_plumbing"
                    class="font-weight-bolder header text-brown title mt-3">Do you have
                    a material? </h4>
            </div>
        </div>


        <div class="radio-large_yes">
            <div class="row">
                <div onclick="haveMaterial(false)" class="col-lg-6 col-md-6 col-sm-6">
                    <input type="radio" name="SelectMaterial">
                    <label class="lbl-item-req" id="item_no">Yes</label>
                </div>
                <div onclick="haveMaterial(true)" class="col-lg-6 col-md-6 col-sm-6">
                    <input type="radio" name="SelectMaterial">
                    <label class="lbl-item-req selected" id="item_yes">NO</label>
                </div>


            </div>
        </div>
    </div>


    <div id="itemSelection">
        <div class="col-lg-12 p-md-0">


            <div class="radio-toolbar-round-3nd">
                <div class="row">
                    <div class="col-md-12">
                        <h4 id="head_title_plumbing"
                            class="font-weight-bolder header text-brown title mt-3">Choose one</h4>
                    </div>
                </div>
                <div class="row" id="taprownew">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <input type="radio" id="tap" name="ChooseItems"
                               checked="checked">
                        <label for="tap">
                            <img style="width: 50px;height: 50px"
                                 src="{{asset('frontEnd/images/tapfor.png')}}">
                        </label>
                        <p class="text_Data_choose">Basic</p>
                        <p class="text_Data_choose">Basic $25.00</p>
                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <input type="radio" id="tap2" name="ChooseItems">
                        <label for="tap2"><img style="width: 50px;height: 50px"
                                               src="{{asset('frontEnd/images/tapfor.png')}}"></label>
                        <p class="text_Data_choose">Basic</p>
                        <p class="text_Data_choose">Basic $25.00</p>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <input type="radio" id="tap3" name="ChooseItems">
                        <label for="tap3"><img style="width: 50px;height: 50px"
                                               src="{{asset('frontEnd/images/tapfor.png')}}"></label>
                        <p class="text_Data_choose">Basic</p>
                        <p class="text_Data_choose">Basic $25.00</p>
                    </div>
                </div>
            </div>
            <br>
        </div>
    </div>
    <input type="button" name="previous" class="previous btn-brown_3nd mt-3"
           value="BACK"/>
    <input type="button" name="make_payment" class="next btn-brown_2nd mt-3"
           value="NEXT"/>
</fieldset>
