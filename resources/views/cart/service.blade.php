<fieldset id="service_section">
    <div class="col-lg-12 p-md-0">
        <div class="row">
            <div class="col-md-12">
                <h4 id="head_title_plumbing"
                    class="font-weight-bolder header text-brown title mt-3">What needs
                    to be done?</h4>
            </div>
        </div>
    </div>

    <div class="row" id="serviceRow">
    </div>


    <input type="button" name="previous" class="previous btn-brown_3nd mt-3"
           value="BACK"/>
    <input type="button" id="categorynext" name="make_payment"
           class="next btn-brown_2nd mt-3" value="NEXT"/>
</fieldset>
