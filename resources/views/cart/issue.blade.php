<fieldset id="issue_section">
    <div class="col-lg-12 p-md-0">
        <div class="row">
            <div class="col-md-12">
                <h4 id="head_title_plumbing"
                    class="font-weight-bolder header text-brown title mt-3">Select
                    issue</h4>
            </div>
        </div>

        <div class="radio-toolbar">
            <div class="row" id="issuerow">
                <div class="col-lg-3 col-md-12 col-sm-12">
                    <input type="radio" id="Leaking" name="SelectIssue" checked>
                    <label for="Leaking">Leaking</label>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12">
                    <input type="radio" id="Clogged" name="SelectIssue">
                    <label for="Clogged">Clogged</label>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12">
                    <input type="radio" id="Broken" name="SelectIssue">
                    <label for="Broken">Broken</label>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12">
                    <input type="radio" id="Rusty" name="SelectIssue">
                    <label for="Rusty">Rusty</label>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12">
                    <input type="radio" id="LessWaterFlow" name="SelectIssue">
                    <label for="LessWaterFlow">Less water flow</label>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12">
                    <input type="radio" id="TooOld" name="SelectIssue">
                    <label for="TooOld">Too old</label>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12">
                    <input type="radio" id="NewTap" name="SelectIssue">
                    <label for="NewTap">New tap</label>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12">
                    <input type="radio" id="Other" name="SelectIssue">
                    <label for="Other">Other</label>
                </div>
            </div>
        </div>
    </div>
    <input type="button" name="previous" class="previous btn-brown_3nd mt-3"
           value="BACK"/>
    <input type="button" name="make_payment" class="next btn-brown_2nd mt-3"
           value="NEXT"/>
</fieldset>
