<div class="col-md-3 bg-brown">
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10">
            <p class="text-black">Current Request</p>
        </div>
{{--        <div class="col-lg-2 col-md-2 col-sm-2">--}}
{{--            <i class="fa fa-edit text-brown" style="margin-top: 18px;"></i>--}}
{{--            <p class="right_panel_edit">Edit</p>--}}
{{--        </div>--}}
    </div>
    <div class="bg-white current-inner-box">
        <div class="row m-0">
            <div class="col-lg-2 col-md-2 col-sm-2 p-0">
                <i class="fa fa-check-circle text-brown fa-3x" style="margin-top: 8px;"></i>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 mb-3">
                <p id="category_t" class="text-nowrap service-type mx-auto my-2"></p>
                <p id="service_t" class="text-nowrap right_panel_text"></p>
                <p id="item_t" class="text-nowrap right_panel_text"></p>
            </div>
        </div>
        <hr class="class-1 ">

        <div class="col-md-12 mt-4">
            <div class="row mt-2">
                <div class="col-md-9 text-black_1">Inspection <span class="text-brown">[waived]</span></div>
                <div class="row  text-black_1 text-right text-nowrap">$<p id="inspection_t">{{!empty($cart_data)?number_format($cart_data->inspection_charge,2):'00.00'}}</p></div>
            </div>

            <div class="row mt-2">
                <div class="col-md-9 text-black_1"><p class="text-nowrap" id="service_tt">{{!empty($cart_data->service)?$cart_data->service->name:'Service'}}</p></div>
                <div class="row  text-black_1 text-nowrap text-right">$<p id="service_price_t">{{!empty($cart_data)?number_format($cart_data->service_charge,2):'00.00'}}</p></div>
            </div>

            <div class="row mt-2">
                <div class="col-md-9 text-black_1">Material <span class="text-brown"><p id="item_name_t">{{!empty($cart_data->item)?$cart_data->item->item_id:''}}</p></span></div>
                <div class="row text-black_1 text-right text-nowrap">$<p id="item_price_t">{{!empty($cart_data)?number_format($cart_data->material_charge,2):'00.00'}}</p></div>
            </div>

            <div class="row mt-2">
                <div class="col-md-9 text-brown_1 font-weight-bold">Estd. Total **</div>
                <div class="row  text-brown_1 text-right text-nowrap font-weight-bold">$<p id="lbl_est">{{!empty($cart_data)?number_format($cart_data->estimate,2):'00.00'}}</p></div>
            </div>

        </div>

        <hr>

        <div class="">
            <p class="text-black_2">
                ** Your technician will fully inspect your area of concern and update you on recommended
                service should there be anything different from your original service request.
            <p>
            <p class="text-black_2">
                The inspection is free if you move forward with your original service request or with our
                recommended service.
            </p>
            <p class="text-black_2">
                You are also free to decline either service offered but a fee of $25.00 will be charged for
                the inspection.
            </p>


{{--            <div class="row">--}}
{{--                <div class="col-lg-3 col-md-3 col-sm-3 text-right p-0">--}}
{{--                    <label class="containerCheckbox_data">--}}
{{--                        <input type="checkbox">--}}
{{--                        <span class="checkmarkCheckbox_data"></span>--}}
{{--                    </label>--}}
{{--                </div>--}}
{{--                <div class="col-lg-9 col-md-9 col-sm-9">--}}
{{--                    <p class="text-black_2">--}}
{{--                        I accept the above charges and services--}}
{{--                        from Brownwires.--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>

{{--    <div class="bg-white current-inner-box" style="margin-top: 3%;">--}}
{{--        <div class="row m-0">--}}
{{--            <div class="col-lg-2 col-md-2 col-sm-2">--}}
{{--                <img style="width: 30px;height: 30px"--}}
{{--                     src="{{asset('frontEnd/icon/icons8-calendar-100.png')}}">--}}
{{--            </div>--}}
{{--            <div class="col-lg-8 col-md-8 col-sm-8">--}}
{{--                <p class="service-type">Date & Time</p>--}}
{{--            </div>--}}
{{--            <div class="col-lg-2 col-md-2 col-sm-2">--}}
{{--                <i class="fa fa-edit text-brown"></i>--}}
{{--                <p class="right_panel_edit">Edit</p>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row m-0">--}}
{{--            <div class="col-lg-12 col-md-12 col-sm-12" >--}}
{{--                <p class="right_panel_text_3" id="day_t">Thursday, 01st March 2020--}}
{{--                    <span class="right_panel_text_2" id="time_slot_t">4PM-6PM</span>--}}
{{--                </p>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <hr class="class-1">--}}
{{--        <p class="text-black_3">--}}
{{--            <b>Instructions for Technician</b><br>--}}
{{--            <span style="padding-left: 30px">* Please call my door from the security gate.</span>--}}
{{--        </p>--}}
{{--    </div>--}}

{{--    <div class="bg-white current-inner-box" style="margin-top: 3%;">--}}
{{--        <div class="row m-0">--}}
{{--            <div class="col-lg-2 col-md-2 col-sm-2">--}}
{{--                <img style="width: 30px;height: 30px"--}}
{{--                     src="{{asset('frontEnd/icon/icons8-marker-100.png')}}">--}}
{{--            </div>--}}
{{--            <div class="col-lg-8 col-md-8 col-sm-8">--}}
{{--                <p class="service-type">Location</p>--}}
{{--            </div>--}}
{{--            <div class="col-lg-2 col-md-2 col-sm-2">--}}
{{--                <i class="fa fa-edit text-brown"></i>--}}
{{--                <p class="right_panel_edit">Edit</p>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row m-0">--}}
{{--            <div class="col-lg-12 col-md-12 col-sm-12">--}}
{{--                <p class="right_panel_text_3">24 Eunos Crescent,#05-3025,SG &nbsp; <span>400024</span></p>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <hr class="class-1">--}}
{{--        <p class="text-black_3">--}}
{{--            <b>Instructions for Technician</b><br>--}}
{{--            <span style="padding-left: 30px">* Please call my door from the security gate.</span>--}}
{{--        </p>--}}
{{--    </div>--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-12">--}}
{{--            <center>--}}
{{--                <button class="btn btn-brown mt-3">CONFIRM</button>--}}
{{--            </center>--}}
{{--        </div>--}}
{{--    </div>--}}
</div>
