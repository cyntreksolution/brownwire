<fieldset id="category_section">
    <div class="col-lg-12 p-md-0">
        <div class="row">
            <div class="col-md-12">
                <h4 id="head_title_plumbing"
                    class="font-weight-bolder header text-brown title mt-3">Select
                    service category</h4>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($data as $key=>$cat)
            <div onclick="CategoryClick({{$cat->id}})"
                 class="col-lg-3 col-md-3 col-sm-3 mt-4">
                <div class="card section {{($selected==$cat->id) ? 'section2':''}}">
                    <p class="containerData">
                        <input type="radio" id="serviceCategory" class="serviceCategory"
                               name="serviceCategory" value="{{$cat->id}}" required>
                        <span class="checkmark"><img
                                src="{{asset('upload/frontEnd/icon/'.$cat->category_image)}}"
                                class="iconData"></span>
                    </p>
                    <p class="text_Data">{{$cat->category_name}}</p>
                </div>
            </div>
        @endforeach
    </div>

    <input type="button" id="categorynext1" name="next" class="next btn-brown_2nd mt-3" value="NEXT"/>
</fieldset>
<!-- END SERVICE CATEGORY PART  -->
