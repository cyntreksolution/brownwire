<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <title>Brownwire.com</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="http://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/bootstrap/css/bootstrap.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/font-awesome/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('frontEnd/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css')}}"
          integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css')}}"
          integrity="sha256-kksNxjDRxd/5+jGurZUJd1sdR2v+ClrCl3svESBaJqw=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        .section {
            background-color: white;
            color: black;
        }

        .section2 {
            background-color: #8A4B20;
            color: black;
        }

        .section2 .text_Data {
            color: white;
        }

        .section2 .text_Data_large {
            color: white;
        }

        .selected {
            background-color: #8A4B20;
        }
    </style>
</head>


<body class="pane scroller">
<div id="app">


    <!-- HEADER-MENU -->
@include('inc.topbar')
<!--/ HEADER-MENU-->

    <main style="padding-bottom: 6% !important;">
        <div class="row m-0">
            <div class="col-md-9 mt-5" style="padding-top: 2% !important;margin-top: 7% !important;">
                <div class="row" id="taprow">
                    @include('cart.left_sidebar')

                    <div class="col-md-8">
                        <div class="row">
                        {!! Form::open(['route' => 'address.submit', 'method' => 'post','id'=>'msform','style'=>'width: 100% !important;']) !!}
                        <!-- START- LOCATION SERVICES  -->
                            <fieldset id="plumbing2Page">
                                <!-- progressbar -->

{{--                                <div id="progressbar">--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-lg-12">--}}
{{--                                            <li class="active" id="Choose"><strong>Choose</strong></li>--}}
{{--                                            <li id="add_info"><strong>Add info</strong></li>--}}
{{--                                            <li id="Shedule"><strong>Schedule</strong></li>--}}
{{--                                            <li id="Confirm" style="color:lightgrey;"><strong>Confirm</strong></li>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                                <input type="hidden" name="cart_id" value="{{$cart}}" />

                                <div class="row">
                                    <div class="col-lg-12 p-md-0">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h4 id="head_title_plumbing"
                                                    class="font-weight-bolder header text-brown title mt-3">Type in
                                                    your postal code to add location of service</h4>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-12 p-md-0">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input style="font-size: 22px"
                                                           class="form-control input-border text-black_1 font-weight-bold"
                                                           name="postal_code" placeholder="Postal Code"
                                                          value="{{!empty($address->postal_code)?$address->postal_code:null}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input class="form-control input-border" name="address1"
                                                           placeholder="400024 24 Eunos Crescent"
                                                           value="{{!empty($address->address_1)?$address->address_1:null}}"  required>
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control input-border" name="address2"
                                                           value="{{!empty($address->address_2)?$address->address_2:null}}" placeholder="#05-3025" required>
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control input-border" name="address3"
                                                           value="{{!empty($address->address_3)?$address->address_3:null}}"  placeholder="Singapore" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 id="head_title_plumbing"
                                            class="font-weight-bolder header text-brown title mt-3">Directions for
                                            technician</h4>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control input-border" name="directions_for_technician"
                                                   placeholder="Please call my door from the security gate." required>
                                        </div>
                                    </div>
                                </div>


                                <input type="button" name="previous" class="previous btn-brown_3nd mt-3"
                                       value="BACK"/>
                                <input onclick="submitForm()" type="button" name="NEXT" class="btn-brown_2nd mt-3"
                                       value="PROCEED">
                            </fieldset>



                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>


            @include('cart.right_sidebar')
        </div>
    </main>


    <!-- FOTTER-MENU -->
@include('inc.fotter')
<!--/ FOTTER-MENU -->

    <!--/////CONTACT US MENU -->
</div>
<script src="{{asset('frontEnd/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/popper/popper.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js')}}"
        integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
<script src="{{asset('frontEnd/js/custom.js')}}"></script>

<script>
    $(document).ready(function () {

        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;

        $(".next").click(function () {

            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({'opacity': opacity});
                },
                duration: 600
            });
        });

        $(".previous").click(function () {

            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();

            //Remove class active
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();

            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    previous_fs.css({'opacity': opacity});
                },
                duration: 600
            });
        });

        $('.radio-group .radio').click(function () {
            $(this).parent().find('.radio').removeClass('selected');
            $(this).addClass('selected');
        });

        $(".submit").click(function () {
            return false;
        })
    });

    $(function () {
        $('.section').click(function () {
            $('.section2').removeClass('section2');
            $(this).addClass('section2');
        });
    });


</script>

<script>


    var category = 0;

    function CategoryClick(category_id) {
        $('#category_id').val(category_id);
        category = category_id;
        $.ajax({
            type: "GET",
            url: '/cart/' + category_id,
            success: function (response) {
                $("#serviceRow").empty();
                $.each(response, function (key, value) {
                    $("#serviceRow").append('<div onclick="clickme(' + value.service_id + ')" id="ser_' + value.service_id + '" class="col-lg-4 col-md-4 col-sm-4 ml-3">\n' +
                        '                                            <div class="card_large section service">\n' +
                        '                                                <p class="containerData_large">\n' +
                        '                                                    <input type="radio"  id="INSTALLATION" name="NeedTo_beDone">\n' +
                        '                                                    <span class="checkmark_large"><img src="/frontEnd/icon/icons8-support-100.png" class="iconData_large"></span>\n' +
                        '                                                </p>\n' +
                        '                                                <p class="text_Data_large">' + value.service.name + '</p>\n' +
                        '                                            </div>\n' +
                        '                                        </div>');
                });
            }
        });
    }

    // });

    var service = 0;

    function clickme(service_id) {
        $('#service_id').val(service_id);
        service = service_id;
        $('.service').removeClass('selected');

        let xxx = '#ser_' + service_id;
        $(xxx).children().addClass('selected');


        $.ajax({
            type: "GET",
            url: '/plumbing4/' + service_id + '/' + category,
            success: function (response) {
                $("#itemtyperow").empty();
                $.each(response, function (key, value) {
                    $("#itemtyperow").append('  <div onclick="clickitem(' + value.id + ',' + value.itemtype_id + ')" id="ite' + value.id + '" class="col-lg-3 col-md-12 col-sm-12" >\n' +
                        '                           <input type="radio" id="Leaking_' + value.id + '" name="needFix" >\n' +
                        '                                                    <p for="Leaking_' + value.id + '">' + value.itemtype.itemtypeid + '</p>\n' +
                        '                                                </div>');
                });
            }
        });

    }

    var item = 0;

    function clickitem(id, itemTypeId) {
        $('#itemtype_id').val(itemTypeId);

        item = id;

        $('.item').removeClass('selected');

        let xxx = '#ite' + id;
        $(xxx).children().addClass('selected');
        $.ajax({
            type: "GET",
            url: '/plumbing5/' + id,
            success: function (response) {
                $("#issuerow").empty();
                $.each(response, function (key, value) {

                    $("#issuerow").append(' <div onclick="clickissue(' + value.issue.id + ')"  class="col-lg-3 col-md-12 col-sm-12">\n' +
                        '                                                    <input type="radio" id="Leaking" name="SelectIssue" checked>\n' +
                        '                                                    <p for="Leaking">' + value.issue.name + '</p>\n' +
                        '                                                </div>');
                });
            }
        });
        $.ajax({
            type: "GET",
            url: '/plumbing6/' + itemTypeId,
            success: function (response) {
                $("#taprownew").empty();
                $.each(response, function (key, value) {
                    let price = parseFloat(value.price);
                    $("#taprownew").append('<div onclick="clickitemcheck(' + value.id + ')" class="col-lg-3 col-md-3 col-sm-3">\n' +
                        '                                                    <input type="radio"  name="ChooseItems" checked="checked">\n' +
                        '                                                    <label >\n' +
                        '                                                        <img style="width: 50px;height: 50px" src="/frontEnd/images/tapfor.png">\n' +
                        '                                                    </label>\n' +
                        '                                                    <p class="text_Data_choose">' + value.item_id + '</p>\n' +
                        '                                                    <p class="text_Data_choose">' + price + '</p>\n' +
                        '                                                </div>');
                });
            }
        });
    }


    function clickissue(issue_id) {
        $('#issue_id').val(issue_id);
    }

    function clickitemcheck(item1_id) {
        $('#item_id').val(item1_id);
    }

    function clicktimeslot(timeslot, dateString) {
        $('#timeslot_id').val(timeslot);
    }

    function clickday(dy, dateString) {
        $('#selectedDate').val(dy);
        $.ajax({
            type: "GET",
            url: '/get/timeslot/' + dateString,
            success: function (response) {
                $("#timeslot").empty();
                $.each(response, function (key, value) {
                    $("#timeslot").append('<div class="col-md-4" onclick="clicktimeslot(' + value.id + ')">\n' +
                        '                        <input type="radio" name="radioFruit4" >\n' +
                        '                        <label>' + value.start_time + ' - ' + value.end_time + '</label>\n' +
                        '                    </div>');
                });
            }
        });
    }

    function haveMaterial(status) {
        if (status) {
            $('#itemSelection').removeClass('d-none');
        } else {
            $('#itemSelection').addClass('d-none');
        }

    }

    function submitForm() {
        $('#msform').submit();
    }


</script>

</body>
</html>
