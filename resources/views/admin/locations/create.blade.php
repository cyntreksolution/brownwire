@extends('layouts.backend.master')
@section('content')

        <div class="content-wrapper">
        <!-- Default form contact -->

        @if(count($errors) >0)
        <div class="alert alert-danger">
        <ul>
        @foreach($errors->all() as $error)

        <li>{{$error}}</li>
        @endforeach

        </ul>
        </div>
        @endif
        @if(\Session::has('success'))
    <div class="alert alert-success">
    <p>{{\Session::get('success')}}</p>
    </div>
        @endif
                    <div class="card">

<form class="text-center border border-light p-5" action="{{route('location.store')}}" method="post" id="categoryAdd">

     {{csrf_field()}}
   <div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Location Id</label>
            <input type="text" name="location_id" id="defaultContactFormName" class="form-control" placeholder="location Id">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Location Name</label>
             <input type="text" name="location_name" id="defaultContactFormEmail" class="form-control mb-4" placeholder="location Name">
        </div>
    </div>
</div>
    <!-- Copy -->

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Is Active</label>
            <select class="browser-default custom-select mb-4" name="is_active">
        <option value="" selected="" disabled>Choose option</option>
        <option value="1">Active</option>
        <option value="2">Deactive</option>
    </select>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Space</label>
            <select class="browser-default custom-select mb-4" name="space_id">
        <option value="" selected="" disabled>Choose option</option>
          @foreach ($users as $key => $spaces)
        <option value="{{$spaces->id}}">{{$spaces->name}}</option>
           @endforeach
    </select>
        </div>
    </div>

</div>
    <!-- Send button -->
    <div class="col-md-6">
    <button  class="btn btn-info btn-block" type="submit">Save</button>
</div>
</form>
<!-- Default form contact -->
        </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">


@endsection
@section('script')
<script>
        $("#categoryAdd").validate({
            rules: {
                location_id: {
                    required: true,
                    // minlength: 5
                },
                location_name: {
                    required: true,
                    // min: 0,
                },
                 is_active: {
                    required: true,
                    // min: 0,
                },
                 space_id: {
                    required: true,
                    // min: 0,
                },
            },
            messages: {
                location_id: {
                    required: "Please enter a Location Id",
                    // minlength: "Item name must consist of at least 5 characters"
                },
                location_name: {
                    required: "Please enter the location name",
                    // min: "Price must be greater than 0"
                }
            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
    </script>
<script src="{{ asset('admin/js/data-table.js')}}"></script>
@endsection

