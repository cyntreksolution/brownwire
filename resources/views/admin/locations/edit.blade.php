@extends('layouts.backend.master')
@section('content')

        <div class="content-wrapper">
        <!-- Default form contact -->

           <div class="card-body">
 {!! Form::model($location, ['route' => ['location.update', $location->id],'id'=>'createForm', 'method' => 'PATCH']) !!}


     {{csrf_field()}}
   <div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Location Id</label>
            <input type="text" name="location_id" id="defaultContactFormName" class="form-control" placeholder="Category Id" value="{{$location->location_id}}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Location Name Name</label>
            {!! Form::text('location_name', null, ['class' => 'form-control']) !!}
           <!--   <input type="text" name="category_name" id="defaultContactFormEmail" class="form-control mb-4" placeholder="Category Name" value="{{$location->location_name}}"> -->
        </div>
    </div>
</div>
    <!-- Copy -->

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Is Active</label>
            <select class="browser-default custom-select mb-4" name="is_active">
        <option value="" disabled>Choose option</option>
        <option value="1" {{ $location->is_active == 1 ? 'selected' : '' }}>Active</option>
        <option value="2" {{ $location->is_active == 2 ? 'selected' : '' }}>Deactive</option>
    </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Space</label>
            {!! Form::select('space_id', $spaces,null, ['class' => 'form-control']) !!}
        </div>
    </div>

</div>
    <!-- Send button -->
    <div class="col-md-6">
    <input  class="btn btn-info btn-block" type="submit"></button>
</div>
 {!! Form::close() !!}
<!-- </form> -->
<!-- Default form contact -->
        </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">


@endsection
@section('script')
<script src="{{ asset('admin/js/data-table.js')}}"></script>

<script>
        $("#createForm").validate({
            rules: {
                location_id: {
                    required: true,
                    // minlength: 5
                },
                location_name: {
                    required: true,
                    // minlength: 5
                },

            },
            messages: {
                name: {
                    required: "Please enter a name",
                    // minlength: "Item name must consist of at least 5 characters"
                },

            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
    </script>
<script src="{{ asset('admin/js/data-table.js')}}"></script>

@endsection

