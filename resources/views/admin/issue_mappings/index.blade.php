@extends('layouts.backend.master')
@section('content')
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Issue Mapping
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home </a></li>
                        <li class="breadcrumb-item active" aria-current="page">Issue Mapping</li>
                    </ol>
                </nav>
            </div>
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">Issue Mapping Details</h4>
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item Mapping name</th>
                                        <th>Issue Name</th>

                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($data as $key =>$issue_mapping)

                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td class="text-nowrap">{{$issue_mapping->item_mapping->category->category_name}} ->
                                                {{$issue_mapping->item_mapping->service->name}} -> {{$issue_mapping->item_mapping->itemtype->itemtypeid}}
                                            </td>

                                            <td>{{!empty($issue_mapping->issue)?$issue_mapping->issue->name:'SKIP'}}</td>


                                            <td>

                                                <a href="" data-id="{{$issue_mapping->id}}"
                                                   class="button delete-confirm">
                                                    <i style="color: #e52d27;"
                                                       class="remove ml-2 fa fa-times-circle"> </i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <a href="{{ route('issue_mapping.create')}}">
            <div id="settings-trigger"><i class="fas fa-plus-circle fa-10x"></i></div>
        </a>
        @endsection
        @section('script')
             <script>
                $('.delete-confirm').on('click', function (e) {
                    event.preventDefault();
                    var id = $(this).data('id');
                    const url = $(this).attr('href');
                    // alert(id);
                    swal({
                        title: 'Are you sure?',
                        text: 'This record and it`s details will be permanently deleted!',
                        icon: 'warning',
                        buttons: ["Cancel", "Yes!"],
                    }).then(function (value) {
                        if (value) {
                            // window.location.href = url;
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            $.ajax({
                                type: "delete",
                                url: '/admin/issue_mapping/' + id,
                                data: {
                                    id: id
                                },
                                success: function (success) {
                                    if (success='true') {
                                        swal(
                                            'Deleted!',
                                            'Your record has been deleted.',
                                            'success'
                                        );
                                        location.reload();
                                    } else {
                                        swal(
                                            'Sorry!',
                                            'Your record has dependencies.',
                                            'warning'
                                        );
                                    }
                                }
                            });
                        }
                    });
                });
            </script>
@endsection

