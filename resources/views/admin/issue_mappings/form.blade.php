<div class="col-md-6">
    <div class="form-check form-group">
        <label>Item Mapping</label>
        <select class="form-control" name="item_mapping_id">
            <option>Select Option</option>
            @foreach($item_mapping as $item)
                <option value="{{$item->id}}">{{$item->category->category_name.' -> '.' '.$item->service->name.' -> '.' '.$item->itemtype->itemtypeid}}</option>
            @endforeach
        </select>

    </div>
</div>

<div class="col-md-6">
    <div class="form-check form-group">
        <label class="">Issue</label>
        {!! Form::select('issue_id', $issue,null, ['class' => 'form-control']) !!}
        <i class="input-helper"></i>
    </div>
</div>
