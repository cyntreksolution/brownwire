@extends('layouts.backend.master')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
               Configurations
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('category.index')}}">Categories</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">
                {!! Form::open(['route' => 'app.config', 'method' => 'post']) !!}

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Contact Number</label>
                            <input type="text" name="contact" class="form-control"
                                    value="{{$data->contact}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email"
                                   class="form-control mb-4"
                                   value="{{$data->email}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text" name="description"
                                   class="form-control mb-4"
                                   value="{{$data->description}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Home URL</label>
                            <input type="text" name="home_url"
                                   class="form-control mb-4"
                                   value="{{$data->home_url}}">
                        </div>
                    </div>
                </div>

                <!-- Send button -->
                <div class="col-md-6">
                    <input class="btn btn-primary" type="submit" value="Save">
                </div>
            {!! Form::close() !!}
            <!-- </form> -->
                <!-- Default form contact -->
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $("#categoryAdd").validate({
            rules: {
                category_id: {
                    required: true,
                },
                category_name: {
                    required: true,
                },
                is_active: {
                    required: true,
                },
                category_image: {
                    required: true,
                },
            },
            messages: {
                category_id: {
                    required: "Please enter a category Id",
                },
                category_name: {
                    required: "Please enter the category name",
                }
            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
    </script>
@endsection

