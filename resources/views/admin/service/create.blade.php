@extends('layouts.backend.master')

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Service Create
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('services.index')}}">Services</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">
                {!! Form::open(['route' => 'services.store', 'method' => 'post','id'=>'createForm','files'=>true ]) !!}
                @include('admin.service.form',['edit'=>false])
                <input class="btn btn-primary" type="submit" value="Save Service">
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $("#createForm").validate({
            rules: {
                name: {
                    required: true,
                },
                service_id: {
                    required: true,
                },
                inspection_charge: {
                    required: true,
                    money: true,
                },
                service_image: {
                    required: true,
                },
            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger');
                $(element).addClass('form-control-danger');
            }
        });
    </script>
@endsection
