<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Service ID</label>
            {!! Form::text('service_id', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>Name</label>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
    </div>

</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Inspection Charge</label>
            {!! Form::number('inspection_charge', null, ['class' => 'form-control','step'=>'00.01']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="row">
            <div class="col-md-8">
                <div class="form-check form-group">
                    <label>Image (100px * 100px)</label>
                    {!! Form::file('service_image',['class' => 'form-control','accept'=>'jpeg|jpg|png']) !!}
                </div>
            </div>
            <div class="col-md-4">
                @if ($edit)
                    <img id="service_image" width="100px" src="{{asset('frontEnd/icon/'.$service->service_image)}}" alt=""/>
                @endif
            </div>
        </div>

    </div>
</div>

<div class="row">

    <div class="col-md-6">
        <div class="form-check form-group">
            <label class="form-check-label">
                {!! Form::checkbox('is_active', '1', null,  ['id' => 'is_active','class'=>'form-check-input','checked']) !!}
                Enabled
                <i class="input-helper"></i></label>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-check form-group">
            <label class="form-check-label">

                {!! Form::checkbox('is_optional', '1', null,  ['id' => 'is_optional','class'=>'form-check-input']) !!}
                Is Optional
                <i class="input-helper"></i></label>
        </div>
    </div>
</div>
