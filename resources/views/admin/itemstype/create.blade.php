@extends('layouts.backend.master')
@section('content')

    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
               Item Type Create
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('itemtype.index')}}">Item Type</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">

                <form class="border border-light p-5" action="{{route('itemtype.store')}}" method="post"
                      id="spaceAdd">

                    {{csrf_field()}}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ItemTypes Id</label>
                            <input type="text" name="itemtypeid" id="defaultContactFormName" class="form-control"
                                   placeholder="Item Type Id">
                        </div>
                    </div>


                    <div class="col-md-6">
                        <button class="btn btn-primary" type="submit">Save Item Type</button>
                    </div>
                </form>
                <!-- Default form contact -->
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $("#spaceAdd").validate({
            rules: {
                itemtypeid: {
                    required: true,
                    // minlength: 5
                },

            },
            messages: {
                itemtypeid: {
                    required: "Please enter Item Type Id",
                    // minlength: "Item name must consist of at least 5 characters"
                },

            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
    </script>
@endsection
