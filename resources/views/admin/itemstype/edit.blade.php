@extends('layouts.backend.master')
@section('content')

    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Item Type Edit
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('itemtype.index')}}">Item Type</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                </ol>
            </nav>
        </div>
        <!-- Default form contact -->

        <div class="card">
            <div class="card-body">

                {!! Form::model($itemtype, ['route' => ['itemtype.update', $itemtype->id], 'method' => 'PATCH']) !!}

                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Item Type Id</label>
                            <input type="text" name="itemtypeid" id="defaultContactFormName" class="form-control"
                                   placeholder="Item Type Id" value="{{$itemtype->itemtypeid}}">
                        </div>
                    </div>

                </div>
                <!-- Copy -->


                <!-- Send button -->
                <div class="col-md-6">
                    <button class="btn btn-primary" type="submit">Update Item Type</button>
                </div>
            {!! Form::close() !!}
            <!-- Default form contact -->
            </div>
        </div>
    </div>


@endsection
@section('script')
    <script>
        $("#spaceAdd").validate({
            rules: {
                name: {
                    required: true,
                    // minlength: 5
                },

            },
            messages: {
                name: {
                    required: "Please enter a name",
                    // minlength: "Item name must consist of at least 5 characters"
                },

            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
    </script>
@endsection
