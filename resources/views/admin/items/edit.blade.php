@extends('layouts.backend.master')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Item Edit
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('item.index')}}">Items</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">
                {!! Form::model($item, ['route' => ['item.update', $item->id], 'method' => 'PATCH','files'=>true]) !!}
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Item Id</label>
                            {!! Form::text('item_id', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Price</label>
                            {!! Form::text('price', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Item Type</label>
                            {!! Form::select('itemtype_id', $itemstype,null, ['class' => 'form-control']) !!}
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-check form-group">

                            <label>Image(100px * 100px)</label>
                            @if (!empty($item->image))
                                <img id="image" width="100px" src="{{asset('uploads/items/'.$item->image)}}" alt=""/>
                            @endif
                            {!! Form::file('image',['class' => 'form-control']) !!}

                        </div>
                    </div>


                    <!-- Default form contact -->
                </div>
                <input class="btn btn-primary" type="submit" value="Update Item">
                {!! Form::close() !!}
            </div>
        </div>


        @endsection

        @section('script')
            <script>

            </script>
@endsection

