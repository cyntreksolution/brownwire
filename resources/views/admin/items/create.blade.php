@extends('layouts.backend.master')
@section('content')

    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Item Create
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('item.index')}}">Items</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">

                {!! Form::open(['route' => 'item.store', 'method' => 'post','id'=>'createForm','files'=>true ]) !!}

                {{csrf_field()}}
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Item Id</label>
                        {!! Form::text('item_id', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Price</label>
                        {!! Form::number('price', null, ['class' => 'form-control']) !!}
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="form-group">
                        <label>Item Type</label>
                        {!! Form::select('itemtype_id', $itemstype,null, ['class' => 'form-control']) !!}
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="form-check form-group">
                        <label>Image(100px * 100px)</label>
                        {!! Form::file('image',['class' => 'form-control']) !!}
                    </div>
                </div>
                <input class="btn btn-primary" type="submit" value="Save Item">
            {!! Form::close() !!}
            <!-- Default form contact -->
            </div>
        </div>
    </div>


@endsection
@section('script')
    <script>
        $("#createForm").validate({
            rules: {
                item_id: {
                    required: true,
                    // minlength: 5
                },
                price: {
                    required: true,
                    // min: 0,
                },
                itemtype_id: {
                    required: true,
                    // min: 0,
                },
                image: {
                    required: true,
                    // min: 0,
                },
            },
            messages: {
                item_id: {
                    required: "Please enter a Item Id",
                    // minlength: "Item name must consist of at least 5 characters"
                },
                price: {
                    required: "Please enter the Item price",
                    // min: "Price must be greater than 0"
                },
                image: {
                    required: "add image",
                    // min: "Price must be greater than 0"
                }
            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
    </script>
    <script src="{{ asset('admin/js/data-table.js')}}"></script>
@endsection

