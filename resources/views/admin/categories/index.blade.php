@extends('layouts.backend.master')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Categories
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> Categories</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Categories List</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category ID</th>
                                    <th>Category Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($categories as $key => $category)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$category->category_id}}</td>
                                        <td>{{$category->category_name}}</td>
                                        <td>
                                            @if($category->is_active==1)
                                                <button class="btn btn-sm btn-success">Enabled</button>
                                            @else
                                                <button class="btn btn-sm btn-danger">Disabled</button>
                                            @endif
                                        </td>
                                        {{--<td><input class="target" type="button" onclick="myFunction(this)"></td>--}}
                                        <td>
                                            <a href="{{route('category.edit',[$category->id])}}"><i
                                                    class="fas fa-pencil-alt btn-icon-append "> </i></a>
                                            <a href="" data-id="{{$category->id}}" class="button delete-confirm">
                                                <i style="color: #e52d27;"
                                                   class="remove ml-2 fa fa-times-circle"> </i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <a href="{{ route('category.create')}}">
        <div id="settings-trigger"><i class="fas fa-plus-circle fa-10x"></i></div>
    </a>
@endsection
@section('script')
    <script>
        $('.delete-confirm').on('click', function (e) {
            event.preventDefault();
            var id = $(this).data('id');
            const url = $(this).attr('href');
            // alert(id);
            swal({
                title: 'Are you sure?',
                text: 'This record and it`s details will be permanently deleted!',
                icon: 'warning',
                buttons: ["Cancel", "Yes!"],
            }).then(function (value) {
                if (value) {
                    // window.location.href = url;
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "delete",
                        url: '/admin/category/' + id,
                        data: {
                            id: id
                        },
                        success: function (success) {
                            if (success == 'true') {
                                swal(
                                    'Deleted!',
                                    'Your record has been deleted.',
                                    'success'
                                );
                                location.reload();
                            } else {
                                swal(
                                    'Sorry!',
                                    'Your record has dependencies.',
                                    'warning'
                                );
                            }
                        }
                    });
                }
            });
        });
    </script>

@endsection

