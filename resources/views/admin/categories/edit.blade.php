@extends('layouts.backend.master')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Category Edit
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('category.index')}}">Categories</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">
            {!! Form::model($category, ['route' => ['category.update', $category->id],'files'=>true, 'method' => 'PATCH']) !!}
            <!-- <form class="text-center border border-light p-5" action="{{route('category.update',$category->id)}}" method="PATCH"> -->

                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Category Id</label>
                            <input type="text" name="category_id" id="defaultContactFormName" class="form-control"
                                   placeholder="Category Id" value="{{$category->category_id}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Category Name</label>
                            <input type="text" name="category_name" id="defaultContactFormEmail"
                                   class="form-control mb-4" placeholder="Category Name"
                                   value="{{$category->category_name}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-check form-group">

                        <label>Image(512px * 512px)</label>
                        @if (!empty($category->category_image))
                            <img id="category_image" width="100px"
                                 src="{{asset('upload/frontEnd/icon/'.$category->category_image)}}" alt=""/>
                        @endif
                        {!! Form::file('category_image',['class' => 'form-control']) !!}

                    </div>
                </div>

                <!-- Copy -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Status</label>
                            <select class="browser-default custom-select mb-4" name="is_active">
                                <option value="" disabled>Choose option</option>
                                <option value="1" {{ $category->is_active == 1 ? 'selected' : '' }}>Enabled</option>
                                <option value="2" {{ $category->is_active == 2 ? 'selected' : '' }}>Disabled</option>
                            </select>
                        </div>
                    </div>

                </div>
                <!-- Send button -->
                <div class="col-md-6">
                    <input class="btn btn-primary" type="submit" value="Save">
                </div>
            {!! Form::close() !!}
            <!-- </form> -->
                <!-- Default form contact -->
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $("#categoryAdd").validate({
            rules: {
                category_id: {
                    required: true,
                },
                category_name: {
                    required: true,
                },
                is_active: {
                    required: true,
                },
                category_image: {
                    required: true,
                },
            },
            messages: {
                category_id: {
                    required: "Please enter a category Id",
                },
                category_name: {
                    required: "Please enter the category name",
                }
            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
    </script>
@endsection

