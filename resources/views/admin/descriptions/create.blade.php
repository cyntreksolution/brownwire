@extends('layouts.backend.master')

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Description Create
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('services.index')}}">Description</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">
                {!! Form::open(['route' => 'description.store', 'method' => 'post','id'=>'createForm']) !!}
                @include('admin.descriptions.form')
                <input class="btn btn-primary" type="submit" value="Submit">
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $("#createForm").validate({
            rules: {
                description_id: {
                    required: true,
                },
                description_name: {
                    required: true,
                },
                is_active: {
                    required: true,
                },
                
            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger');
                $(element).addClass('form-control-danger');
            }
        });
    </script>
@endsection
