<div class="col-md-6">
    <div class="form-group">
        <label>Description Id</label>
        {!! Form::text('description_id', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label>Description Name</label>
        {!! Form::text('description_name', null, ['class' => 'form-control']) !!}
    </div>
</div>



<div class="col-md-6">
    <div class="form-check form-group">
        <label class="form-check-label">
            {!! Form::checkbox('is_active', '1', null,  ['id' => 'is_active','class'=>'form-check-input']) !!}
            Is Active
            <i class="input-helper"></i></label>
    </div>
</div>


 <div class="col-md-6">
    <div class="form-check form-group">
        <label>Service</label>
            {!! Form::select('service_id', $service,null, ['class' => 'form-control']) !!}
            <i class="input-helper"></i>
    </div>
</div>

<div class="col-md-6">
    <div class="form-check form-group">
        <label class="">Item</label>
            {!! Form::select('item_id', $item,null, ['class' => 'form-control']) !!}
            <i class="input-helper"></i>
    </div>
</div>