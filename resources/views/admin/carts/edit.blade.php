@extends('layouts.backend.master')
@section('content')

        <div class="content-wrapper">
        <!-- Default form contact -->

           <div class="card-body">
 {!! Form::model($cart, ['route' => ['cart.update', $cart->id],'files'=>true, 'method' => 'PATCH']) !!}
<!-- <form class="text-center border border-light p-5" action="{{route('category.update',$category->id)}}" method="PATCH"> -->

     {{csrf_field()}}
   <div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Category Id</label>
            <input type="text" name="category_id" id="defaultContactFormName" class="form-control" placeholder="Category Id" value="{{$category->category_id}}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Category Name</label>
             <input type="text" name="category_name" id="defaultContactFormEmail" class="form-control mb-4" placeholder="Category Name" value="{{$category->category_name}}">
        </div>
    </div>
</div>


    <!-- Copy -->
   
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Is Active</label>
            <select class="browser-default custom-select mb-4" name="is_active">
        <option value="" disabled>Choose option</option>
        <option value="1" {{ $category->is_active == 1 ? 'selected' : '' }}>Active</option>
        <option value="2" {{ $category->is_active == 2 ? 'selected' : '' }}>Deactive</option>
    </select>
        </div>
    </div>
   
</div>
    <!-- Send button -->
    <div class="col-md-6">
    <input  class="btn btn-info btn-block" type="submit"></button>
</div>
 {!! Form::close() !!}
<!-- </form> -->
<!-- Default form contact -->
        </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">


@endsection
@section('script')
<script src="{{ asset('admin/js/data-table.js')}}"></script>
@endsection

