@extends('layouts.backend.master')
@section('content')
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>--}}

    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Orders
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> Users</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Categories Details</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
{{--                                    <th>#</th>--}}
                                    <th>Job Id</th>
                                    <th>User</th>
                                    <th>Category</th>
                                    <th>Service</th>
                                    <th>Item Type</th>
                                    <th>Issue</th>
                                    <th>Item</th>
                                    <th>TimeSlot</th>
                                    <th>Show</th>

                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($carts as $key => $cart)
                                    <tr>
{{--                                        <td>{{++$key}}</td>--}}
                                        <td>{{$cart->job_id}}</td>
                                        <td>{{$cart->user->name}}</td>
                                        <td>{{$cart->category->category_name}}</td>
                                        <td>{{$cart->service->name}}</td>
                                        <td>{{$cart->itemtype->itemtypeid}}</td>
                                        <td>{{!empty($cart->issue)?$cart->issue->name:'-'}}</td>
                                        <td>{{!empty($cart->item)?$cart->item->item_id:'-'}}</td>
                                        <td>{{$cart->timeslot->time_slot_day.' '.$cart->timeslot->start_time.' '.$cart->timeslot->end_time}}</td>
{{--                                        @php $address =$cart->address @endphp--}}
{{--                                        <td>{{$address->address_1.','.$address->address_2.','.$address->address_3.', Singapore, '.$address->postal_code}}</td>--}}
                                        <td><a href="{{route('cart.show',$cart->id)}}"><i class="text-danger fa fa-eye"></i> </a> </td>
                                        <td class="form-control">
                                            <select id="cart_status_{{$cart->id}}"
                                                    onchange="ChangeCartStatus({{$cart->id}})"
                                                    class="browser-default custom-select mb-4 status" name="status">
                                                <option value="" disabled>Choose option</option>
                                                <option
                                                    value="confirmed" {{ $cart->status == 'confirmed' ? 'selected' : '' }}>
                                                    Pending
                                                </option>
                                                <option value="cancelled" {{ $cart->status == 'cancelled' ? 'selected' : '' }}>
                                                    Cancelled
                                                </option>
                                                <option value="completed" {{ $cart->status == 'completed' ? 'selected' : '' }}>
                                                    Completed
                                                </option>
                                            </select>

                                        </td>
                                        {{--<td><input class="target" type="button" onclick="myFunction(this)"></td>--}}

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div id="settings-trigger"><i class="fas fa-plus-circle fa-10x"></i></div>

@endsection
@section('script')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $('.delete-confirm').on('click', function (e) {
            event.preventDefault();
            var id = $(this).data('id');
            const url = $(this).attr('href');
            // alert(id);
            swal({
                title: 'Are you sure?',
                text: 'This record and it`s details will be permanantly deleted!',
                icon: 'warning',
                buttons: ["Cancel", "Yes!"],
            }).then(function (value) {
                if (value) {
                    // window.location.href = url;
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "delete",
                        url: '/admin/category/' + id,
                        data: {
                            id: id
                        },
                        success: function (success) {
                            if (success) {
                                swal(
                                    'Deleted!',
                                    'Your record has been deleted.',
                                    'success'
                                );
                                location.reload();
                            } else {
                                let msg = value.message;
                                console.log('msg');
                            }
                        }
                    });
                }
            });
        });
    </script>c
    <script>
        function ChangeCartStatus(cart_id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let id = "#cart_status_"+cart_id;
            let status = $(id).val();
            $.ajax({
                type: "POST",
                data:{status:status},
                url: '/admin/order/' + cart_id + '/status',
                success: function (response) {
                    console.log(response)
                }
            });
        }
    </script>
@endsection

