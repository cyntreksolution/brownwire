@extends('layouts.backend.master')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>

        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
            Spaces
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home </a></li>
                <li class="breadcrumb-item active" aria-current="page"> Users</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">

              <h4 class="card-title">Spaces Details</h4>
                <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>#</th>
                              <th>Space Name</th>
                               <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($spaces as $key => $space)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$space->name}}</td>


                                         <td>
                                            <a href="{{route('space.edit',[$space->id])}}"><i class="fas fa-pencil-alt btn-icon-append "> </i></a>
                                            <a href="" data-id="{{$space->id}}" class="button delete-confirm">
                                                <i style="color: #e52d27;" class="remove ml-2 fa fa-times-circle"> </i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


<a href="{{ route('space.create')}}">
        <div id="settings-trigger"><i class="fas fa-plus-circle fa-10x"></i></div>
    </a>
@endsection
@section('script')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
$('.delete-confirm').on('click', function (e) {
    event.preventDefault();
     var id = $(this).data('id');
    const url = $(this).attr('href');
    // alert(id);
    swal({
        title: 'Are you sure?',
        text: 'This record and it`s details will be permanantly deleted!',
        icon: 'warning',
        buttons: ["Cancel", "Yes!"],
    }).then(function(value) {
        if (value) {
           // window.location.href = url;
            $.ajaxSetup({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                $.ajax({
                        type: "delete",
                        url: '/admin/space/' + id,
                        data: {
                            id: id
                        },
                        success: function (success) {
                            if (success) {
                                swal(
                                    'Deleted!',
                                    'Your record has been deleted.',
                                    'success'
                                );
                                location.reload();
                            } else {
                                let msg = value.message;
                                console.log('msg');
                            }
                        }
                    });
        }
    });
});
</script>
<script src="{{ asset('admin/js/data-table.js')}}"></script>
@endsection

