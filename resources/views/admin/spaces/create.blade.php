@extends('layouts.backend.master')
@section('content')

        <div class="content-wrapper">
        <!-- Default form contact -->

        @if(count($errors) >0)
        <div class="alert alert-danger">
        <ul>
        @foreach($errors->all() as $error)

        <li>{{$error}}</li>
        @endforeach

        </ul>
        </div>
        @endif
        @if(\Session::has('success'))
    <div class="alert alert-success">
    <p>{{\Session::get('success')}}</p>
    </div>
        @endif
                    <div class="card">

<form class="text-center border border-light p-5" action="{{route('space.store')}}" method="post" id="spaceAdd">

     {{csrf_field()}}
   <div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Spaces Name</label>
            <input type="text" name="name" id="defaultContactFormName" class="form-control" placeholder="Spaces Name">
        </div>
    </div>

</div>
    <!-- Copy -->


    <!-- Send button -->
    <div class="col-md-6">
    <button  class="btn btn-info btn-block" type="submit">Save</button>
</div>
</form>
<!-- Default form contact -->
        </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">


@endsection
@section('script')
<script>
        $("#spaceAdd").validate({
            rules: {
                name: {
                    required: true,
                    // minlength: 5
                },

            },
            messages: {
                name: {
                    required: "Please enter a name",
                    // minlength: "Item name must consist of at least 5 characters"
                },

            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
    </script>
<script src="{{ asset('admin/js/data-table.js')}}"></script>
@endsection
