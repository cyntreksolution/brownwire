<div class="col-md-6">
    <div class="form-group">
        <label>Time Slots Id</label>
     {{ Form::select('time_slot_day', array('Sunday' => 'Sunday', 'Monday' => 'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday ','Friday'=>'Friday','Saturday'=>'Saturday'), 'Monday', ['id' => 'myselect','class' => 'form-control']) }}

    </div>
</div>

<div class="col-md-6">
     <div class="form-group">
         <label>Start Time</label>
         {!! Form::time('start_time', null, ['class' => 'form-control']) !!}
     </div>
 </div>
 <div class="col-md-6">
     <div class="form-group">
         <label>End Time</label>
         {!! Form::time('end_time', null, ['class' => 'form-control']) !!}
     </div>
 </div>
<div class="col-md-6">
    <div class="form-check form-group">
        <label class="form-check-label">
            {!! Form::checkbox('is_week_day', '1', null,  ['id' => 'is_week_day','class'=>'form-check-input']) !!}
            Is Week Day
            <i class="input-helper"></i></label>
    </div>
</div>

<div class="col-md-6">
    <div class="form-check form-group">
        <label class="form-check-label">
            {!! Form::checkbox('is_after_office', '1', null,  ['id' => 'is_after_office','class'=>'form-check-input']) !!}
            Is After Office
            <i class="input-helper"></i></label>
    </div>
</div>

