@extends('layouts.backend.master')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Users Login
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> Users</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Users Details</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Mobile 1</th>
                                    <th>Mobile 2</th>
                                    <th>Role</th>
                                    <th>Email</th>
                                    <th>Crated Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $key => $user)
                                    @php $addr =$user->address()->latest()->first() @endphp
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$addr->contact_no_1}}</td>
                                        <td>{{$addr->contact_no_2}}</td>
                                        <td>{{ preg_replace('~[\\\\/:*?"<>|\\[\\]]~', ' ', $user->roles->pluck('name'))}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->created_at->format('d/m/Y H:i:s')}}</td>

                                        {{--<td><input class="target" type="button" onclick="myFunction(this)"></td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')

@endsection

