<div class="col-md-6">
    <div class="form-group">
        <label>Payment Type Id</label>
        {!! Form::text('payment_type_id', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label>Payment Type Name</label>
        {!! Form::text('payment_type_name', null, ['class' => 'form-control']) !!}
    </div>
</div>



 <div class="col-md-6">
    <div class="form-check form-group">
        <label>Payment Optional Icon</label>
            {!! Form::text('payment_optional_icon', null, ['class' => 'form-control']) !!}
    </div>
</div>

