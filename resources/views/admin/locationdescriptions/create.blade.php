@extends('layouts.backend.master')

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Location Description Create
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('services.index')}}">Location Description</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">
                {!! Form::open(['route' => 'location_description.store', 'method' => 'post','id'=>'createForm']) !!}
                @include('admin.locationdescriptions.form')
                <input class="btn btn-primary" type="submit" value="Submit">
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $("#createForm").validate({
            rules: {
                time_slots_id: {
                    required: true,
                },
                
            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger');
                $(element).addClass('form-control-danger');
            }
        });
    </script>
@endsection
