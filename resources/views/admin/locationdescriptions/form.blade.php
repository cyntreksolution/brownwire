


 <div class="col-md-6">
    <div class="form-check form-group">
        <label>Description</label>
            {!! Form::select('description_id', $description,null, ['class' => 'form-control']) !!}
            <i class="input-helper"></i>
    </div>
</div>

<div class="col-md-6">
    <div class="form-check form-group">
        <label class="">Location</label>
            {!! Form::select('location_id', $location,null, ['class' => 'form-control']) !!}
            <i class="input-helper"></i>
    </div>
</div>