@extends('layouts.backend.master')

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('location_description.index')}}">Location Description</a></li>
                    <li class="breadcrumb-item active" aria-current="page"> </li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">
               {!! Form::model($locationDescription, ['route' => ['location_description.update', $locationDescription->id], 'method' => 'PATCH']) !!}
                @include('admin/locationdescriptions.form')
                <input class="btn btn-primary" type="submit" value="Submit">
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $("#itemEdit").validate({
            rules: {
                description_id: {
                    required: true,
                    minlength: 5
                },
                location_id: {
                    number: true,
                    required: true,
                    min: 0,
                },
               
            },
            messages: {
                name: {
                    required: "Please Select a description id",
                    //minlength: "Item name must consist of at least 5 characters"
                },
                price: {
                    required: "Please Select the location id",
                    //min: "Price must be greater than 0"
                }
            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
    </script>
@endsection
