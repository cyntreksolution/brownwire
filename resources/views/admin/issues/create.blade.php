@extends('layouts.backend.master')

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
               Issue Create
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('issue.index')}}">Issue</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">
                {!! Form::open(['route' => 'issue.store', 'method' => 'post','id'=>'createForm']) !!}
                @include('admin.issues.form')
                <input class="btn btn-primary" type="submit" value="Save issue">
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $("#createForm").validate({
            rules: {
               name: {
                    required: true,
                },


            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger');
                $(element).addClass('form-control-danger');
            }
        });
    </script>
@endsection
