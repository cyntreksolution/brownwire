@extends('layouts.backend.master')

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('immediate_issue.index')}}">Immediate Issues</a></li>
                    <li class="breadcrumb-item active" aria-current="page"> </li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">
               {!! Form::model($immediateIssue, ['route' => ['immediate_issue.update', $immediateIssue->id], 'method' => 'PATCH']) !!}
                @include('admin/immediateissues.form')
                <input class="btn btn-primary" type="submit" value="Submit">
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

