@extends('layouts.backend.master')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Item Mapping
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home </a></li>
                    <li class="breadcrumb-item active" aria-current="page">Item Mapping</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">

                <h4 class="card-title" settings-trigger>Item Mapping Details</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category Name</th>
                                    <th>Service Name</th>
                                    <th>Item Name</th>
                                    <th>Price</th>
                                    <th>Status</th>

                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($data as $key => $item_mapping)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$item_mapping->category->category_name}}</td>
                                        <td>{{$item_mapping->service->name}}</td>
                                        <td>{{$item_mapping->itemtype->itemtypeid}}</td>
                                        <td>{{number_format($item_mapping->price,2)}}</td>
                                        @if($item_mapping->status==1)
                                            <td>
                                                <button class="btn btn-sm btn-success">Active</button>
                                            </td>
                                        @else
                                            <td>
                                               <button class="btn btn-sm btn-warning">Deactive</button>
                                            </td>
                                        @endif


                                        <td>
                                            <a href="{{route('item_mapping.edit',[$item_mapping->id])}}"><i
                                                    class="fas fa-pencil-alt btn-icon-append "> </i></a>
                                            <a href="" data-id="{{$item_mapping->id}}" class="button delete-confirm">
                                                <i style="color: #e52d27;" class="remove ml-2 fa fa-times-circle"> </i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <a href="{{ route('item_mapping.create')}}">
        <div id="settings-trigger"><i class="fas fa-plus-circle fa-10x"></i></div>
    </a>
@endsection
@section('script')
    <script>
        $('.delete-confirm').on('click', function (e) {
            event.preventDefault();
            var id = $(this).data('id');
            const url = $(this).attr('href');
            // alert(id);
            swal({
                title: 'Are you sure?',
                text: 'This record and it`s details will be permanantly deleted!',
                icon: 'warning',
                buttons: ["Cancel", "Yes!"],
            }).then(function (value) {
                if (value) {
                    // window.location.href = url;
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "delete",
                        url: '/admin/item_mapping/' + id,
                        data: {
                            id: id
                        },
                        success: function (success) {
                            if (success=='true') {
                                swal(
                                    'Deleted!',
                                    'Your record has been deleted.',
                                    'success'
                                );
                                location.reload();
                            } else {
                                swal(
                                    'Sorry!',
                                    'Your record has dependencies.',
                                    'warning'
                                );
                            }
                        }
                    });
                }
            });
        });
    </script>
@endsection

