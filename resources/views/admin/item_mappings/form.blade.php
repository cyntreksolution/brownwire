


 <div class="col-md-6">
    <div class="form-check form-group">
        <label>Category</label>
            {!! Form::select('category_id', $category,null, ['class' => 'form-control']) !!}
            <i class="input-helper"></i>
    </div>
</div>

<div class="col-md-6">
    <div class="form-check form-group">
        <label class="">Service</label>
            {!! Form::select('service_id', $service,null, ['class' => 'form-control']) !!}
            <i class="input-helper"></i>
    </div>
</div>
<div class="col-md-6">
    <div class="form-check form-group">
        <label class="">Item Type</label>
            {!! Form::select('itemtype_id', $itemstype,null, ['class' => 'form-control']) !!}
            <i class="input-helper"></i>
    </div>
</div>
 <div class="col-md-6">
     <div class="form-group">
         <label>Price</label>
         {!! Form::text('price', null, ['class' => 'form-control']) !!}
     </div>
 </div>

<div class="col-md-6">
    <div class="form-check form-group">
        <label class="form-check-label">
            {!! Form::checkbox('status', '1', 1,  ['id' => 'is_active','class'=>'form-check-input']) !!}
            Is Active
            <i class="input-helper"></i></label>
    </div>
</div>
