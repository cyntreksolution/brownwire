<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <title>Brownwire.com</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="http://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/bootstrap/css/bootstrap.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/font-awesome/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('frontEnd/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('frontEnd/css/account.css')}}">
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css')}}"
          integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css')}}"
          integrity="sha256-kksNxjDRxd/5+jGurZUJd1sdR2v+ClrCl3svESBaJqw=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>


<body class="pane scroller">
<div id="app">


    <!-- HEADER-MENU ---------------------->
@include('inc.topbar')
<!--/ HEADER-MENU --------------------->

    <main style="padding-bottom: 6% !important;">
        <div class="row m-0">
            <div class="col-md-12" style="padding-top: 2% !important;margin-top: 7% !important;">

                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <h6 class="text-brown accoountMain_head">BROWNWIRE</h6>
                        {{--                            <p class="fs-14 text-black mb-0 text-sm-center subName">Account</p>--}}
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <h3 class="font-weight-bolder header text-black title mt-3 text-sm-center accountHeading_main">
                            Welcome {{\Illuminate\Support\Facades\Auth::user()->name}}</h3>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <a href="/"><input type="button" name="HOME" class="btn btn-brown mr-2 accountHome"
                                           value="Back to Home"/></a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <img class="img_accountTab"
                                     src="{{asset('frontEnd/icon/50px/icons8-test-account-50.png')}}">
                                <a href="/account" class="nav-link">My Profile</a>
                            </li>
                            <li class="nav-item">
                                <img class="img_accountTab"
                                     src="{{asset('frontEnd/icon/50px/icons8-password-50.png')}}">
                                <a href="/security" class="nav-link active">Security</a>
                            </li>
                            <li class="nav-item">
                                <img class="img_accountTab"
                                     src="{{asset('frontEnd/icon/50px/icons8-list-view-50.png')}}">
                                <a href="/activity" class="nav-link">Activity</a>
                            </li>
                            <li class="nav-item">
                                <img class="img_accountTab"
                                     src="{{asset('frontEnd/icon/50px/icons8-sign-out-50.png')}}">
                                <a href="logout" class="nav-link">Logout</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-8">
                        <div class="row">
                            <div class="container-fluid">
                                <div class="tab-content">

                                    <div class="tab-pane active" id="Security" role="tabpanel">
                                        <div class="row">
                                            <div class="col-lg-12 p-md-0">
                                                <h3 class="font-weight-bolder header text-brown text-sm-center accountTab_name">
                                                    Security</h3>
                                            </div>
                                        </div>
                                        @if (session('error'))
                                            <div class="alert alert-danger">
                                                {{ session('error') }}
                                            </div>
                                        @endif
                                        @if (session('success'))
                                            <div class="alert alert-success">
                                                {{ session('success') }}
                                            </div>
                                        @endif
                                        {!! Form::model($user ?? '', ['route' => ['accountupdate.update',Auth::id()],'files'=>true, 'method' => 'PATCH']) !!}
                                        <input type="hidden" name="password_reset" value="1">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                                <label for="inputMDEx" class="text-brown accountTag_name">Name</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8">
                                                <input type="text" disabled class="form-control accountTextData"
                                                       required placeholder="Enter Name"
                                                       value="{{ Auth::user()->name }}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                                <label for="inputMDEx" class="text-brown accountTag_name">Email</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8">
                                                <input disabled type="text" class="form-control accountTextData"
                                                       required placeholder="Email" value="{{ Auth::user()->email }}">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                                <label for="inputMDEx" class="text-brown accountTag_name">Current
                                                    Password</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8">
                                                <input type="password" name="current-password"
                                                       class="form-control accountTextData" required
                                                       placeholder="Password" value="">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                                <label for="inputMDEx" class="text-brown accountTag_name">New
                                                    Password</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8">
                                                <input type="password" name="new-password"
                                                       class="form-control accountTextData" required
                                                       placeholder="New Password" value="">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                                <label for="inputMDEx" class="text-brown accountTag_name text-nowrap">Confirm
                                                    Password</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8">
                                                <input type="password" name="new-password_confirmation"
                                                       class="form-control accountTextData" required
                                                       placeholder="Confirm Password" value="">
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <input type="reset" name="clear" class="btn-brown_3nd mt-3"
                                                       value="CLEAR"/>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <input type="submit" name="confirm"
                                                       class="btn-brown_2nd mt-3 accountConfirm" value="CONFIRM">
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>


    <!-- FOTTER-MENU ---------------------->
@include('inc.fotter')
<!--/ FOTTER-MENU --------------------->

</div>
<script src="{{asset('frontEnd/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/popper/popper.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js')}}"
        integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
<script src="{{asset('frontEnd/js/custom.js')}}"></script>

</body>
</html>
