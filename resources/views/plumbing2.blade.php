<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <title>Brownwire.com</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="http://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/bootstrap/css/bootstrap.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/font-awesome/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('frontEnd/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css')}}"
          integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css')}}"
          integrity="sha256-kksNxjDRxd/5+jGurZUJd1sdR2v+ClrCl3svESBaJqw=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <style>
        .section {
            background-color: white;
            color: black;
        }

        .section2 {
            background-color: #8A4B20;
            color: black;
        }

        .section2 .text_Data {
            color: white;
        }

        .section2 .text_Data_large {
            color: white;
        }

        .selected {
            background-color: #8A4B20 !important;
            color: white !important;
        }

        .selected .text_Data{
            background-color: #8A4B20 !important;
            color: white !important;
        }
        .selected .text_Data_large{
            background-color: #8A4B20 !important;
            color: white !important;
        }

        .radio-toolbar-round-3nd label {
            padding-top: 40px !important;
        }

        .overlay-spin{
            background: #ffffff;
            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 5000;
            top: 0;
            left: 0;
            float: left;
            text-align: center;
            padding-top: 25%;
            opacity: .80;
        }
    </style>
</head>


<body class="pane scroller">
<div id="app">


    <!-- HEADER-MENU -->
@include('inc.topbar')
<!--/ HEADER-MENU-->

    <main style="padding-bottom: 6% !important;">
        <div class="row m-0">
            <div class="col-md-9 mt-5" style="padding-top: 2% !important;margin-top: 7% !important;">
                <div class="row" id="taprow">
                    @include('cart.left_sidebar')
                    <div  id="loadingDiv" class="text-center overlay-spin">
                        <div class="spinner-grow" style="width: 3rem; height: 3rem;" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            {!! Form::open(['route' => 'proceed.transaction', 'method' => 'post','id'=>'msform','style'=>'width: 100% !important;']) !!}
                            <input type="hidden" name="category_id" id="category_id">
                            <input type="hidden" name="service_id" id="service_id">
                            <input type="hidden" name="itemtype_id" id="itemtype_id">
                            <input type="hidden" name="issue_id" id="issue_id">

                            <input type="hidden" name="isNeedItem" id="isNeedItem">
                            <input type="hidden" name="item_id" id="item_id">
                            <input type="hidden" name="selectedDate" id="selectedDate">
                            <input type="hidden" name="timeslot_id" id="timeslot_id">


                            <input type="hidden" name="inspection_charge" id="inspection_charge">
                            <input type="hidden" name="service_charge" id="service_charge">
                            <input type="hidden" name="material_charge" id="material_charge">
                            <input type="hidden" name="estimate" id="estimate">
                            <input type="hidden" name="user_id" id="user_id"
                                   value="{{\Illuminate\Support\Facades\Auth::id()}}">


                            <!-- START SERVICE CATEGORY PART  -->
                        @include('cart.category')
                        <!-- END SERVICE CATEGORY PART  -->


                            <!-- START- WHAT NEED TO BE DONE PART -->
                        @include('cart.service')
                        <!-- END- WHAT NEED TO BE DONE PART  -->


                            <!-- START FIXING-PART  -->
                        @include('cart.item_type')
                        <!-- END- FIXING-PART  -->


                            <!-- START- ISSUE-PART  -->
                        @include('cart.issue')
                        <!-- END- ISSUE-PART  -->


                            <!-- START- YES/NO  -->
                        @include('cart.item')
                        <!-- END- YES/NO  -->


                            <!-- START- TIME SETTINGS  -->
                        @include('cart.time')
                        <!-- END- TIME SETTINGS  -->


                            <!-- START- LOCATION SERVICES  -->


                            </form>

                        </div>
                    </div>
                </div>
            </div>


            @include('cart.right_sidebar')
        </div>
    </main>


    <!-- FOTTER-MENU -->
@include('inc.fotter')
<!--/ FOTTER-MENU -->

    <!--/////CONTACT US MENU -->
</div>
<script src="{{asset('frontEnd/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/popper/popper.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js')}}"
        integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
<script src="{{asset('frontEnd/js/custom.js')}}"></script>

<script>
    var is_skip = 0;
    var category_selected = 0;
    var service_selected = 0;
    var item_type_selected = 0;
    var timeslot_selected = 0;
    var is_need_item = 1;
    var is_item_selected = 0;
    var category = 0;

    var select_category_arrow = $('#select_category_arrow');
    var select_service_arrow = $('#select_service_arrow');
    var select_item_type_arrow = $('#select_item_type_arrow');
    var select_issue_arrow = $('#select_issue_arrow');

    var $loading = $('#loadingDiv').hide();
    $(document)
        .ajaxStart(function () {
            $loading.show();
        })
        .ajaxStop(function () {
            $loading.hide();
        });

    $(document).ready(function () {
        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;

        $(".next").click(function () {

            current_fs = $(this).parent()

            if (current_fs[0].id == 'category_section') {
                if (!category_selected) {
                    Swal.fire('Please Select Category');
                    return 0;
                }
                select_category_arrow.removeClass('d-none');
                select_service_arrow.addClass('d-none');
                select_item_type_arrow.addClass('d-none');
            }
            if (current_fs[0].id == 'service_section') {
                if (!service_selected) {
                    Swal.fire('Please Select Service');
                    return 0;
                }
                select_service_arrow.removeClass('d-none');
                select_item_type_arrow.addClass('d-none');
            }
            if (current_fs[0].id == 'item_type_section') {
                if (!item_type_selected) {
                    Swal.fire('Please Select Item Type');
                    return 0;
                }
                select_item_type_arrow.removeClass('d-none');
            }
            if (current_fs[0].id == 'timeslot_section') {
                if (!timeslot_selected) {
                    Swal.fire('Please Select Time Slot');
                    return 0;
                }
            }


            if (current_fs[0].id == 'item_section') {
                console.log('is_need_item')
                console.log('is_item_selected')
                console.log(is_item_selected)
                if (is_need_item) {
                    if (!is_item_selected) {
                        Swal.fire('Please Select Item');
                        return 0;
                    }
                }
            }


            if (current_fs[0].id == 'item_type_section' && is_skip) {
                next_fs = $('#item_section');
            } else {
                next_fs = $(this).parent().next();
            }

            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({'opacity': opacity});
                },
                duration: 600
            });
        });

        $(".previous").click(function () {

            current_fs = $(this).parent()
            if (current_fs[0].id == 'item_section' && is_skip) {
                previous_fs = $('#item_type_section');
            } else {
                previous_fs = $(this).parent().prev();
            }

            //Remove class active
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();

            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    previous_fs.css({'opacity': opacity});
                },
                duration: 600
            });
        });

        $('.radio-group .radio').click(function () {
            $(this).parent().find('.radio').removeClass('selected');
            $(this).addClass('selected');
        });

        $(".submit").click(function () {
            return false;
        })
    });

    $(function () {
        $('.section').click(function () {
            $('.section2').removeClass('section2');
            $(this).addClass('section2');
        });
    });


    CategoryClick({{$selected}});
    @php $today = Carbon\Carbon::today() @endphp
    @php $day1 =Carbon\Carbon::today()->addDays(1) @endphp
    @if (time() < strtotime('18:00:00'))
    clickday('{{ $today}}', '{{$today->isoFormat('dddd')}}')
    @else
    clickday('{{ $day1}}', '{{$day1->isoFormat('dddd')}}')

    @endif


    function CategoryClick(category_id) {
        $('#category_id').val(category_id);
        category_selected = 1;
        category = category_id;
        updateCategoryNameOnRightBar(category_id)
        $.ajax({
            type: "GET",
            url: '/cart/' + category_id,
            success: function (response) {
                $("#serviceRow").empty();
                $.each(response, function (key, value) {
                    $("#serviceRow").append('<div onclick="clickme(' + value.service_id + ')" id="ser_' + value.service_id + '" class="col-lg-3 col-md-3 col-sm-3 m-3">\n' +
                        '                                            <div class="card_large section service">\n' +
                        '                                                <p class="containerData_large">\n' +
                        '                                                    <input type="radio"  id="INSTALLATION" name="NeedTo_beDone">\n' +
                        '                                                    <span class="checkmark_large"><img src="/frontEnd/icon/' + value.service.service_image + '" class="iconData_large"></span>\n' +
                        '                                                </p>\n' +
                        '                                                <p class="text_Data_large">' + value.service.name + '</p>\n' +
                        '                                            </div>\n' +
                        '                                        </div>');
                });
            }
        });
    }

    function updateInspectionCharge(service_id) {
        $.ajax({
            type: "GET",
            url: '/services/' + service_id,
            success: function (response) {
                $('#inspection_charge').val(response.inspection_charge);
                $('#inspection_t').empty();
                $('#service_t').empty();
                $('#service_t').text(response.name);
                $('#service_tt').empty();
                $('#service_tt').text(response.name);
                $('#lft_service').empty();
                $('#lft_service').text(response.name);
                $('#inspection_t').text(Number.parseFloat(response.inspection_charge).toFixed(2));
                updateEstOnRightBar();
            }
        });

    }

    function updateCategoryNameOnRightBar(category_id) {
        $.ajax({
            type: "GET",
            url: '/category/' + category_id,
            success: function (response) {
                $('#category_t').empty();
                $('#category_t').text(response.category_name);
                $('#lft_category').empty();
                $('#lft_category').text(response.category_name);
            }
        });
    }

    function updateItemTypeOnRightBar(item_type_id) {
        $.ajax({
            type: "GET",
            url: '/item-type/' + item_type_id,
            success: function (response) {
                $('#item_t').empty();
                $('#item_t').text(response.itemtypeid);
                $('#lft_item_type').empty();
                $('#lft_item_type').text(response.itemtypeid);
            }
        });
    }

    function updatePriceOnRightBar(item_id) {
        $.ajax({
            type: "GET",
            url: '/item/' + item_id,
            success: function (response) {
                $('#item_name_t').empty();
                $('#item_price_t').empty();
                $('#item_name_t').text(response.item_id);
                $('#item_price_t').text(parseFloat(response.price).toFixed(2));
                $('#material_charge').val(parseFloat(response.price).toFixed(2));
                updateEstOnRightBar();
            }
        });

    }

    function updateServicePriceOnRightBar(id) {
        $.ajax({
            type: "GET",
            url: '/item-mapping/' + id,
            success: function (response) {
                $('#service_price_t').empty();
                $('#service_price_t').text(Number.parseFloat(response.price).toFixed(2));
                $('#service_charge').val(Number.parseFloat(response.price).toFixed(2));
                updateEstOnRightBar()
            }
        });

    }

    function updateEstOnRightBar() {
        let inspection = Number.parseFloat($('#inspection_t').text().replace(',', '.', '$ '));
        let replacement = Number.parseFloat($('#service_price_t').text().replace(',', '.', '$ '));
        let item = Number.parseFloat($('#item_price_t').text().replace(',', '.', '$'));
        let total = replacement + item;
        // let total = inspection + replacement + item;

        $('#lbl_est').empty();
        $('#lbl_est').text(Number.parseFloat(total).toFixed(2));
        $('#estimate').val(Number.parseFloat(total).toFixed(2));

    }


    var service = 0;

    function clickme(service_id) {
        $('#service_id').val(service_id);
        service_selected = 1;
        service = service_id;
        $('.service').removeClass('selected');
        let xxx = '#ser_' + service_id;
        $(xxx).children().addClass('selected');

        updateInspectionCharge(service_id);
        $.ajax({
            type: "GET",
            url: '/plumbing4/' + service_id + '/' + category,
            success: function (response) {
                $("#itemtyperow").empty();
                $.each(response, function (key, value) {
                    $("#itemtyperow").append('  <div onclick="clickitem(' + value.id + ',' + value.itemtype_id + ')" id="ite' + value.id + '" class="col-lg-3 col-md-12 col-sm-12" >\n' +
                        '                           <input type="radio" id="Leaking_' + value.id + '" name="needFix" >\n' +
                        '                                                    <p class="lbl_item_type" id="lebel_' + value.id + '" for="Leaking_' + value.id + '">' + value.itemtype.itemtypeid + '</p>\n' +
                        '                                                </div>');
                });
            }
        });

    }

    var item = 0;

    function setIsSkip(value) {
        is_skip = value;
    }

    function clickitem(id, itemTypeId) {
        setIsSkip(0);
        updateItemTypeOnRightBar(itemTypeId);
        updateServicePriceOnRightBar(id);
        $('#itemtype_id').val(itemTypeId);
        item_type_selected = 1;
        let selected_element = '#lebel_' + id;
        $('.lbl_item_type').removeClass('selected')
        $(selected_element).addClass('selected');
        $.ajax({
            type: "GET",
            url: '/plumbing5/' + id,
            success: function (response) {
                $("#issuerow").empty();
                $.each(response, function (key, value) {
                    console.log(value)
                    if (value.issue == null) {
                        setIsSkip(1);
                        return 1;
                    } else {
                        $("#issuerow").append(' <div onclick="clickissue(' + value.issue.id + ')"  class="col-lg-3 col-md-12 col-sm-12">\n' +
                            '                                                    <input type="radio" id="Leaking" name="SelectIssue">\n' +
                            '                                                    <p id="issue_lbl_' + value.issue.id + '" class="lbl_issue" for="Leaking">' + value.issue.name + '</p>\n' +
                            '                                                </div>');
                    }

                });
            }
        });
        $.ajax({
            type: "GET",
            url: '/plumbing6/' + itemTypeId,
            success: function (response) {
                $("#taprownew").empty();
                $.each(response, function (key, value) {
                    let price = parseFloat(value.price);
                    $("#taprownew").append('<div onclick="clickitemcheck(' + value.id + ')" class="col-lg-3 col-md-3 col-sm-3">\n' +
                        '                                                    <input type="radio"  name="ChooseItems" checked="checked">\n' +
                        '                                                    <label class="item_lbl" id="item_lbl_' + value.id + '" >\n' +
                        '                                                        <img style="width: 50px;height: 50px" src="/uploads/items/' + value.image + '">\n' +
                        '                                                    </label>\n' +
                        '                                                    <p class="text_Data_choose">' + value.item_id + '</p>\n' +
                        '                                                    <p class="text_Data_choose">' + '$ ' + Number.parseFloat(price).toFixed(2) + '</p>\n' +
                        '                                                </div>');
                });
            }
        });
    }

    function clickissue(issue_id) {
        let selected_element = '#issue_lbl_' + issue_id;
        $('.lbl_issue').removeClass('selected')
        $(selected_element).addClass('selected');
        $('#issue_id').val(issue_id);
    }

    function clickitemcheck(item1_id) {
        is_item_selected = 1;
        let selected_element = '#item_lbl_' + item1_id;
        $('.item_lbl').removeClass('selected')
        $(selected_element).addClass('selected');
        $('#item_id').val(item1_id);
        updatePriceOnRightBar(item1_id);
    }

    function clicktimeslot(timeslot, dateString) {
        $('#timeslot_id').val(timeslot);
        timeslot_selected = 1;
        let selected_element = '#timeslot_s_' + timeslot;
        $('.timeslot').removeClass('selected')
        $(selected_element).addClass('selected');
    }

    function clickday(dy, dateString) {
        $('#selectedDate').val(dy);
        $.ajax({
            type: "GET",
            url: '/get/timeslot/' + dateString,
            success: function (response) {
                $("#timeslot").empty();
                $.each(response, function (key, value) {
                    $("#timeslot").append('<div class="col-md-4" onclick="clicktimeslot(' + value.id + ')">\n' +
                        '                        <input type="radio" name="radioFruit4" >\n' +
                        '                        <label class="timeslot" id="timeslot_s_' + value.id + '">' + value.start_time + ' - ' + value.end_time + '</label>\n' +
                        '                    </div>');
                });
            }
        });
    }

    function haveMaterial(status) {
        $('.lbl-item-req').removeClass('selected')
        if (status) {
            is_need_item = 1;
            $('#item_yes').addClass('selected');
            $('#itemSelection').removeClass('d-none');
        } else {
            is_need_item = 0;
            $('#item_no').addClass('selected');
            $('#itemSelection').addClass('d-none');

            $('#item_name_t').empty();
            $('#item_price_t').empty();
            $('#item_price_t').text('00.00');

            $('.item_lbl').removeClass('selected')
            $('#material_charge').val(0);
            $('#item_id').val(null);
            is_item_selected = 0;
        }

    }

    function submitForm() {
        if (!timeslot_selected) {
            Swal.fire('Please Select Time Slot');
            return 0;
        }


        $('#msform').submit();
    }


</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</body>
</html>
