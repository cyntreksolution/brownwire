<footer class="">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <img class="backgroundImage_service" src="{{asset('assest/images/back2.png')}}">
                <a class="navbar-brand" href="/">
                    <h2 class="font-weight-bolder">
                        <img class="backgroundImage_logo" src="{{asset('frontEnd/images/logo.png')}}">
                    </h2>
                </a>

                <p class="fs-14 text-black mt-3">{{!empty($config_data->description)?$config_data->description:''}}
                </p>
            </div>
            <div class="col-lg-8">
                <h4 class="text-black font-weight-bold">You can always reach us by</h4>
                <div class="row mt-4">
                    <div class="col-lg-4 col-md-4">
                        <h6 class="text-brown">EMAIL</h6>
                        <p class="fs-14 text-black mb-0">{{!empty($config_data->email)?$config_data->email:''}}</p>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <h6 class="text-brown">MAKE CALL</h6>
                        <p class="fs-14 text-black mb-0">{{!empty($config_data->contact)?$config_data->contact:''}}</p>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <h6 class="text-brown">SITE NAVIGATION</h6>
                        <a href="/#" style="text-decoration: none !important;"><p class="fs-14 text-black mb-0">How it works</p></a>
                        <a href="/login" class="fs-14 text-black mb-0">Sign Up / Login</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-md-5">
            <div class="col-md-12">
                <p style="font-size: 13px" class="text-center text-brown"><strong>www.brownwires.com All Rights Reserved. Powered By Cyntrek Solutions</strong> </p>
            </div>
        </div>
    </div>
</footer>
