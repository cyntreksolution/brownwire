<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light-brown shadow-sm" >
    <div class="container-fluid" >
        <a class="navbar-brand" href="{{$config_data->home_url}}">
            <h2 class="font-weight-bolder">
                <img class="backgroundImage_logo" src="{{asset('frontEnd/images/logo.png')}}">
            </h2>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav ml-auto menu" style="font-weight: bold !important;">
                <li class="nav-item ">
                    <a class="nav-link text-brown" href="/#home">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-brown" href="/#services">Services</a>
                </li>
                <li class="nav-item mr-2">
                    <a class="nav-link text-brown" href="/#contact">Contact Us</a>
                </li>



{{--                @if (Route::has('register'))--}}
{{--                <li class="nav-item btn-align">--}}
{{--                    <a class="btn btn-brown mr-2" href="{{ route('register') }}">{{ __('SIGNIN') }}</a>--}}
{{--                </li>--}}
{{--                @endif--}}

                @if (Auth::check())
                    <li class="nav-item mr-2">
                        <div class="row">
{{--                            <div class="col-lg-8 col-md-2 col-sm-2">--}}
{{--                                <a class="nav-link text-brown" href="/account" style="pointer-events: none !important">My Account</a>--}}
{{--                            </div>--}}
                            <div class="col-lg-4 col-md-1 col-sm-1">
                                <a href="/account"><img src="{{asset('frontEnd/icon/icons8-test-account-100.png')}}" class="iconLog"></a>
                            </div>
                        </div>
                    </li>
                @else
                    <li class="nav-item btn-align">
                        <a class="btn btn-brown mr-2" href="{{ route('register') }}">{{ __('SIGNUP') }}</a>
                    </li>
                <li class="nav-item btn-align">
                    <a class="btn btn-transparent" href="{{ route('login') }}">{{ __('LOGIN') }}</a>
                </li>

                @endif
              </ul>
        </div>
    </div>
</nav>
