<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <title>Brownwire.com</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="http://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/bootstrap/css/bootstrap.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/font-awesome/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('frontEnd/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css')}}"
          integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css')}}"
          integrity="sha256-kksNxjDRxd/5+jGurZUJd1sdR2v+ClrCl3svESBaJqw=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>


<body class="pane scroller" style="overflow-x: hidden !important;">
<div id="app">


    <!-- HEADER-MENU ---------------------->
@include('inc.topbar')
<!--/ HEADER-MENU --------------------->

    <main style="padding-bottom: 6% !important;">
        <div class="row m-0">
            <div class="col-md-12 mt-5" style="padding-top: 2% !important;margin-top: 7% !important;">
                <div class="row">
                    <div class="col-md-2">
                        {{--                        <div class="col-md-12">--}}
                        {{--                            <h4 id="head_title_menuSet" class="font-weight-bolder header text-blue title mt-3">You selected</h4>--}}

                        {{--                            <div class="timeline">--}}
                        {{--                                <p> Plumbing </p>--}}
                        {{--                                <i class="fa fa-arrow-circle-down text-orange fa-2x"></i>--}}
                        {{--                            </div>--}}

                        {{--                            <div class="timeline">--}}
                        {{--                                <p> Kitchen </p>--}}
                        {{--                                <i class="fa fa-arrow-circle-down text-orange fa-2x"></i>--}}
                        {{--                            </div>--}}

                        {{--                            <div class="timeline">--}}
                        {{--                                <p> Sink Tap </p>--}}
                        {{--                                <i class="fa fa-arrow-circle-down text-orange fa-2x"></i>--}}
                        {{--                            </div>--}}

                        {{--                            <div class="timeline">--}}
                        {{--                                <p> Broken </p>--}}
                        {{--                                <i class="fa fa-arrow-circle-down text-orange fa-2x"></i>--}}
                        {{--                            </div>--}}

                        {{--                            <div class="timeline">--}}
                        {{--                                <p> Replacement </p>--}}
                        {{--                                <i class="fa fa-arrow-circle-down text-orange fa-2x"></i>--}}
                        {{--                            </div>--}}

                        {{--                            <div class="timeline">--}}
                        {{--                                <p> With material </p>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>

                    <div class="col-md-10">
                        <div class="row">
                            <form id="msform">
                                {{--                                <div id="progressbar">--}}
                                {{--                                    <div class="row">--}}
                                {{--                                        <div class="col-lg-9 col-md-8 col-sm-8">--}}
                                {{--                                            <li class="active" id="Choose" ><strong>Choose</strong></li>--}}
                                {{--                                            <li class="active" id="add_info"><strong>Add info</strong></li>--}}
                                {{--                                            <li class="active" id="Shedule"><strong>Schedule</strong></li>--}}
                                {{--                                            <li class="active" id="Confirm2"><strong>Confirm</strong></li>--}}
                                {{--                                        </div>--}}
                                {{--                                        <div class="col-lg-3 col-md-4 col-sm-4">--}}
                                {{--                                            <a href="/"><input type="button" name="HOME" class="btn btn-brown" value="BACK TO HOME" /></a>--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h4 id="head_title_plumbing3" class="font-weight-bolder header text-orange">Your
                                            request has been completed successfully. Our technician will contact you
                                            accordingly.</h4>
                                    </div>
                                </div>
                                <div class="bg-brown_02">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <p class="text-black">{{$cart->job_id}}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-12 col-sm-12">
                                            <div class="bg-white current-inner-box" style="margin-top: 2%;">
                                                <div class="row m-0">
                                                    <div class="col-lg-5 col-md-5 col-sm-5 p-0">
                                                        <p class="right_panel_text_3">{{$cart->category->category_name}}</p>
                                                    </div>
                                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                                        <p class="right_panel_text_3">{{$cart->service->name}}
                                                            - {{$cart->itemtype->itemtypeid}}</p>
                                                    </div>
                                                </div>
                                                <hr class="class-1">

                                                <div class="col-md-12 mt-4">
                                                    <div class="row mt-2">
                                                        <div class="col-md-9 text-black_1">Inspection <span
                                                                class="text-black_1">[waived]</span></div>
                                                        <div class="col-md-3 text-black_1 text-right">$ {{number_format($cart->inspection_charge,2)}}</div>
                                                    </div>

                                                    <div class="row mt-2">
                                                        <div
                                                            class="col-md-9 text-black_1">{{$cart->service->name}}</div>
                                                        <div class="col-md-3 text-black_1 text-right">$ {{number_format($cart->service_charge,2)}}</div>
                                                    </div>

                                                    <div class="row mt-2">
                                                        <div class="col-md-9 text-black_1">Material <span
                                                                class="text-black_1">{{!empty($cart->item)?$cart->item->item_id:''}}</span>
                                                        </div>
                                                        <div class="col-md-3 text-black_1 text-right">
                                                            $ {{number_format($cart->material_charge,2)}}</div>
                                                    </div>

                                                    <div class="row mt-2">
                                                        <div class="col-md-9 text-brown_1 font-weight-bold">Estimated
                                                            Total **
                                                        </div>
                                                        <div class="col-md-3 text-brown_1 text-right font-weight-bold">
                                                            ${{!empty($cart->estimate)?number_format($cart->estimate,2):00}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-12 col-sm-12 ">
                                            <div class="bg-white current-inner-box" style="margin-top: 2%;">
                                                <div class="row m-0">
                                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                                        <img style="width: 30px;height: 30px"
                                                             src="{{asset('frontEnd/icon/icons8-calendar-100.png')}}">
                                                    </div>
                                                    <div class="col-lg-10 col-md-10 col-sm-10">
                                                        <p class="service-type">Date & Time</p>
                                                    </div>
                                                </div>
                                                <div class="row m-0">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <p class="right_panel_text_3">{{\Carbon\Carbon::parse($cart->selectedDate)->format('D,d M Y')}}
                                                            <span class="right_panel_text_2">{{\Carbon\Carbon::parse($cart->timeslot->start_time)->format('g:i A')}} - {{\Carbon\Carbon::parse($cart->timeslot->end_time)->format('g:i A')}}</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <hr class="class-1">
                                                <p class="text-black_3">
                                                    <b>Instructions for Technician</b><br>
                                                    <span
                                                        style="padding-left: 30px">{{!empty($cart->instruction)?$cart->instruction:'-'}}</span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-12 col-sm-12 ">
                                            <div class="bg-white current-inner-box" style="margin-top: 2%;">
                                                <div class="row m-0">
                                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                                        <img style="width: 30px;height: 30px"
                                                             src="{{asset('frontEnd/icon/icons8-marker-100.png')}}">
                                                    </div>
                                                    <div class="col-lg-10 col-md-10 col-sm-10">
                                                        <p class="service-type">Location</p>
                                                    </div>
                                                </div>
                                                <div class="row m-0">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        @php $address =$cart->address @endphp
                                                        <p class="right_panel_text_3">{{$address->address_1.','.$address->address_2.','.$address->address_3.', Singapore, '.$address->postal_code}}
                                                        </p>
                                                    </div>
                                                </div>
                                                <hr class="class-1">
                                                <p class="text-black_3">
                                                    <b>Directions for Technician</b><br>
                                                    <span
                                                        style="padding-left: 30px">{{!empty($cart->directions_for_technician)?$cart->directions_for_technician:'-'}}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-12 col-sm-12">

                                        </div>
                                        <div class="col-lg-8 col-md-12 col-sm-12">
                                            <div class="">
                                                <p class="text-black_2">
                                                    ** Your technician will fully inspect your area of concern and
                                                    update you on recommended service should there be anything diﬀerent
                                                    from your original service request.
                                                <p>
                                                <p class="text-black_2">
                                                    # The inspection is free if you move forward with your original
                                                    service request or with our recommended service.<br>

                                                    You are also free to decline either service oﬀered but a fee of
                                                    $25.00 will be charged for the inspecon.</p>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="class-2">

                                    <div class="col-lg-12 col-md-12">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                                <p class="right_panel_text_3">Payment Instructions :</p>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <p class="service-type mx-auto text-black_1">Using PayNow</p>
                                                <p class="text-black_2">
                                                    1. On your job report handed over by technician, scan the brownwires
                                                    PayNow QR code and transfer the amount from your account.
                                                <p>
                                            </div>

                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                                <button type="button" class="qrCodeBtn"><img
                                                        src="{{asset('frontEnd/icon/icons8-qr-code-100.png')}}"
                                                        class="QrCode"></button>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <p class="service-type mx-auto text-black_1">Using By cash</p>
                                                <p class="text-black_2">
                                                    1. Simply hand over the cash to our technician.<br>
                                                    2. In return, a cash received note will be provided by the
                                                    technician.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>


    <!-- FOTTER-MENU ---------------------->
@include('inc.fotter')
<!--/ FOTTER-MENU --------------------->

</div>
<script src="{{asset('frontEnd/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/popper/popper.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js')}}"
        integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
<script src="{{asset('frontEnd/js/custom.js')}}"></script>

<script>
    $(document).ready(function () {

        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;

        $(".next").click(function () {

            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({'opacity': opacity});
                },
                duration: 600
            });
        });

        $(".previous").click(function () {

            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();

            //Remove class active
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();

            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    previous_fs.css({'opacity': opacity});
                },
                duration: 600
            });
        });

        $('.radio-group .radio').click(function () {
            $(this).parent().find('.radio').removeClass('selected');
            $(this).addClass('selected');
        });

        $(".submit").click(function () {
            return false;
        })
    });
</script>
</body>

</html>
