<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <title>Brownwire.com</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="http://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/bootstrap/css/bootstrap.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/font-awesome/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('frontEnd/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('frontEnd/css/account.css')}}">
    <link rel="stylesheet" href="{{asset('frontEnd/css/activity.css')}}">
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css')}}"
          integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css')}}"
          integrity="sha256-kksNxjDRxd/5+jGurZUJd1sdR2v+ClrCl3svESBaJqw=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        .btn-danger {
            margin-right: 15px !important;
        }
    </style>
</head>


<body class="pane scroller">
<div id="app">


    <!-- HEADER-MENU ---------------------->
@include('inc.topbar')
<!--/ HEADER-MENU --------------------->

    <main style="padding-bottom: 6% !important;">
        <div class="row m-0">
            <div class="col-md-12" style="padding-top: 2% !important;margin-top: 7% !important;">

                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <h6 class="text-brown accoountMain_head">BROWNWIRE</h6>
                        {{--                            <p class="fs-14 text-black mb-0 text-sm-center subName">Account</p>--}}
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <h3 class="font-weight-bolder header text-black title mt-3 text-sm-center accountHeading_main">
                            Welcome {{\Illuminate\Support\Facades\Auth::user()->name}}</h3>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <a href="/"><input type="button" name="HOME" class="btn btn-brown mr-2 accountHome"
                                           value="Back to Home"/></a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <img class="img_accountTab"
                                     src="{{asset('frontEnd/icon/50px/icons8-test-account-50.png')}}">
                                <a class="nav-link" href="/account">My Profile</a>
                            </li>
                            <li class="nav-item">
                                <img class="img_accountTab"
                                     src="{{asset('frontEnd/icon/50px/icons8-password-50.png')}}">
                                <a href="/security" class="nav-link">Security</a>
                            </li>
                            <li class="nav-item">
                                <img class="img_accountTab"
                                     src="{{asset('frontEnd/icon/50px/icons8-list-view-50.png')}}">
                                <a href="/activity" class="nav-link active">Activity</a>
                            </li>
                            <li class="nav-item">
                                <img class="img_accountTab"
                                     src="{{asset('frontEnd/icon/50px/icons8-sign-out-50.png')}}">
                                <a href="logout" class="nav-link">Logout</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-8">
                        <div class="row">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-12 p-md-0">
                                        <h3 class="font-weight-bolder header text-brown text-sm-center accountTab_name">
                                            Activity</h3>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <ul class="nav nav-justified">
                                            <li class="tab-items">
                                                <a class="nav-link active" id="Upcoming-tab" data-toggle="tab"
                                                   href="#Upcoming" role="tab" aria-controls="Upcoming"
                                                   aria-selected="true">Upcoming</a>
                                            </li>
                                            <li class="tab-items">
                                                <a class="nav-link" id="History-tab" data-toggle="tab" href="#History"
                                                   role="tab" aria-controls="History" aria-selected="false">History</a>
                                            </li>
                                        </ul>

                                        <div class="tab-content">


                                            <div class="tab-pane fade show active" id="Upcoming" role="tabpanel"
                                                 aria-labelledby="Upcoming-tab">

                                                <div class="panel-group" id="accordion" role="tablist"
                                                     aria-multiselectable="true">

                                                    @foreach($activities as $activity)
                                                        @if ($activity->status == 'confirmed')
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed" role="button"
                                                                           data-toggle="collapse"
                                                                           data-parent="#accordion"
                                                                           href="#collapse{{$activity->id}}"
                                                                           aria-expanded="false"
                                                                           aria-controls="collapse{{$activity->id}}">
                                                                            <div class="row">
                                                                                <div class="col-lg-3 col-md-3 col-sm-3">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-2 col-md-2 col-sm-2">
                                                                                            <img
                                                                                                class="activityUpcoming_img_sm"
                                                                                                src="{{asset('upload/frontEnd/icon/'.$activity->category->category_image)}}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <p class="activityUpcoming_mainHead">{{$activity->category->category_name}}</p>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <p class="activityUpcoming_subHead">{{$activity->service->name}}
                                                                                            - {{!empty($activity->itemtype)?$activity->itemtype->itemtypeid:''}} - {{!empty($activity->item)?$activity->item->item_id:''}}</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-3 col-md-3 col-sm-3">
                                                                                    <div class="row">
                                                                                        <p class="activityUpcoming_job">{{$activity->job_id}}</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-3 col-md-3 col-sm-3">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-2 col-md-2 col-sm-2">
                                                                                            <img
                                                                                                class="activityUpcoming_img_sm"
                                                                                                src="{{asset('frontEnd/icon/50px/icons8-calendar-50.png')}}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <p class="activityUpcoming_mainHead_date">{{\Carbon\Carbon::parse($activity->selectedDate)->format('D,d M Y')}}</p>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <p class="activityUpcoming_subHead_time p-0 text-center">{{\Carbon\Carbon::parse($activity->timeslot->start_time)->format('g:i A')}}
                                                                                            - {{\Carbon\Carbon::parse($activity->timeslot->end_time)->format('g:i A')}}</p>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                                    <h4 id="activityUpcoming_subHead_shedule"
                                                                                        class="font-weight-bolder header text-orange">
                                                                                        SCHEDULED</h4>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapse{{$activity->id}}"
                                                                     class="panel-collapse collapse" role="tabpanel"
                                                                     aria-labelledby="headingOne">
                                                                    <div class="panel-body">

                                                                        <div class="row">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12">
                                                                                <div class="bg-white current-inner-box"
                                                                                     style="margin-top: 2%;">
                                                                                    <div class="row m-0">
                                                                                        <div
                                                                                            class="col-lg-5 col-md-5 col-sm-5 p-0">
                                                                                            <p class="activityPanel_body_topicMain">{{$activity->category->category_name}}</p>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-7 col-md-7 col-sm-7">
                                                                                            <p class="activityPanel_body_topicSub">{{$activity->service->name}}
                                                                                               {{!empty($activity->itemtype)?$activity->itemtype->itemtypeid:''}} - {{!empty($activity->item)?$activity->item->item_id:''}}</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <hr class="class-2">

                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-md-8 text-black_activity">
                                                                                            Inspection <span
                                                                                                class="text-black_activity">[waived]</span>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-md-4 text-black_activity text-right text-nowrap">
                                                                                            <p class="text-nowrap text-center">
                                                                                                $ {{number_format($activity->inspection_charge,2)}}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mt-2">
                                                                                        <div
                                                                                            class="col-md-8 text-black_activity">{{$activity->service->name}}</div>
                                                                                        <div
                                                                                            class="col-md-4 text-black_activity text-right text-nowrap">
                                                                                            <p class="text-nowrap text-center">
                                                                                                $ {{number_format($activity->service_charge,2)}}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mt-2">
                                                                                        <div
                                                                                            class="col-md-8 text-black_activity">
                                                                                            Material <span
                                                                                                class="text-black_activity">[{{!empty($activity->item)?$activity->item->item_id:''}}]</span>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-md-4 text-black_activity text-right text-nowrap">
                                                                                            <p class="text-nowrap text-center">
                                                                                                $ {{number_format($activity->material_charge,2)}}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <hr class="class-3">
                                                                                    <div class="row mt-2">
                                                                                        <div
                                                                                            class="col-md-8 text-brown_activity font-weight-bold">
                                                                                            Estimated Total **
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-md-4 text-brown_activity text-right font-weight-bold text-nowrap">
                                                                                            <p class="text-nowrap text-center">
                                                                                                $ {{number_format($activity->estimate,2)}}</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 ">
                                                                                <div class="bg-white current-inner-box"
                                                                                     style="margin-top: 2%;">
                                                                                    <div class="row m-0">
                                                                                        <div
                                                                                            class="col-lg-2 col-md-2 col-sm-2">
                                                                                            <img
                                                                                                style="width: 30px;height: 30px"
                                                                                                src="{{asset('frontEnd/icon/icons8-calendar-100.png')}}">
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-10 col-md-10 col-sm-10">
                                                                                            <p class="service-type activitymain_Top">
                                                                                                Date & Time</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row m-0">
                                                                                        <div
                                                                                            class="col-lg-12 col-md-12 col-sm-12">
                                                                                            <p class="text-brown_activity_date">{{\Carbon\Carbon::parse($activity->selectedDate)->format('D,d M Y')}}
                                                                                                <span
                                                                                                    class="text-brown_activity_time">{{\Carbon\Carbon::parse($activity->timeslot->start_time)->format('g:i A')}} - {{\Carbon\Carbon::parse($activity->timeslot->end_time)->format('g:i A')}}</span>
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <hr class="class-2">
                                                                                    <p class="text-black_3">
                                                                                        <b>Instructions for
                                                                                            Technician</b><br>
                                                                                        <span
                                                                                            style="padding-left: 30px">{{!empty($activity->instruction)?$activity->instruction:'-'}}</span>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 ">
                                                                                <div class="bg-white current-inner-box"
                                                                                     style="margin-top: 2%;">
                                                                                    <div class="row m-0">
                                                                                        <div
                                                                                            class="col-lg-2 col-md-2 col-sm-2">
                                                                                            <img
                                                                                                style="width: 30px;height: 30px"
                                                                                                src="{{asset('frontEnd/icon/icons8-marker-100.png')}}">
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-10 col-md-10 col-sm-10">
                                                                                            <p class="service-type activitymain_Top">
                                                                                                Location</p>
                                                                                            @php $address =$activity->address @endphp
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row m-0">
                                                                                        <div
                                                                                            class="col-lg-12 col-md-12 col-sm-12">
                                                                                            <p class="text-brown_activity_date">{{$address->address_1.','.$address->address_2.','.$address->address_3}}
                                                                                                <span
                                                                                                    class="text-brown_activity_time">{{$address->postal_code}}</span>
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <hr class="class-2">
                                                                                    <p class="text-black_3">
                                                                                        <b>Directions for Technician</b><br>
                                                                                        <span
                                                                                            style="padding-left: 30px">{{!empty($activity->directions_for_technician)?$activity->directions_for_technician:'-'}}</span>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12">

                                                                            </div>
                                                                            {{--                                                                                <div class="col-lg-4 col-md-12 col-sm-12">--}}
                                                                            {{--                                                                                    <button type="submit" name="requestSummary" class="btn-brown_activity_document mt-3"><img class="requestSummary_img" src="{{asset('frontEnd/icon/50px/icons8-agreement-50.png')}}">VIEW REQUEST SUMMARY</button>--}}
                                                                            {{--                                                                                </div>--}}
                                                                            <div class="col-lg-4 col-md-12 col-sm-12">
                                                                                <div class="row">
                                                                                    {{--                                                                                        <div class="col-lg-6 col-md-6 col-sm-6">--}}
                                                                                    {{--                                                                                            <input type="button" name="reshedule" class="btn-brown_activity_reshedule mt-3" value="RESHEDULE" />--}}
                                                                                    {{--                                                                                        </div>--}}
                                                                                    <div
                                                                                        class="col-lg-6 col-md-6 col-sm-6">
                                                                                        {!! Form::open(['route' => ['order.cancel',$activity->id], 'method' => 'post','id'=>'frmCan']) !!}
                                                                                        <button onclick="orderCancel()"
                                                                                               type="button" class="btn-brown_activity_cancel mt-3">
                                                                                            Cancel
                                                                                        </button>
                                                                                        {!! Form::close() !!}
                                                                                    </div>
                                                                                    <div
                                                                                        class="col-lg-6 col-md-6 col-sm-6">
                                                                                        <button
                                                                                            onclick="scheduleOrder()"
                                                                                            type="submit"
                                                                                            name="cancel"
                                                                                            class="btn-brown_activity_cancel mt-3"
                                                                                            value="CANCEL">Re-schedule
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        @endif
                                                    @endforeach
                                                </div>

                                                <div class="row">
                                                    {{--                                                    <div class="col-lg-6 col-md-6 col-sm-6">--}}
                                                    {{--                                                        <input type="reset" name="back" class="btn-brown_3nd mt-3"--}}
                                                    {{--                                                               value="BACK"/>--}}
                                                    {{--                                                    </div>--}}
                                                    {{--                                                    <div class="col-lg-6 col-md-6 col-sm-6">--}}

                                                    {{--                                                    </div>--}}
                                                </div>


                                            </div>


                                            <div class="tab-pane fade" id="History" role="tabpanel"
                                                 aria-labelledby="History-tab">
                                                <div class="panel-group" id="accordion" role="tablist"
                                                     aria-multiselectable="true">

                                                    @foreach($activities as $activity)
                                                        @if ($activity->status == 'cancelled' || $activity->status == 'completed' )
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed" role="button"
                                                                           data-toggle="collapse"
                                                                           data-parent="#accordion"
                                                                           href="#collapse{{$activity->id}}"
                                                                           aria-expanded="false"
                                                                           aria-controls="collapse{{$activity->id}}">
                                                                            <div class="row">
                                                                                <div class="col-lg-3 col-md-3 col-sm-3">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-2 col-md-2 col-sm-2">
                                                                                            <img
                                                                                                class="activityUpcoming_img_sm"
                                                                                                src="{{asset('upload/frontEnd/icon/'.$activity->category->category_image)}}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <p class="activityUpcoming_mainHead">{{$activity->category->category_name}}</p>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <p class="activityUpcoming_subHead">{{$activity->service->name}}
                                                                                            - {{!empty($activity->itemtype)?$activity->itemtype->itemtypeid:''}} - {{!empty($activity->item)?$activity->item->item_id:''}}</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-3 col-md-3 col-sm-3">
                                                                                    <div class="row">
                                                                                        <p class="activityUpcoming_job">{{$activity->job_id}}</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-3 col-md-3 col-sm-3">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-2 col-md-2 col-sm-2">
                                                                                            <img
                                                                                                class="activityUpcoming_img_sm"
                                                                                                src="{{asset('frontEnd/icon/50px/icons8-calendar-50.png')}}">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <p class="activityUpcoming_mainHead_date">{{\Carbon\Carbon::parse($activity->selectedDate)->format('D,d M Y')}}</p>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <p class="activityUpcoming_subHead_time p-0 text-center">{{\Carbon\Carbon::parse($activity->timeslot->start_time)->format('g:i A')}}
                                                                                            - {{\Carbon\Carbon::parse($activity->timeslot->end_time)->format('g:i A')}}</p>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                                                    <h4 id="activityUpcoming_subHead_shedule"
                                                                                        class="font-weight-bolder header text-orange">
                                                                                        @if($activity->status=='cancelled')
                                                                                            CANCELLED
                                                                                        @elseif($activity->status=='completed')
                                                                                            COMPLETED
                                                                                        @endif
                                                                                    </h4>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapse{{$activity->id}}"
                                                                     class="panel-collapse collapse" role="tabpanel"
                                                                     aria-labelledby="headingOne">
                                                                    <div class="panel-body">

                                                                        <div class="row">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12">
                                                                                <div class="bg-white current-inner-box"
                                                                                     style="margin-top: 2%;">
                                                                                    <div class="row m-0">
                                                                                        <div
                                                                                            class="col-lg-5 col-md-5 col-sm-5 p-0">
                                                                                            <p class="activityPanel_body_topicMain">{{$activity->category->category_name}}</p>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-7 col-md-7 col-sm-7">
                                                                                            <p class="activityPanel_body_topicSub">{{$activity->service->name}}
                                                                                              {{!empty($activity->itemtype)?$activity->itemtype->itemtypeid:''}}  - {{!empty($activity->item)?$activity->item->item_id:''}}</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <hr class="class-2">

                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-md-8 text-black_activity">
                                                                                            Inspection <span
                                                                                                class="text-black_activity">[waived]</span>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-md-4 text-black_activity text-right text-nowrap">
                                                                                            <p class="text-nowrap text-center">
                                                                                                $ {{number_format($activity->inspection_charge,2)}}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mt-2">
                                                                                        <div
                                                                                            class="col-md-8 text-black_activity">{{$activity->service->name}}</div>
                                                                                        <div
                                                                                            class="col-md-4 text-black_activity text-right text-nowrap">
                                                                                            <p class="text-nowrap text-center">
                                                                                                $ {{number_format($activity->service_charge,2)}}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row mt-2">
                                                                                        <div
                                                                                            class="col-md-8 text-black_activity">
                                                                                            Material <span
                                                                                                class="text-black_activity">[{{!empty($activity->item)?$activity->item->item_id:''}}]</span>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-md-4 text-black_activity text-right text-nowrap">
                                                                                            <p class="text-nowrap text-center">
                                                                                                $ {{number_format($activity->material_charge,2)}}</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <hr class="class-3">
                                                                                    <div class="row mt-2">
                                                                                        <div
                                                                                            class="col-md-8 text-brown_activity font-weight-bold">
                                                                                            Estimated Total **
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-md-4 text-brown_activity text-right font-weight-bold text-nowrap">
                                                                                            <p class="text-nowrap text-center">
                                                                                                $ {{number_format($activity->estimate,2)}}</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 ">
                                                                                <div class="bg-white current-inner-box"
                                                                                     style="margin-top: 2%;">
                                                                                    <div class="row m-0">
                                                                                        <div
                                                                                            class="col-lg-2 col-md-2 col-sm-2">
                                                                                            <img
                                                                                                style="width: 30px;height: 30px"
                                                                                                src="{{asset('frontEnd/icon/icons8-calendar-100.png')}}">
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-10 col-md-10 col-sm-10">
                                                                                            <p class="service-type activitymain_Top">
                                                                                                Date & Time</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row m-0">
                                                                                        <div
                                                                                            class="col-lg-12 col-md-12 col-sm-12">
                                                                                            <p class="text-brown_activity_date">{{\Carbon\Carbon::parse($activity->selectedDate)->format('D,d M Y')}}
                                                                                                <span
                                                                                                    class="text-brown_activity_time">{{\Carbon\Carbon::parse($activity->timeslot->start_time)->format('g:i A')}} - {{\Carbon\Carbon::parse($activity->timeslot->end_time)->format('g:i A')}}</span>
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <hr class="class-2">
                                                                                    <p class="text-black_3">
                                                                                        <b>Instructions for
                                                                                            Technician</b><br>
                                                                                        <span
                                                                                            style="padding-left: 30px">{{!empty($activity->instruction)?$activity->instruction:'-'}}</span>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-12 col-sm-12 ">
                                                                                <div class="bg-white current-inner-box"
                                                                                     style="margin-top: 2%;">
                                                                                    <div class="row m-0">
                                                                                        <div
                                                                                            class="col-lg-2 col-md-2 col-sm-2">
                                                                                            <img
                                                                                                style="width: 30px;height: 30px"
                                                                                                src="{{asset('frontEnd/icon/icons8-marker-100.png')}}">
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-10 col-md-10 col-sm-10">
                                                                                            <p class="service-type activitymain_Top">
                                                                                                Location</p>
                                                                                            @php $address =$activity->address @endphp
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row m-0">
                                                                                        <div
                                                                                            class="col-lg-12 col-md-12 col-sm-12">
                                                                                            <p class="text-brown_activity_date">{{$address->address_1.','.$address->address_2.','.$address->address_3}}
                                                                                                <span
                                                                                                    class="text-brown_activity_time">{{$address->postal_code}}</span>
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <hr class="class-2">
                                                                                    <p class="text-black_3">
                                                                                        <b>Directions for Technician</b><br>
                                                                                        <span
                                                                                            style="padding-left: 30px">{{!empty($activity->directions_for_technician)?$activity->directions_for_technician:'-'}}</span>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-lg-4 col-md-12 col-sm-12">

                                                                            </div>
                                                                            {{--                                                                                <div class="col-lg-4 col-md-12 col-sm-12">--}}
                                                                            {{--                                                                                    <button type="submit" name="requestSummary" class="btn-brown_activity_document mt-3"><img class="requestSummary_img" src="{{asset('frontEnd/icon/50px/icons8-agreement-50.png')}}">VIEW REQUEST SUMMARY</button>--}}
                                                                            {{--                                                                                </div>--}}

                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>


    <!-- FOTTER-MENU ---------------------->
@include('inc.fotter')
<!--/ FOTTER-MENU --------------------->

</div>
<script src="{{asset('frontEnd/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/popper/popper.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js')}}"
        integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
<script src="{{asset('frontEnd/js/custom.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>

    function orderCancel() {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Cancel Order',
            cancelButtonText: 'No!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $("#frmCan").submit();
            }
        })
    }


    function scheduleOrder() {
        Swal.fire('Please Contact System Administrator {{!empty($config_data->contact)?$config_data->contact:null}}')
    }
</script>
</body>
</html>
