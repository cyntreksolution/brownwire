<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <title>Brownwire.com</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="http://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

    <link rel="stylesheet" href="{{asset('frontEnd/plugins/bootstrap/css/bootstrap.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/font-awesome/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('frontEnd/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body class="pane scroller">
<div id="app">


    <!-- HEADER-MENU -->
@include('inc.topbar')
<!--/ HEADER-MENU -->

    <main>
        <!-- HOME MENU -->
        <div id="home"></div>
        <img class="backgroundImage_home" src="{{asset('frontEnd/images/back1.png')}}">
        <div class="container py-4">
            <div class="row mt-5">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                            <h1 id="header_trusted_service" class="text-black mt-5 font-weight-bolder landing-header">Your trusted services expert</h1>
                            <p class="text-black landing_desc_new">
                                “Be it a painting touch up in the living room, leaking pipe and bathroom or kitchen sink choke clearnce,one place for you to get it done.-customer 1”
                            </p>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row marginSubtopic">
                                    <h3 id="header_trusted_service_1" class="text-black mt-5 font-weight-bolder landing-header">Get a fix right away.</h3>
                                    <div class="float-right text-right mt-1 bookBtnArea">
                                        <a href="/service" class="btn text-white btn-brown btn-booknow GTbook">BOOK NOW</a>
                                        <p class="text-brown m-0 landing-p">Convenient.</p>
                                        <p class="text-brown m-0 landing-p">Affordable.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 mt-4">
                            <div class="container mt-5">
                                <h3 class="text-orange  font-weight-bolder homeSubHeader">In simple steps ...</h3>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <h4 class="text-black chData">Choose</h4>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 mt-3">
                                        <p class="text-brown chData_sub">from list of services</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <h4 class="text-black chData">Add Info</h4>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 mt-3">
                                        <p class="text-brown chData_sub">and details of problem</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <h4 class="text-black chData">Shedule</h4>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 mt-3">
                                        <p class="text-brown chData_sub">appointment date & time</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <h4 class="text-black chData">Confirm</h4>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 mt-3">
                                        <p class="text-brown chData_sub">service request</p>
                                    </div>
                                </div>

                                <h3 class="text-orange font-weight-bolder homeSubHeader">Easy Payment Options*</h3>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <h4 class="text-black payText_1">Pay using</h4>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 mt-3">
                                        <h4 class="text-black payText_2">Cash or PayNow</h4>
                                        <p class="text-brown payText_3">*after work is completed.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ////HOME MENU ---------------------->
            <!-- ////HOME MENU -->


            <!-- ABOUT MENU -->
        {{--            <div id="about"></div>--}}
        {{--            <div class="row mt-5" style="margin-top: 12% !important;">--}}
        {{--                <div class="col-lg-12 p-md-0">--}}
        {{--                    <div class="row">--}}
        {{--                        <div class="col-md-12">--}}
        {{--                            <h3 id="Title_for_menus"--}}
        {{--                                class="font-weight-bolder header text-brown title mt-3 text-sm-center">ABOUT US</h3>--}}
        {{--                            <p style="font-size: 20px !important;text-align:right;margin-right: 2.7% !important;"--}}
        {{--                               class="text-black title_services">--}}
        {{--                                We provide reliable homecare and facility management services at affordable--}}
        {{--                                prices through use of technology offering convenience to our customers.--}}
        {{--                            </p>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <div class="row" style="margin-top: 3%">--}}
        {{--                <div class="col-lg-4 col-md-4 col-sm-4 text-sm-center mb-sm-3">--}}
        {{--                    <div class="circle">--}}
        {{--                        <div class="circle-inner">--}}
        {{--                            <h2 class="text-brown text-center font-weight-bolder counter">4.0</h2>--}}
        {{--                            <p class="text-center text-black title_services">Average Rating</p>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}

        {{--                <div class="col-lg-4 col-md-4 col-sm-4 text-lg-center mb-sm-3">--}}
        {{--                    <div class="circle">--}}
        {{--                        <div class="circle-inner">--}}
        {{--                            <h2 class="text-brown text-center font-weight-bolder counter">4</h2>--}}
        {{--                            <p class="text-center text-black title_services">Customers Served</p>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--                <div class="col-lg-4 col-md-4 col-sm-4 text-lg-right mb-sm-3">--}}
        {{--                    <div class="circle">--}}
        {{--                        <div class="circle-inner">--}}
        {{--                            <h2 class="text-brown text-center font-weight-bolder counter">7</h2>--}}
        {{--                            <p class="text-center text-black title_services">Categories</p>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        <!--///// ABOUT MENU -->


            <!--SERVICES MENU -->
            <div id="services"></div>

            <div class="row mt-5" style="margin-top: 12% !important;">
                <div class="col-lg-12 p-md-0">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 id="Title_for_menus_services"
                                class="font-weight-bolder header text-brown title mt-3 text-sm-center">OUR SERVICES</h3>
                        </div>
                    </div>
                </div>
            </div>
            {{--                <div class="row">--}}
            {{--                    <div class="col-md-10 p-0">--}}
            {{--                        <select class="form-control search-input w-100" name="size">--}}
            {{--                            <option selected="selected" value="">ceiling fan installation</option>--}}
            {{--                            <option value="L">Large</option>--}}
            {{--                            <option value="S">Small</option>--}}
            {{--                        </select>--}}
            {{--                    </div>--}}
            {{--                    <div class="col-md-2 p-0">--}}
            {{--                        <button class="btn btn-search ">More</button>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            <div class="row mt-5">
                {{--                <div class="owl-carousel owl-theme owl-loaded owl-drag" id="services-2">--}}
                {{--                    <div class="owl-stage-outer">--}}
                {{--                        <div class="owl-stage">--}}
                @foreach($data as $value=>$key)
                    @if($key->is_active==1)
                        {{--                                    <div class="owl-item">--}}
                        {{--                                        <div class="item">--}}
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-md-1">
                            <div class="box-inner">
                                <a href="{{route('service.select',[$key->id])}}" style="text-decoration: none">
                                    <div class="box brown-box">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <img class="servicesImg" src="{{asset('upload/frontEnd/icon/'.$key->category_image)}}">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <p class="text-brown servicesBoxTopic">{{$key->category_name}}</p>
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <i class="fa fa-chevron-circle-right text-brown fa-2x float-right"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>



                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                    @endif
                @endforeach
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>

            {{--            <div class="row mt-5 justify-content-center">--}}
            {{--                @foreach($service as $value=>$key)--}}
            {{--                    <div class="col-lg-3 col-md-3 col-sm-3 p-md-0">--}}
            {{--                        <a href="{{route('service.select',[$key->id])}}" style="text-decoration: none">--}}
            {{--                            <div class="box blue-box2">--}}
            {{--                                <div class="circle-blue-sm">--}}
            {{--                                    <img style="width: 100px;height: 100px"--}}
            {{--                                         src="{{asset('frontEnd/images/'.$key->service_image)}}">--}}
            {{--                                </div>--}}
            {{--                                <div class="header text-black mt-3">    {{$key->name}}</div>--}}
            {{--                                <p class="services_text text-black">--}}
            {{--                                    {{$key->description}}--}}
            {{--                                </p>--}}
            {{--                                <div class="row">--}}
            {{--                                    <div class="col-md-12">--}}
            {{--                                        <center>--}}
            {{--                                            <button class="btn btn-brown mt-3 know">More</button>--}}
            {{--                                        </center>--}}
            {{--                                    </div>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </a>--}}
            {{--                    </div>--}}



            {{--                @endforeach--}}

            {{--            </div>--}}


        <!--HOW IT WORKS MENU -->
            {{--            <div id="how"></div>--}}
            {{--            <div class="row w-100" style="padding-top: 10% !important;">--}}
            {{--                <div class="col-md-10">--}}
            {{--                    <h3 id="Title_for_howIt_menu" class="font-weight-bolder header text-brown title mt-3 text-sm-left">--}}
            {{--                        HOW IT WORKS</h3>--}}
            {{--                    <h4 class="text-black font-weight-bold title-2">In just 4 simple steps.</h4>--}}
            {{--                </div>--}}
            {{--                <div class="col-md-2 text-lg-right text-center">--}}
            {{--                    <button class="btn btn-brown btn-booknow">BOOK NOW</button>--}}
            {{--                </div>--}}
            {{--            </div>--}}

            {{--            <div class="row mt-5 w-100">--}}

            {{--                <div class="col-lg-3 step">--}}
            {{--                    <div class="row text-lg-left text-center">--}}
            {{--                        <div class="col-md-4">--}}
            {{--                            <div class="brown-circle">--}}
            {{--                                <div class="number-circle">--}}
            {{--                                    <p class="font-weight-bolder text-orange counter">1</p>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                        <div class="col-md-8">--}}
            {{--                            <p class="header text-brown font-weight-bolder m-0 title">Choose</p>--}}
            {{--                            <p class="text-black desc how_it_text">from list of services </p>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}

            {{--                <div class="col-lg-3 step">--}}
            {{--                    <div class="row text-lg-left text-center">--}}
            {{--                        <div class="col-md-4">--}}
            {{--                            <div class="brown-circle">--}}
            {{--                                <div class="number-circle">--}}
            {{--                                    <p class="font-weight-bolder text-orange counter">2</p>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                        <div class="col-md-8">--}}
            {{--                            <p class="header text-brown font-weight-bolder m-0 title">Add Info</p>--}}
            {{--                            <p class="text-black desc how_it_text">details of problem </p>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}

            {{--                <div class="col-lg-3 step">--}}
            {{--                    <div class="row text-lg-left text-center">--}}
            {{--                        <div class="col-md-4">--}}
            {{--                            <div class="brown-circle">--}}
            {{--                                <div class="number-circle">--}}
            {{--                                    <p class="font-weight-bolder text-orange counter">3</p>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                        <div class="col-md-8">--}}
            {{--                            <p class="header text-brown font-weight-bolder m-0 title">Schedule</p>--}}
            {{--                            <p class="text-black desc how_it_text"> date & time </p>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}

            {{--                <div class="col-lg-3 step">--}}
            {{--                    <div class="row text-lg-left text-center">--}}
            {{--                        <div class="col-md-4">--}}
            {{--                            <div class="brown-circle">--}}
            {{--                                <div class="number-circle">--}}
            {{--                                    <p class="font-weight-bolder text-orange counter">4</p>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                        <div class="col-md-8">--}}
            {{--                            <p class="header text-brown font-weight-bolder m-0 title">Confirm </p>--}}
            {{--                            <p class="text-black desc how_it_text">service request</p>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--        </div>--}}


            {{--        <div class="light-pink-row">--}}
            {{--            <div class="container">--}}
            {{--                <div class="row m-0">--}}
            {{--                    <div class="col-lg-8">--}}
            {{--                        <div class="text-orange title_Easy_Payment">Easy Payment Options</div>--}}
            {{--                        <div class="text-black desc text-right title_Easy_Payment_Sub">after completion of the service--}}
            {{--                            **--}}
            {{--                        </div>--}}
            {{--                    </div>--}}

            {{--                    <div class="col-lg-4 mt-3 mb-3">--}}
            {{--                        <div class="row pay text-center">--}}
            {{--                            <div class="col-lg-4  col-sm-4 col-4 p-0">--}}
            {{--                                <div class="number-circle-p">--}}
            {{--                                    <img src="{{asset('frontEnd/images/pay1.png')}}">--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                            <div class="col-lg-4 col-sm-4  col-4 p-0">--}}
            {{--                                <div class="number-circle-p">--}}
            {{--                                    <img src="{{asset('frontEnd/images/pay2.png')}}">--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                            <div class="col-lg-4 col-sm-4 col-4  p-0">--}}
            {{--                                <div class="number-circle-p">--}}
            {{--                                    <img src="{{asset('frontEnd/images/pay3.png')}}">--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--        </div>--}}


            <div class="container">
                <!--CONTACT US MENU -->
                <div id="contact">
                    <div class="row mr-0 mt-5">
                        <div class="col-md-12">
                            <h3 class="font-weight-bolder header text-brown mt-3 title text-sm-left">CONTACT US</h3>
                        </div>
                    </div>
                    <div class="blog mb-5">

                        <h4 class="text-black font-weight-bold text-center mt-9 mb-5 subtopic">Send us a message</h4>
                        {!! Form::open(['route' => 'contact.data', 'method' => 'post']) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control input-border" name="name" value="" placeholder="Name"
                                           required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control input-border" name="email" value="" placeholder="Email"
                                           required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="number" class="form-control input-border" name="phone"
                                           value="" placeholder="Phone" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control input-border" name="service" value="" required>
                                        <option disabled selected>Select Category</option>

                                        @foreach($data as $item)
                                            <option value="{{$item->id}}">{{$item->category_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <textarea class="form-control input-border" name="message" rows="5"
                                          placeholder="Write your message with details here" required></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <center>
                                    <button class="btn btn-brown mt-3">Send Message</button>
                                </center>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
            <div class="com-md-1 float-right m-3">
                <a class="p-4" href="#home"><i class="fa fa-arrow-circle-up text-brown fa-3x"></i></a>
            </div>
            <style>
                html {
                    scroll-behavior: smooth;
                }
            </style>
    </main>

    <!-- FOTTER-MENU -->
@include('inc.fotter')
<!--/ FOTTER-MENU -->

    <!--/////CONTACT US MENU -->
</div>
<script src="{{asset('frontEnd/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/popper/popper.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js')}}"></script>
<script src="{{asset('frontEnd/js/custom.js')}}"></script>
</body>

</html>
