<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <title>Brownwire.com</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="http://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/bootstrap/css/bootstrap.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('frontEnd/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css')}}" integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css')}}" integrity="sha256-kksNxjDRxd/5+jGurZUJd1sdR2v+ClrCl3svESBaJqw=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>


<body class="pane scroller">
    <div id="app">
    

    <!-- HEADER-MENU ---------------------->
    @include('inc.topbar')
    <!--/ HEADER-MENU --------------------->	

        <main style="padding-bottom: 6% !important;">
            <div class="row m-0">
            <div class="col-md-9 mt-5" style="padding-top: 2% !important;margin-top: 7% !important;">
                <div class="row">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-12 p-md-0">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 id="head_title_plumbing" class="font-weight-bolder header text-brown title mt-3">Select work type</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="card card-1">                        
                                        <label class="containerData">
                                            <input type="radio" checked="checked" name="type">
                                            <span class="checkmark"></span>
                                        </label>
                                        <p class="text_Data">Home</p>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card card-1">
                                        <label class="containerData">
                                            <input type="radio"  name="type">
                                            <span class="checkmark"></span>
                                        </label>
                                        <p class="text_Data">Home</p>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card card-1">
                                        <label class="containerData">
                                            <input type="radio"  name="type">
                                            <span class="checkmark"></span>
                                        </label>
                                        <p class="text_Data">Home</p>
                                    </div>
                                </div>
                            </div>
                      
                            {{----------------------- //////////// --------------}}
                            <div class="col-lg-12 p-md-0">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 id="head_title_plumbing" class="font-weight-bolder header text-brown title mt-3">Where do you need the service?</h4>
                                    </div>
                                </div>
                            </div>
                     
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="card card-1">                        
                                        <label class="containerData">
                                            <input type="radio" checked="checked" name="service">
                                            <span class="checkmark"></span>
                                        </label>
                                        <p class="text_Data">Pantry</p>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="card card-1">
                                        <label class="containerData">
                                            <input type="radio"  name="service">
                                            <span class="checkmark"></span>
                                        </label>
                                        <p class="text_Data">Kitchen</p>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="card card-1">
                                        <label class="containerData">
                                            <input type="radio"  name="service">
                                            <span class="checkmark"></span>
                                        </label>
                                        <p class="text_Data">Pantry</p>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="card card-1">
                                        <label class="containerData">
                                            <input type="radio"  name="service">
                                            <span class="checkmark"></span>
                                        </label>
                                        <p class="text_Data">Pantry</p>
                                    </div>
                                </div>
                            </div>
                   
                            


                            {{----------------------- //////////// --------------}}
                            <div class="col-lg-12 p-md-0">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 id="head_title_plumbing" class="font-weight-bolder header text-brown title mt-3">What needs a fix?</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="card card-1">                        
                                        <label class="containerData">
                                            <input type="radio" checked="checked" name="fix">
                                            <span class="checkmark"></span>
                                        </label>
                                        <p class="text_Data">Sink Tap</p>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="card card-1">
                                        <label class="containerData">
                                            <input type="radio"  name="fix">
                                            <span class="checkmark"></span>
                                        </label>
                                        <p class="text_Data">Sink Tap</p>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="card card-1">
                                        <label class="containerData">
                                            <input type="radio"  name="fix">
                                            <span class="checkmark"></span>
                                        </label>
                                        <p class="text_Data">Sink</p>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="card card-1">
                                        <label class="containerData">
                                            <input type="radio"  name="fix">
                                            <span class="checkmark"></span>
                                        </label>
                                        <p class="text_Data">Sink</p>
                                    </div>
                                </div>
                            </div>



                            {{----------------------- //////////// --------------}}
                            <div class="col-lg-12 p-md-0">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 id="head_title_plumbing" class="font-weight-bolder header text-brown title mt-3">Select issue</h4>
                                    </div>
                                </div>

                                <div class="radio-toolbar">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <input type="radio" id="Leaking" name="radioFruit"  checked>
                                            <label for="Leaking">Leaking</label>
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="radio" id="Clogged" name="radioFruit">
                                            <label for="Clogged">Clogged</label>
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="radio" id="Broken" name="radioFruit">
                                            <label for="Broken">Broken</label> 
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="radio" id="Rusty" name="radioFruit">
                                            <label for="Rusty">Rusty</label> 
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="radio" id="LessWaterFlow" name="radioFruit">
                                            <label for="LessWaterFlow">Less water flow</label>
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="radio" id="TooOld" name="radioFruit">
                                            <label for="TooOld">Too old</label>
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="radio" id="NewTap" name="radioFruit">
                                            <label for="NewTap">New tap</label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                             {{----------------------- //////////// --------------}}
                            <div class="col-lg-12 p-md-0">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 id="head_title_plumbing" class="font-weight-bolder header text-brown title mt-3">What needs to be done?</h4>
                                    </div>
                                </div>

                               
                                <div class="radio-toolbar">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <input type="radio" id="REPLACEMENT" name="radioFruit1"  checked>
                                            <label for="REPLACEMENT">REPLACEMENT</label>
                                            <p style="font-size: 10px;color:orange;text-align: right;font-weight: bold;">Recommended.</p>
                                            <p style="font-size: 10px;text-align: right;margin-top: -7%;">
                                                Based on provided info.<br>
                                                Subject to on-site assessment
                                            </p>
                                        </div>
                                    
                                        <div class="col-lg-4">
                                            <input type="radio" id="INSTALLATION" name="radioFruit1">
                                            <label for="INSTALLATION">INSTALLATION</label>
                                        </div>
                                    
                                        <div class="col-lg-4">
                                            <input type="radio" id="REPAIR" name="radioFruit1">
                                            <label for="REPAIR">REPAIR</label> 
                                        </div>
                                    </div>
                                </div>
                            </div>


                             {{----------------------- //////////// --------------}}
                             <div class="col-lg-12 p-md-0">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 id="head_title_plumbing" class="font-weight-bolder header text-brown title mt-3">Do you have a replacement tap?</h4>
                                    </div>
                                </div>

                               
                                <div class="radio-toolbar">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <input type="radio" id="YES" name="radioFruit2"  checked>
                                            <label for="YES">YES</label>
                                        </div>
                                    
                                        <div class="col-lg-3">
                                            <input type="radio" id="NO" name="radioFruit2">
                                            <label for="NO">NO</label>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            {{----------------------- //////////// --------------}}
                            <div class="col-lg-12 p-md-0">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 id="head_title_plumbing" class="font-weight-bolder header text-brown title mt-3">Do you have a replacement tap?</h4>
                                    </div>
                                </div>

                               
                                <div class="radio-toolbar-round">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <input type="radio" id="TODAY" name="radioFruit3"  checked>
                                            <label for="TODAY">TODAY</label>
                                        </div>
                                    
                                        <div class="col-lg-3">
                                            <input type="radio" id="Day1" name="radioFruit3">
                                            <label for="Day1">THU 01</label>
                                        </div>

                                        <div class="col-lg-3">
                                            <input type="radio" id="Day2" name="radioFruit3">
                                            <label for="Day2">FRI 02</label>
                                        </div>

                                        <div class="col-lg-3">
                                            <input type="radio" id="Day3" name="radioFruit3">
                                            <label for="Day3">SAT 03</label>
                                        </div>
                                    </div>
                                </div><br>


                                <div class="radio-toolbar-time">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <input type="radio" id="10am_12pm" name="radioFruit4"  checked>
                                            <label for="10am_12pm">10am-12pm</label>
                                        </div>
                                    
                                        <div class="col-lg-4">
                                            <input type="radio" id="12pm_2pm" name="radioFruit4">
                                            <label for="12pm_2pm">12pm-2pm</label>
                                        </div>

                                        <div class="col-lg-4">
                                            <input type="radio" id="2pm_4pm" name="radioFruit4">
                                            <label for="2pm_4pm">2pm-4pm</label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-2"></div>
                                            <div class="col-lg-4">
                                                <input type="radio" id="4pm_6pm" name="radioFruit4">
                                                <label for="4pm_6pm">4pm-6pm</label>
                                            </div>

                                            <div class="col-lg-4">
                                                <input type="radio" id="6pm_8pm" name="radioFruit4">
                                                <label for="6pm_8pm">6pm-8pm</label>
                                            </div>
                                        <div class="col-lg-2"></div>
                                    </div>
                                </div>
                            </div>


                            
                             {{----------------------- //////////// --------------}}
                             <div class="row">
                                <div class="col-lg-12 p-md-0">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 id="head_title_plumbing" class="font-weight-bolder header text-brown title mt-3">Type in your postal code to add location of service</h4>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-12 p-md-0">
                                    <form method="POST" action="#" accept-charset="UTF-8">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control input-border text-orange font-weight-bold" name="name" value="400024" placeholder="Name" required>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input class="form-control input-border" name="phone" value="400024 24 Eunos Crescent" placeholder="Phone" required>
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control input-border" name="phone" value="#05-3025" placeholder="Phone" required>
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control input-border" name="phone" value="Singapore" placeholder="Phone" required>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                             </div>

                        </div>
                    </div>
                </div>
            </div>





            <div class="col-md-3 bg-brown">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-black">Current Request</p>
                    </div>
                </div>
                    <div class="bg-white current-inner-box">
                        <div class="row m-0">
                            <div class="col-md-2 p-0">
                                <i class="fa fa-check-circle text-brown fa-3x"></i>
                            </div>
                            <div class="col-md-8">
                                <p class="service-type mx-auto my-2"> Replacement</p>
                                <p class="right_panel_text">Plumbing</p>
                                <p class="right_panel_text">Kitchen - Sink Tap</p>
                            </div>
                            <div class="col-md-2">
                                <i class="fa fa-edit"></i>
                            </div>
                        </div>
                        <hr class="class-1">

                        <div class="col-md-12 mt-4">
                            <div class="row mt-2">
                                <div class="col-md-9 text-black_1">Inspection <span class="text-brown">[waived]</span></div>
                                <div class="col-md-3 text-black_1 text-right">$0.00</div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-9 text-black_1">Replacement</div>
                                <div class="col-md-3 text-black_1 text-right">$50.00</div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-9 text-black_1">Material <span class="text-brown">[sink tap]</span></div>
                                <div class="col-md-3 text-black_1 text-right">$25.00</div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-9 text-brown font-weight-bold">Estd. Total **</div>
                                <div class="col-md-3 text-brown text-right font-weight-bold">$75.00</div>
                            </div>

                        </div>

                        <hr>

                        <div class="">
                            <p class="text-black_2">
                            ** Your technician will fully inspect your area of concern and update you on recommended service should there be anything diﬀerent from your original service request.
                            <p>
                            <p class="text-black_2">
                                The inspecon is free if you move forward with your original service request or with our recommended service.
                            </p>
                            <p class="text-black_2">
                                You are also free to decline either service oﬀered but a fee of $25.00 will be charged for the inspecon.</p>
                            </p>

                            <div class="row">
                                <div class="col-md-2 text-right p-0">
                                    <label class="containerCheckbox_data">
                                        <input type="checkbox" checked="checked">
                                        <span class="checkmarkCheckbox_data"></span>
                                    </label>
                                </div>
                                <div class="col-md-10">
                                    <p class="text-black_2">
                                        I accept the above charges and services
                                        from Brownwires.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="bg-white current-inner-box" style="margin-top: 3%;">
                        <div class="row m-0">
                            <div class="col-md-2">
                                <i class="fa fa-check-circle text-brown fa-3x"></i>
                            </div>
                            <div class="col-md-6">
                                <p class="service-type mx-auto my-2">Date & Time</p>
                                <p class="right_panel_text_3">Thursday, 01st March 2020</p>
                            </div>
                            <div class="col-md-4">
                                <p class="right_panel_text_2">4PM-6PM</p>
                            </div>
                        </div>
                        <hr class="class-1">
                        <p class="text-black_3">
                            <b>Instructions for Technician</b><br>
                            <span style="padding-left: 30px">Please call my door from the security gate.</span>
                        </p>
                    </div>

                    <div class="bg-white current-inner-box" style="margin-top: 3%;">
                        <div class="row m-0">
                            <div class="col-md-2">
                                <i class="fa fa-check-circle text-brown fa-3x"></i>
                            </div>
                            <div class="col-md-6">
                                <p class="service-type mx-auto my-2">Location</p>
                                <p class="right_panel_text_3">24 Eunos Crescent,#05-3025,SG</p>
                            </div>
                            <div class="col-md-4">
                                <p class="right_panel_text_2">400024</p>
                            </div>
                        </div>
                        <hr class="class-1">
                        <p class="text-black_3">
                            <b>Instructions for Technician</b><br>
                            <span style="padding-left: 30px">Please call my door from the security gate.</span>
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <center>
                                <button class="btn btn-brown mt-3">CONFIRM</button>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </main>



          <!-- FOTTER-MENU ---------------------->
          @include('inc.fotter')
          <!--/ FOTTER-MENU --------------------->
  
          <!--/////CONTACT US MENU -------------------------------->
      </div>
      <script src="{{asset('frontEnd/plugins/jquery/jquery.min.js')}}"></script>
      <script src="{{asset('frontEnd/plugins/popper/popper.min.js')}}"></script>
      <script src="{{asset('frontEnd/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
      <script src="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js')}}" integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
      <script src="{{asset('frontEnd/js/custom.js')}}"></script>
  </body>
  
  </html>