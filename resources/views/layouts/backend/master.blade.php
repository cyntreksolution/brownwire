<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Brownwire</title>
    @include('layouts.backend.partials.style')
</head>

<body>
<div class="container-scroller">
    @include('layouts.backend.partials.header')
    <div class="container-fluid page-body-wrapper">
        @include('layouts.backend.partials.sidebar')
        <div class="main-panel">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @yield('content')
            @include('layouts.backend.partials.footer')
        </div>
    </div>
</div>

@include('layouts.backend.partials.javascript')
@yield('script')
@if(session('success'))
    <script>
        $.toast({
            heading: 'Success',
            text: "{{session('success.message')}}",
            showHideTransition: 'slide',
            icon: 'success',
            loaderBg: '#f96868',
            position: 'top-right'
        })</script>
@elseif(session('error'))
    <script>
        $.toast({
            heading: 'Danger',
            text: "{{session('error.message')}}",
            showHideTransition: 'slide',
            icon: 'error',
            loaderBg: '#f2a654',
            position: 'top-right'
        })
    </script>
@elseif(session('warning'))
    Command: toastr["warning"]("{{session('warning.message')}} ", "{{session('warning.title')}}")
@elseif(session('info'))
    Command: toastr["info"]("{{session('info.message')}} ", "{{session('info.title')}}")
@endif
</body>


</html>

