<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="profile-image">
                    <img src="{{ asset('admin/images/faces/face5.jpg')}}" alt="image"/>
                </div>
                <div class="profile-name">
                    <p class="name">

                    </p>
                    <p class="designation">
                        Super Admin
                    </p>
                </div>
            </div>
        </li>
        <li class="nav-item">
            {{--            <a class="nav-link" href="{{route('admin.dashboard')}}">--}}
            <i class="fa fa-home menu-icon"></i>
            <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('contact.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Contacts</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('user.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Users</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('services.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Services</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('category.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Categories</span>
            </a>
        </li>

    <!--   <li class="nav-item">
            <a class="nav-link" href="{{route('space.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Spaces</span>
            </a>
        </li> -->

        <li class="nav-item">
            <a class="nav-link" href="{{route('itemtype.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Item Types</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('item.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Item</span>
            </a>
        </li>
    <!-- <li class="nav-item">
            <a class="nav-link" href="{{route('location.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Location Master</span>
            </a>
        </li> -->
        <li class="nav-item">
            <a class="nav-link" href="{{route('time_slot.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Time Slots</span>
            </a>
        </li>

    <!--   <li class="nav-item">
            <a class="nav-link" href="{{route('description.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Description Master</span>
            </a>
        </li>
 -->
    <!-- <li class="nav-item">
            <a class="nav-link" href="{{route('location_description.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Location Description Mapping</span>
            </a>
        </li> -->

{{--        <li class="nav-item">--}}
{{--            <a class="nav-link" href="{{route('immediate_issue.index')}}">--}}
{{--                <i class="fab fa-trello menu-icon"></i>--}}
{{--                <span class="menu-title">Immediate Issues</span>--}}
{{--            </a>--}}
{{--        </li>--}}
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link" href="{{route('payment_type.index')}}">--}}
{{--                <i class="fab fa-trello menu-icon"></i>--}}
{{--                <span class="menu-title">Payment Type</span>--}}
{{--            </a>--}}
{{--        </li>--}}
        <li class="nav-item">
            <a class="nav-link" href="{{route('issue.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Issues</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('item_mapping.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Item Mapping</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('issue_mapping.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Issue Mapping</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('cart.index')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Orders</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('app.config')}}">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Configurations</span>
            </a>
        </li>
    </ul>
</nav>
