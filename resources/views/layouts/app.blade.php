<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Brownwire.com</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}" />
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
        integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"
        integrity="sha256-kksNxjDRxd/5+jGurZUJd1sdR2v+ClrCl3svESBaJqw=" crossorigin="anonymous" />

</head>

<body>
    <div id="app">
        <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light-brown shadow-sm">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{--                <img src="images/brownwire.jpeg" width="100px">--}}
                    Brownwire.com
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav ml-auto menu">
                        <li class="nav-item ">
                            <a class="nav-link text-blue" href="/#home">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link text-blue" href="/#services">Services</a>
                        </li>
                        <li class="nav-item mr-2">
                            <a class="nav-link text-blue" href="/#contact">Contact Us</a>
                        </li>

                        <!-- Authentication Links -->
                        @guest
                        @if (Route::has('register'))
                        <li class="nav-item btn-align">
                            <a class="btn btn-brown mr-2" href="{{ route('register') }}">SIGN
                                UP</a>
                        </li>
                        @endif
                        <li class="nav-item btn-align">
                            <a class="btn btn-white" href="{{ route('login') }}">LOGIN</a>
                        </li>
                        @else
                        <li class="nav-item dropdown">
                            <a class="nav-item btn btn-normal btn-brown m-1" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <main>
            @yield('content')
        </main>
        <footer class="">
            <div class="container">

                <div class="row">
                    <div class="col-lg-4">
                        <h3 class="text-brown font-weight-bold">BROWNWIRES</h3>

                        <p class="fs-14 color-999 mt-3">Lorem ipsum dolor sit amet,Stet clita kasd gubergren,
                            no sea takimata sanctus est is the is the magna
                            aliquya.</p>
                    </div>

                    <div class="col-lg-8">
                        <h4 class="text-dark-blue">You can reach us</h4>
                        <div class="row mt-4">
                            <div class="col-lg-4 col-md-4">
                                <h6 class="text-brown">EMAIL</h6>
                                <p class="fs-14 text-blue mb-0">info@brownwires.com</p>
                                <p class="fs-14 text-blue mb-0">career.brownwires.com</p>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <h6 class="text-brown">MAKE CALL</h6>
                                <p class="fs-14 text-blue mb-0">+65 1234 5678</p>
                                <p class="fs-14 text-blue mb-0">+65 1234 5678</p>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <h6 class="text-brown">GET IN TOUCH</h6>
                                <p class="fs-14 text-blue mb-0">123/A, Hamburger City</p>
                                <p class="fs-14 text-blue mb-0">New York, USA</p>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row mt-md-5">
                    <div class="col-md-12">
                        <p class="text-center text-brown"><strong>www.brownwire.com All Rights Reserved.</strong> </p>
                    </div>
                </div>

            </div>
        </footer>
    </div>
    <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('plugins/popper/popper.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
        integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
    <script src="{{asset('js/custom.js')}}"></script>
</body>

</html>
