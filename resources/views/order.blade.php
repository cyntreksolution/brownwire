@extends('layouts.backend.master')
@section('content')

    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                {{$activity->job_id}}
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('cart.index')}}">orders</a></li>
                    <li class="breadcrumb-item active" aria-current="page"> {{$activity->job_id}}</li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">
                @php $address =$activity->address @endphp
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>User</label>
                            </div>
                            <div class="col-md-6">
                                {{$activity->user->name}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Mobile</label>
                            </div>
                            <div class="col-md-6">
                                {{$address->contact_no_1}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Category</label>
                            </div>
                            <div class="col-md-6">
                                {{$activity->category->category_name}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Service</label>
                            </div>
                            <div class="col-md-6">
                                {{$activity->service->name}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Job ID</label>
                            </div>
                            <div class="col-md-6">
                                {{$activity->job_id}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Selected Date</label>
                            </div>
                            <div class="col-md-6">
                                {{\Carbon\Carbon::parse($activity->selectedDate)->format('D,d M Y')}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Time Slot</label>
                            </div>
                            <div class="col-md-6">
                                {{\Carbon\Carbon::parse($activity->timeslot->start_time)->format('g:i A')}}
                                - {{\Carbon\Carbon::parse($activity->timeslot->end_time)->format('g:i A')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Status</label>
                            </div>
                            <div class="col-md-6">
                                <label class="text-uppercase ">{{$activity->status}}</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Item Type</label>
                            </div>
                            <div class="col-md-6">
                                {{!empty($activity->itemtype)?$activity->itemtype->itemtypeid:''}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Item</label>
                            </div>
                            <div class="col-md-6">
                                {{!empty($activity->item)?$activity->item->item_id:''}}
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Inspection Charge [waived]</label>
                            </div>
                            <div class="col-md-6">
                                $ {{number_format($activity->inspection_charge,2)}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Service Charge</label>
                            </div>
                            <div class="col-md-6">
                                $ {{number_format($activity->service_charge,2)}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Material Charge</label>
                            </div>
                            <div class="col-md-6">
                                $ {{number_format($activity->material_charge,2)}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Total Charge</label>
                            </div>
                            <div class="col-md-6">
                                $ {{number_format($activity->estimate,2)}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Instructions for
                                    Technician</label>
                            </div>
                            <div class="col-md-6">
                                {{!empty($activity->instruction)?$activity->instruction:'-'}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Directions for Technician</label>
                            </div>
                            <div class="col-md-6">
                                {{!empty($activity->directions_for_technician)?$activity->directions_for_technician:'-'}}
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Address</label>
                            </div>
                            <div class="col-md-6">
                                {{$address->address_1.','.$address->address_2.','.$address->address_3}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Postal Code</label>
                            </div>
                            <div class="col-md-6">
                                {{$address->postal_code}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>



@endsection
@section('script')
@endsection

