@extends('layouts.app')

@section('content')
    <div class="row m-0">

        <div class="col-md-9 mt-5" style="padding-top: 10rem !important;">
            <div class="row">
                <div class="col-md-2">
                    <p class="big-number text-brown font-weight-bolder">1</p>
                </div>
                <div class="col-md-10">
                    <div class="row">

                        <div class="item-box bg-blue p-3">
                            <div class="inner-box border-white">
                                <p class="title text-center text-white text-uppercase">KITCHEN</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-md-2">
                    <p class="big-number stroke-blue text-white font-weight-bolder">2</p>
                </div>
                <div class="col-md-10">
                    <div class="row">

                        <div class="item-box bg-white border-blue p-3">
                            <div class="inner-box border-blue">
                                <p class="title text-center text-blue text-uppercase">SINK TAP</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row mt-5">
                <div class="col-md-2">
                    <p class="big-number text-dark font-weight-bolder">3</p>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="circle-service">
                                <p class="text-center text-blue title">Installation</p>
                            </div>
                        </div>

                        <div class="col-md-4 text-center">
                            <div class="circle-service">
                                <p class="text-center text-blue title">Replacement</p>
                            </div>
                        </div>
                        <div class="col-md-4 text-right">
                            <div class="circle-service">
                                <p class="text-center text-blue title">Repair</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-3 bg-brown" style="padding-top: 9rem !important;">
            <p class="text-white">Current Request</p>
            <div class="bg-white current-inner-box">
                <div class="row m-0">
                    <div class="col-md-2 p-0">
                        <i class="fa fa-check-circle text-brown fa-4x"></i>
                    </div>
                    <div class="col-md-8">
                        <h5>Plumbing</h5>
                        <h6>Kitchen - Sink Tap</h6>
                    </div>
                    <div class="col-md-2">
                        <i class="fa fa-edit"></i>
                    </div>
                </div>

                <div class="row bg-brown">
                    <p class="service-type mx-auto my-2"> Replacement</p>
                </div>

                <div class="col-md-12 mt-4">

                    <div class="row mt-2">
                        <div class="col-md-9 text-blue">Inspection <span class="text-brown">[waived]</span></div>
                        <div class="col-md-3 text-blue text-right">$0.00</div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-9 text-blue">Replacement</div>
                        <div class="col-md-3 text-blue text-right">$50.00</div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-9 text-blue">Material <span class="text-brown">[sink tap]</span></div>
                        <div class="col-md-3 text-blue text-right">$25.00</div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-9 text-blue font-weight-bolder">Estd. Total **</div>
                        <div class="col-md-3 text-blue text-right font-weight-bolder">$75.00</div>
                    </div>

                </div>

                <hr>

                <div class="">
                    <p class="text-blue">** Your technician will fully inspect your area of concern and update you on recommended service should there be anything diﬀerent from your original service request.
                        The inspecon is free if you move forward with your original service request or with our recommended service.
                        You are also free to decline either service oﬀered but a fee of $25.00 will be charged for the inspecon.</p>

                    <div class="row">
                        <div class="col-md-2 text-right p-0">
                            <i class="fa fa-check-square text-success fa-2x"></i>
                        </div>
                        <div class="col-md-10">
                            <p class="text-blue">I accept the above charges and services  from Brownwires</p>
                        </div>
                    </div>
                </div>

                <div class="row m-0">
                    <div class="col-md-2 p-0">
                        <i class="fa fa-close text-danger fa-2x"></i>
                    </div>
                    <div class="col-md-10 p-0">
                        <p class="text-blue">Date & Time</p>
                    </div>
                </div>

                <div class="row m-0">
                    <div class="col-md-2 p-0">
                        <i class="fa fa-close text-danger fa-2x"></i>
                    </div>
                    <div class="col-md-10 p-0">
                        <p class="text-blue">Location</p>
                    </div>
                </div>


            </div>
        </div>

    </div>
@endsection
