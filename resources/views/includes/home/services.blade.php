<div id="services"></div>
<div class="row" style="padding-top: 10% !important;">
    <div class="col-md-10 mt-5 ml-auto">
        <h3 class="text-brown text-lg-right text-sm-center font-weight-bold title mb-4">OUR SERVICES</h3>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-md-9 p-0">
        {!! Form::select('size', ['L' => 'Large', 'S' => 'Small'], null,['class'=>'form-control search-input
        w-100','placeholder'=>'ceiling fan installation']) !!}
    </div>
    <div class="col-md-2 p-0">
        <button class="btn btn-search ">Search</button>
    </div>
</div>
<div class="row mt-5">

    <div class="owl-carousel owl-theme owl-loaded owl-drag" id="services-2">

        <div class="owl-stage-outer">
            <div class="owl-stage">
                @for($i=0;$i<6;$i++) <div class="owl-item">
                    <div class="item">
                        <div class="col-lg-12 p-md-0">
                            <a href="/single" style="text-decoration: none">
                                <div class="box blue-box">
                                    <div class="circle-sm">
                                    </div>
                                    <div class="header text-brown mt-3">Home Improvement</div>
                                    <p class="">Lorem ipsum dolor sit amet,Stet clita kasd
                                        lorem ipsum dolor sit amet. sed diam
                                        eirmod tempor dolore.
                                    </p>

                                </div>
                            </a>
                        </div>
                    </div>
            </div>
            @endfor
        </div>
    </div>
</div>

</div>


<div class="row mt-5 justify-content-center">

    @for($i=1;$i<4;$i++) <div class="col-lg-3 p-md-1">
        <div class="box-inner">
            <a href="/single" style="text-decoration: none">
                <div class="box brown-box">
                    <div class="circle-blue-sm"></div>
                    <div class="header text-brown mt-3">Home Improvement</div>
                    <p class="">Lorem ipsum dolor sit amet,Stet clita kasd
                        lorem ipsum dolor sit amet. sed diam
                        eirmod tempor dolore.
                    </p>
                </div>
            </a>
        </div>
</div>
@endfor

</div>