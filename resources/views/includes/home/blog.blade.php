<div class="row mr-0 mt-20">
    <div class="col-md-12">
        <h3 class="font-weight-bold text-brown text-lg-right text-sm-center title">BW BLOG</h3>
    </div>
</div>
<div class="blog">
    <h4 class="text-blue font-weight-bold text-center mt-10 mb-5 title">Trending</h4>
    <div class="row">
        <div class="col-lg-4 mb-sm-3">
            <div class="card">
                <img class="card-img-top" src="{{asset('images/blog-1.jpg')}}" alt="Card image cap">
                <div class="card-body">
                    <span class="text-brown">26th October,2019</span>
                    <h5 class="card-title text-blue">How to keep kitchen free from …? </h5>
                    <hr align="left" class="box-hr mt-5 text-left">
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                        the card's content.</p>
                    <a class="text-brown" href="#">Know more -></a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 mb-sm-3"">
                <div class=" card">
            <img class="card-img-top" src="{{asset('images/blog-2.jpg')}}" alt="Card image cap">
            <div class="card-body">
                <span class="text-brown">26th October,2019</span>
                <h5 class="card-title text-blue">I used Brownwires for my wall painting. I enjoyed their
                    service</h5>
                <hr align="left" class="box-hr mt-5 text-left">
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                    the card's content.</p>
                <a class="text-brown" href="#">Know more -></a>
            </div>
        </div>
    </div>
    <div class="col-lg-4 mb-sm-3"">
                <div class=" card">
        <img class="card-img-top" src="{{asset('images/blog-3.jpg')}}" alt="Card image cap">
        <div class="card-body">
            <span class="text-brown">26th October,2019</span>
            <h5 class="card-title text-blue">All things you need to know when you move in to a new
                house. </h5>
            <hr align="left" class="box-hr mt-5 text-left">
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                the card's content.</p>
            <a class="text-brown" href="#">Know more -></a>
        </div>
    </div>
</div>
</div>
<div class="col-md-12">
    <center>
        <button class="btn btn-brown mt-5 btn-all">View All</button>
    </center>
</div>
</div>