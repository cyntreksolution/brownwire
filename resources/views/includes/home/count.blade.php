<div class="row" style="margin-top: 15%">
    <div class="col-lg-4 text-sm-center mb-sm-3">
        <div class="circle">
            <div class="circle-inner">
                <h2 class="text-brown text-center font-weight-bolder counter">4.2</h2>
                <p class="text-center text-blue title">Average Rating</p>
            </div>
        </div>
    </div>

    <div class="col-lg-4 text-lg-center mb-sm-3">
        <div class="circle">
            <div class="circle-inner">
                <h2 class="text-brown text-center font-weight-bolder counter">100</h2>
                <p class="text-center text-blue title">Customer Served</p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 text-lg-right mb-sm-3">
        <div class="circle">
            <div class="circle-inner">
                <h2 class="text-brown text-center font-weight-bolder counter">100</h2>
                <p class="text-center text-blue title">Service Experts</p>
            </div>
        </div>
    </div>

</div>