<div id="contact">
    <div class="row mr-0 mt-5">
        <div class="col-md-12">
            <h3 class="font-weight-bolder header text-brown mt-3 title text-sm-center">CONTACT US</h3>
        </div>
    </div>
    <div class="blog mb-5">

        <h4 class="text-blue font-weight-bold text-center mt-10 mb-5">Send Us a Message</h4>
        {{ Form::open(['url' =>  route('page.contact'), 'method' => 'POST' ]) }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input class="form-control input-border" name="name" value="{{ old('name') }}" placeholder="Name"
                        required>
                    @if ($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input class="form-control input-border" name="email" value="{{ old('email') }}" placeholder="Email"
                        required>
                    @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input class="form-control input-border" name="phone" value="{{ old('phone') }}" placeholder="Phone"
                        required>
                    @if ($errors->has('phone'))
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <select class="form-control input-border" name="service" value="{{ old('service') }}" required>
                        <option value="" disabled selected>Service</option>
                        <option value="1" selected>test</option>
                    </select>
                    @if ($errors->has('service'))
                    <span class="text-danger">{{ $errors->first('service') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <textarea class="form-control input-border" name="message" rows="5" placeholder="Message"
                    required>{{ old('message') }}</textarea>
                @if ($errors->has('message'))
                <span class="text-danger">{{ $errors->first('message') }}</span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <center>
                    <button class="btn btn-brown mt-3">Send Message</button>
                </center>
            </div>
        </div>
        {{ Form::close() }}

    </div>
</div>