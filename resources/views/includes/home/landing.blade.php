<div class="row">
    <div class="col-md-12 mt-5">
        <h1 class="text-orange mt-5 font-weight-bolder landing-header">
            Your trusted service expert.</h1>
        <div class="row m-0 p-0">
            <div class="col-lg-6 p-0 landing-left">
                <div class="row">
                    <div class="col-lg-8">
                        <p class="text-blue mt-4 landing-desc">"Be it a painting touch up in the living room,leaking
                            pipe in
                            bathroom or kitchen
                            sink choke clearance,one place for you to get it done" - Customer 1</p>
                    </div>
                </div>
                <h3 class="text-blue mt-4 font-weight-bolder landing-header-2">Get a fix right away.</h3>
                <div class="row">
                    <div class="col-md-12 p-0 mt-md-5">
                        <div class="float-right text-right mt-1">

                            <button class="btn btn-brown btn-booknow">BOOK NOW</button>

                            <p class="text-brown m-0 landing-p">Reliable.</p>

                            <p class="text-brown m-0 landing-p">Affordable.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 ml-auto mt-5">
                <img class="img-fluid" src="{{asset('images/man1.png')}}">
            </div>
        </div>
    </div>
</div>