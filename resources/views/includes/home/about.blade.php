<div id="about"></div>
<div class="row mt-5">
    <div class="col-lg-5 p-md-0">
        <img class="img-fluid" src="{{asset('images/man.png')}}">
    </div>
    <div class="col-lg-7 p-md-0 about-sec">
        <div class="row">
            <div class="col-md-10 mt-5 ml-auto">
                <h3 class="text-brown text-md-right font-weight-bold text-sm-center title">ABOUT US</h3>
            </div>
            <div class="col-md-11 ml-auto box-yellow">
                <p class="text-blue about-desc font-1">
                    We take proper care of & maintaining
                    your home or office and help you not worry
                    about such things so you can
                    just get on with your life.???
                </p>
            </div>
        </div>
    </div>
</div>