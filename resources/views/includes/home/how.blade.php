<div class="row w-100" style="padding-top: 10% !important;">
    <div class="col-md-10">
        <h3 class="text-brown font-weight-bolder title">HOW IT WORKS</h3>
        <h4 class="text-blue font-weight-bold title-2">In just 4 simple steps.</h4>
    </div>
    <div class="col-md-2 text-lg-right text-center">
        <button class="btn btn-brown">Book Now</button>
    </div>
</div>


<div class="row mt-5 w-100">

    <div class="col-lg-3 step">
        <div class="row text-lg-left text-center">
            <div class="col-md-4">
                <div class="brown-circle">
                    <div class="number-circle">
                        <p class="font-weight-bolder text-orange counter">1</p>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <p class="header text-brown font-weight-bolder m-0 title">Choose</p>
                <p class="text-blue desc">from list of services </p>
            </div>
        </div>
    </div>

    <div class="col-lg-3 step">
        <div class="row text-lg-left text-center">
            <div class="col-md-4">
                <div class="brown-circle">
                    <div class="number-circle">
                        <p class="font-weight-bolder text-orange counter">2</p>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <p class="header text-brown font-weight-bolder m-0 title">Add Info</p>
                <p class="text-blue desc">& details of problem </p>
            </div>
        </div>
    </div>

    <div class="col-lg-3 step">
        <div class="row text-lg-left text-center">
            <div class="col-md-4">
                <div class="brown-circle">
                    <div class="number-circle">
                        <p class="font-weight-bolder text-orange counter">3</p>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <p class="header text-brown font-weight-bolder m-0 title">Schedule</p>
                <p class="text-blue desc">appointment date & time </p>
            </div>
        </div>
    </div>

    <div class="col-lg-3 step">
        <div class="row text-lg-left text-center">
            <div class="col-md-4">
                <div class="brown-circle">
                    <div class="number-circle">
                        <p class="font-weight-bolder text-orange counter">4</p>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <p class="header text-brown font-weight-bolder m-0 title">Confirm </p>
                <p class="text-blue desc">service request</p>
            </div>
        </div>
    </div>


</div>