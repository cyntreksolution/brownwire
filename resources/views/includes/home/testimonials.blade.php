<div class="row mr-0 mt-20">
    <div class="col-md-12">
        <h3 class="font-weight-bolder header text-brown title mt-3 text-sm-center">TESTIMONIALS</h3>
    </div>
</div>
<div class="row mr-0 testimonials mb-10">
    <div class="col-md-12 text-center">
        <h4 class="text-blue font-weight-bold">What our customers say.</h4>

        <div class="owl-carousel owl-theme" id="testimonials-2">

            <div class="item">
                <span class="text-brown display-1">&#8220;</span>
                <p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                    nostrud
                    exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                <h5 class="font-weight-bold">James Charles</h5>
                <span class="">Envato - Author</span>
            </div>


            <div class="item">
                <span class="text-brown display-1">&#8220;</span>
                <p class="fs-18 color-ddd">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis
                    nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                <h5 class="color-orange mt-20px fw-600 mb-0px">James Charles</h5>
                <span class="color-999">Envato - Author</span>
            </div>

            <div class="item">
                <span class="text-brown display-1">&#8220;</span>
                <p class="fs-18 color-ddd">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis
                    nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                <h5 class="color-orange mt-20px fw-600 mb-0px">James Charles</h5>
                <span class="color-999">Envato - Author</span>
            </div>


        </div>
    </div>


</div>

{{-- <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <span class="text-brown display-1">&#8220;</span>
                    <p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco laboris nisi ut aliquip ex ea commodo.</p>
                    <h5 class="font-weight-bold">James Charles</h5>
                    <span class="">Envato - Author</span>
                </div>
                <div class="carousel-item">
                    <span class="text-brown display-1">&#8220;</span>
                    <p class="fs-18 color-ddd">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                    <h5 class="color-orange mt-20px fw-600 mb-0px">James Charles</h5>
                    <span class="color-999">Envato - Author</span>
                </div>
                <div class="carousel-item">
                    <span class="text-brown display-1">&#8220;</span>
                    <p class="fs-18 color-ddd">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
                    <h5 class="color-orange mt-20px fw-600 mb-0px">James Charles</h5>
                    <span class="color-999">Envato - Author</span>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div> --}}