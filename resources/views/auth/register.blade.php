<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <title>Brownwire.com</title>

     <!-- Fonts -->
     <link rel="dns-prefetch" href="http://fonts.gstatic.com/">
     <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

     <!-- Styles -->
     <link rel="stylesheet" href="{{asset('frontEnd/plugins/bootstrap/css/bootstrap.min.css')}}" type="text/css" />
     <link rel="stylesheet" href="{{asset('frontEnd/plugins/font-awesome/css/font-awesome.min.css')}}" />
     <link rel="stylesheet" href="{{asset('frontEnd/css/main.css')}}">
     <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css')}}" integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous" />
     <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css')}}" integrity="sha256-kksNxjDRxd/5+jGurZUJd1sdR2v+ClrCl3svESBaJqw=" crossorigin="anonymous" />

</head>

<body style="overflow: hidden !important;">
    <div id="app">

        <!-- HEADER-MENU ---------------------->
        @include('inc.topbar')
        <!--/ HEADER-MENU --------------------->

        <main>
            <section class="testimonial py-5" id="testimonial">
                <img class="backgroundImage_login" src="{{asset('frontEnd/images/back1.png')}}">

                <div class="container formData" style="margin-top:6%;">
                    <div class="row">
                    <div class="col-md-8 p-1 border bg-light">

                        {!! Form::open(['route' => 'register', 'method' => 'post','id'=>'createForm']) !!}
{{--                        <form method="POST" action="{{ route('register') }}">--}}



                            <div class="md-form col-md-10">
                                <h2 class="pb-2 text-brown title">SIGN UP</h2>
                                @if($errors->any())
                                    <p class="text-danger">{{$errors->first()}}</p>
                                @endif
                            </div>

                            <div class="md-form col-md-10">
                                <label for="inputMDEx" class="text-brown" >NAME</label><br>
                                <p class="form">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter your full name" >
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </p>
                            </div>

                            <div class="md-form col-md-10">
                                <label for="inputMDEx" class="text-brown" >Mobile Number</label><br>
                                <p class="form">
                                    <input id="mobile" maxlength="8" minlength="8" type="text" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" required autocomplete="name" autofocus placeholder="Enter your mobile number" >
                                    @error('mobile')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </p>
                            </div>


                            <div class="md-form col-md-10">
                                <label for="inputMDEx" class="text-brown">EMAIL</label><br>
                                <p class="form">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter your email address">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </p>
                            </div>


                            <div class="md-form col-md-10">
                                <label for="inputMDEx" class="text-brown">PASSWORD</label><br>
                                <p class="form">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password"  placeholder="Atleast 6 characters">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </p>
                            </div>

                            <div class="md-form col-md-10">
                                <label for="inputMDEx" class="text-brown">CONFIRM PASSWORD</label><br>
                                <p class="form">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password"  placeholder="Confirmation Password">
                                </p>
                            </div>

                            <div class="md-form col-md-10">
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-2 text-right p-0">
                                        <label class="containerCheckbox_data">
                                            <input required type="checkbox">
                                            <span class="checkmarkCheckbox_data"></span>
                                        </label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <p class="text-black_2">
                                            By signing up I agree with the terms and conditions of Brownwires.
                                        </p>
                                    </div>

                                    <div class="col-md-4 ml-auto">
                                        <button type="submit" class="btn btn-brown" style="padding: 20px 40px;">SIGNUP</button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                    <div class="col-md-4 px-4 py-5 text-white text-center" style=" background-color:#8A4B20;opacity: 0.8;">
                        <div class="text-left">
                            <h2 class="">Have an account ?</h2>
                            <a href="login"><button class="btn btn-outline-light mt-4" style="padding: 15px 50px">LOGIN</button></a>
                        </div>
                    </div>
                </div>
            </div>
            </section>
        </main>

    </div>

    <script src="{{asset('frontEnd/plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('frontEnd/plugins/popper/popper.min.js')}}"></script>
    <script src="{{asset('frontEnd/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js')}}" integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
    <script src="{{asset('frontEnd/js/custom.js')}}"></script>
    </body>
</html>
