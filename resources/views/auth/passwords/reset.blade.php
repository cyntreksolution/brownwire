<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <title>Brownwire.com</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="http://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

    <link rel="stylesheet" href="{{asset('frontEnd/plugins/bootstrap/css/bootstrap.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('frontEnd/plugins/font-awesome/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('frontEnd/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body class="pane scroller">
<div id="app">


    <!-- HEADER-MENU -->
@include('inc.topbar')
<!--/ HEADER-MENU -->
    <main>
        <div class="row justify-content-center">
            <div class="col-md-8">
                {{ __('Reset Password') }}


                <form style="margin: 150px 0 40px 0" method="POST" action="{{ route('password.update') }}">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group row">
                        <label for="email"
                               class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                        <div class="col-md-6">
                            <input readonly id="email" type="email"
                                   class="form-control @error('email') is-invalid @enderror" name="email"
                                   value="{{ urldecode($email)  }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password"
                               class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password"
                                   class="form-control @error('password') is-invalid @enderror" name="password"
                                   required autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm"
                               class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-brown">
                                {{ __('Reset Password') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>

    <!-- FOTTER-MENU -->
@include('inc.fotter')
<!--/ FOTTER-MENU -->

    <!--/////CONTACT US MENU -->
</div>
<script src="{{asset('frontEnd/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/popper/popper.min.js')}}"></script>
<script src="{{asset('frontEnd/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js')}}"></script>
<script src="{{asset('frontEnd/js/custom.js')}}"></script>
</body>

</html>

