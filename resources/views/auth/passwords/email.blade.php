<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <title>Brownwire.com</title>

     <!-- Fonts -->
     <link rel="dns-prefetch" href="http://fonts.gstatic.com/">
     <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

     <!-- Styles -->
     <link rel="stylesheet" href="{{asset('frontEnd/plugins/bootstrap/css/bootstrap.min.css')}}" type="text/css" />
     <link rel="stylesheet" href="{{asset('frontEnd/plugins/font-awesome/css/font-awesome.min.css')}}" />
     <link rel="stylesheet" href="{{asset('frontEnd/css/main.css')}}">
     <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css')}}" integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous" />
     <link rel="stylesheet" href="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css')}}" integrity="sha256-kksNxjDRxd/5+jGurZUJd1sdR2v+ClrCl3svESBaJqw=" crossorigin="anonymous" />

</head>

<body style="overflow: hidden !important;">
    <div id="app">

        <!-- HEADER-MENU ---------------------->
        @include('inc.topbar')
        <!--/ HEADER-MENU --------------------->

        <main>
            <section class="testimonial py-5" id="testimonial">
                <img class="backgroundImage_login" src="{{asset('frontEnd/images/back1.png')}}">
                <div class="container formData" style="  margin-top: 12%;">
                    <div class="row">


                        <div class="col-md-12 p-5 border bg-light">
                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                 <div class="md-form col-md-10">
                                    <h2 class="pb-2 text-brown title">RESET PASSWORD</h2>
                                </div>

                                <div class="md-form col-md-12">
                                    <label for="inputMDEx" class="text-brown">EMAIL ADDRESS</label><br>
                                    <p class="form">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  placeholder="Enter your email address">
                                    </p>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>


                                <div class="md-form col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            @if($errors->any())
                                                <p class="text-danger">{{$errors->first()}}</p>
                                            @endif
                                        </div>
                                        <div class="col-md-6 ml-auto text-right">
                                            <button type="submit"  class="btn btn-brown" style="padding: 20px 40px;">Send password reset link</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>

    <script src="{{asset('frontEnd/plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('frontEnd/plugins/popper/popper.min.js')}}"></script>
    <script src="{{asset('frontEnd/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('frontEnd/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js')}}" integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
    <script src="{{asset('frontEnd/js/custom.js')}}"></script>
</body>
</html>
