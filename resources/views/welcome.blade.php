@extends('layouts.app')

@section('content')
<div id="home"></div>
<div class="container py-4" style="padding-top: 10% !important;">
    @include('includes.home.landing')
    @include('includes.home.about')
    @include('includes.home.count')
    @include('includes.home.services')
    @include('includes.home.how')
</div>
<div class="light-pink-row">
    <div class="container">
        <div class="row m-0">

            <div class="col-lg-8">
                <div class="text-brown title">Easy Payment Options </div>
                <div class="text-blue desc text-right">after completion of the service**</div>
            </div>

            <div class="col-lg-4 mt-3 mb-3">
                <div class="row pay text-center">
                    <div class="col-lg-4  col-sm-4 col-4 p-0">
                        <div class="number-circle-p">
                            <img src="/images/pay1.png">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4  col-4 p-0">
                        <div class="number-circle-p">
                            <img src="/images/pay2.png">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-4  p-0">
                        <div class="number-circle-p">
                            <img src="/images/pay3.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    @include('includes.home.testimonials')
    <!-- @include('includes.home.blog') -->
    @include('includes.home.contact')
</div>
<div class="com-md-1 float-right m-3">
    <a class="p-4" href="#home"><i class="fa fa-arrow-circle-up text-brown fa-3x"></i></a>
</div>
<style>
    html {
        scroll-behavior: smooth;
    }
</style>
@endsection