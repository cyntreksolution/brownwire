<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', 'HomeController');
Route::get('plumbing', function () {
    return view('plumbing');
});

Route::get('service/{service?}', 'HomeController@SelectService')->name('service.select');
Route::get('cart/{category}', 'HomeController@category');

Route::get('plumbing4/{service}/{category}', 'HomeController@service');
Route::get('plumbing5/{item_mapping_id}', 'HomeController@itemtype');
Route::get('plumbing6/{itemTypeId}', 'HomeController@item');
//Route::get('summary/{cart_id}', 'HomeController@summary')->name('order.summary');
//Route::get('confirm/{cart_id}', 'HomeController@confirm')->name('order.confirm');
Route::get('account', 'HomeController@account');
Route::get('security', 'HomeController@security');
Route::resource('accountupdate', 'AccountController');





Route::get('/logout', 'Auth\LoginController@logout');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/proceed', 'HomeController@proceedTransaction')->name('proceed.transaction');


Route::get('user/data/{cart}', 'HomeController@OrderAddress')->name('cart.user')->middleware('auth');
Route::post('user/data', 'HomeController@submitOrderAddress')->name('address.submit')->middleware('auth');
Route::get('summary/{cart}', 'HomeController@orderSummary')->name('order.summary')->middleware('auth');
Route::post('confirm', 'HomeController@confirmOrder')->name('order.confirm')->middleware('auth');
Route::post('confirm/{cart}', 'HomeController@submitConfirmOrder')->name('order.confirmed')->middleware('auth');


// Route::middleware(['auth'])->group(function () {
//
 Route::get('admin-console', function () {
     return view('admin.dashboard');
 })->middleware(['auth', 'role:admin'])->name('admin.dashboard');
//
 Route::middleware(['auth', 'role:admin'])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::resource('contact', 'Backend\ContactController');
        Route::resource('user', 'Backend\UserController');
        Route::resource('category', 'Backend\CategoryController');
        Route::resource('space', 'Backend\SpaceController');
        Route::resource('itemtype', 'Backend\ItemtypeController');
        Route::resource('location', 'Backend\LocationController');
        Route::resource('item', 'Backend\ItemController');
        Route::resource('time_slot', 'Backend\TimeSlotController');
        Route::resource('description', 'Backend\DescriptionController');
        Route::resource('location_description', 'Backend\LocationDescriptionController');
        Route::resource('issue', 'Backend\IssuesController');
        Route::resource('issue_mapping', 'Backend\Issue_MappingsController');
        Route::resource('immediate_issue', 'Backend\ImmediateIssueController');
        Route::resource('cart', 'Backend\CartController');
        Route::get('cart/{order_id}/{status}', 'Backend\CartController@status');
        Route::resource('payment_type', 'Backend\PaymentTypeController');
        Route::resource('item_mapping', 'Backend\Item_MappingsController');
        Route::resource('services', 'Backend\ServiceController');
        Route::get('services/table/data', 'Backend\ServiceController@tableData')->name('services.table');
        Route::post('order/{cart}/status', 'HomeController@changeStatus')->name('order.status');
        Route::get('config', 'Backend\AppConfigController@index')->name('app.config');
        Route::post('config', 'Backend\AppConfigController@update')->name('app.config');

    });
 });
Route::get('activity','HomeController@activity')->name('user.activity');
Route::get('category/{category}', 'Backend\CategoryController@show');
Route::get('services/{service}', 'Backend\ServiceController@show');
Route::get('item-type/{item_type}', 'Backend\ItemtypeController@show');
Route::get('item/{item}', 'Backend\ItemController@show');
Route::get('item-mapping/{item}', 'Backend\Item_MappingsController@show');
Route::get('get/timeslot/{day}', 'HomeController@getTimeSlot')->name('order.getTime');

Route::get('debug', 'DebugController@index');
Route::post('contact/us', 'ContactController@contactAdd')->name('contact.data');
Route::post('order/{cart}/cancel', 'HomeController@cancelOrder')->name('order.cancel');


