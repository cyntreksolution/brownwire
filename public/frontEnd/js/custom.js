// $(document).ready(function() {
$("#services-2").owlCarousel({
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 4000,
    dots: false,
    responsiveClass: true,
    navText: [
        "<i class='fa fa-chevron-left'></i>",
        "<i class='fa fa-chevron-right'></i>"
    ],
    responsive: {
        0: {
            items: 1,
            nav: false,
            dots: true
        },
        600: {
            items: 2,
            nav: false
        },
        768: {
            items: 3,
            nav: true,
            loop: false
        },
        1280: {
            items: 4,
            nav: true,
            loop: false
        }
    }
});
// });

$("#testimonials-2").owlCarousel({
    loop: true,
    margin: 10,

    autoplay: true,
    autoplayTimeout: 4000,
    dots: false,
    responsiveClass: true,
    navText: [
        "<i class='fa fa-chevron-left'></i>",
        "<i class='fa fa-chevron-right'></i>"
    ],
    responsive: {
        0: {
            items: 1,
            nav: false,
            dots: true
        },
        600: {
            items: 1,
            nav: false
        },
        1000: {
            items: 1,
            nav: true,
            loop: false
        }
    }
});
