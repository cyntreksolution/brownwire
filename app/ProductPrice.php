<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Product Price
 */
class ProductPrice extends Model
{
    use SoftDeletes;

    protected $table = 'item_price';
    protected $fillable = [
        'deal_price',
        'market_price',
        'supplier_price',
        'item_code',
        'company_id',
        'price_book_id',
        'price_level_id',
        'product_status_id',
        'store_type_id',
        'supplier_id',
        'start_date',
        'end_date',
        'is_warranty_price',
        'user_id',
        'status_id'
    ];
}
