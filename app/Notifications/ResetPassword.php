<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPassword extends Notification
{
    public $token;
    public $user;

    public function __construct($token,$email)
    {
        $this->token = $token;
        $this->user = $email;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from('info@brownwires.com', 'Brownwires.com')
            ->subject('Reset Your Password')
            ->line('You are receiving this email because we received a password reset request for your account.')
            ->action('Reset Password', route('password.reset', ['token' => $this->token, 'email' => urlencode($this->user)]))
            ->line('If you did not request a password reset, no further action is required.');
    }
}
