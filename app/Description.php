<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Description extends Model
{
    protected $fillable = [
   		'id',
        'description_id',
        'description_name',
        'is_active',
        'service_id',
        'item_id'
        ];
    //

        public function service(){
        	return $this->belongsTo(Service::class);
        }

        public function item(){
        	return $this->belongsTo(Item::class);
        }
    //
}
