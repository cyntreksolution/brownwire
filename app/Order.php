<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Order
 */
class Order extends Model
{
    use SoftDeletes;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'order';
    protected $fillable = [
        'order_no',
        'order_reference_no',
        'session_id',
        'user_id',
        'billing_address_id',
        'shipping_address_id',
        'total_amount',
        'shipping_fee',
        'handling_fee',
        'actual_total_amount',
        'actual_shipping_fee',
        'payment_method_id',
        'merchant_record_id',
        'pickup_method_id',
        'shipping_header_id',
        'backend_user_id',
        'backend_user_ip',
        'card_or_bank_refe_no',
        'payment_slip',
        'invoice_no',
        'status_id',
        'sms_send_status',
        'is_new_order'
    ];

    public function getOrderDetails()
    {
        return $this->hasMany('App\OrderDetails', 'order_id', 'id');
    }

}
