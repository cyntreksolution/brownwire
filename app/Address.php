<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes;
    protected $table = 'user_address';

    protected $fillable = [
        'user_id',
        'postal_code',
        'address_1',
        'address_2',
        'address_3',
    ];
}
