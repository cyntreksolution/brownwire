<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IssueMapping extends Model
{
    protected $fillable = [
        'id',
        'item_mapping_id',
        'issue_id',


    ];

    public function item_mapping()
    {
        return $this->belongsTo(Item_Mapping::class)->withoutGlobalScopes(['status']);
    }

    public function issue()
    {
        return $this->belongsTo(Issue::class);
    }


}
