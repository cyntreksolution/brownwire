<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function contactAdd(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|min:11|numeric',
            'service' => 'required',
            'message' => 'required',
        ]);

        $contacts = Contact::create($request->all());

        return redirect('/');
    }
}
