<?php

namespace App\Http\Controllers;

use App\Address;
use App\Service;
use App\Category;
use App\Item_Mapping;
use App\IssueMapping;
use App\Item;
use App\TimeSlot;
use App\Cart;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function update(Request $request, $id)
    {
        if ($request->password_reset){
            $user = User::find($id);
            if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
                // The passwords matches
                return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
            }

            if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
                //Current password and new password are same
                return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
            }

            $validatedData = $request->validate([
                'current-password' => 'required',
                'new-password' => 'required|string|min:6|confirmed',
            ]);

            //Change Password
            $user = Auth::user();
            $user->password = bcrypt($request->get('new-password'));
            $user->save();

            return redirect('/security')->with("success","Password changed successfully !");

        }else{
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->mobile = $request->mobile;
            $user->save();

            $address = Address::whereUserId($id)->latest()->first();
            if (!empty($address)){
                $address->address_1=$request->address_1;
                $address->address_2=$request->address_2;
                $address->address_3=$request->address_3;
                $address->postal_code=$request->postalcode;
                $address->save();
            }else{
                $address = new Address();
                $address->user_id=Auth::id();
                $address->address_1=$request->address_1;
                $address->address_2=$request->address_2;
                $address->address_3=$request->address_3;
                $address->postal_code=$request->postalcode;
                $address->save();
            }



            return redirect('/account')->with('status', 'Profile updated!');
        }

    }
}
