<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Order;
use App\ProductPrice;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\Integer;

class DebugController extends Controller
{
    public function index()
    {
        Log::channel('mine')->debug('Something happened!');
    }
}
