<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Space;
use App\Location;
use Illuminate\Http\Request;
use DB;
class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     //    $data=DB::table('locations')           
     // ->join("spaces", "spaces.id", "=", "locations.space_id")
     // ->select('spaces.name','locations.location_id','locations.location_name','locations.is_active','locations.id')
     //   ->get();
        $data = Location::all();
       return view('admin.locations.index', compact('data'));

        //  $locations = Location::all();
        // return view('admin.locations.index', compact('locations'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $users = DB::table('spaces')->get();
      
       return view('admin.locations.create', compact('users'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $location= new Location([
            'location_id' => $request->get('location_id'),
            'location_name' => $request->get('location_name'),
            'is_active' => $request->get('is_active'),
            'space_id' => $request->get('space_id'),
            ]);

        $location->save();
        return redirect(route('location.index'))->with(['success' => true, 'success.message' => 'Location Created Successfully']);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        $spaces =Space::pluck('name','id');
       return view('admin.locations.edit', compact('location','spaces'));
        // return view('admin.locations.edit', compact('location'));
       //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        $location->location_id = $request->location_id;
        $location->location_name = $request->location_name;
        $location->space_id = $request->space_id;
        $location->is_active = $request->is_active;
         $location->save();
       
        return redirect(route('location.index'))->with(['success' => true, 'success.message' => 'location Updated Successfully']);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
       $location->delete();
        return 'true';
        //
    }
}
