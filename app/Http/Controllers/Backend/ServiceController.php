<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Item_Mapping;
use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::withoutGlobalScopes(['status'])->get();
        return view('admin.service.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => ['required', 'unique:services', 'max:191'],
            'service_id' => ['required','unique:services','max:191'],
            'inspection_charge' => ['required', 'numeric'],
            'service_image' => ['required', 'dimensions:width=100,height=100'],
        ]);

        $filename = null;
        if (!empty($request->file('service_image'))) {
            $image = $request->file('service_image');
            $destinationPath = 'frontEnd/icon/';
            $filename = $image->getClientOriginalName();
            $image->move($destinationPath, $filename);
        }
        $service = new Service([
            'name' => $request->get('name'),
            'service_id' => $request->get('service_id'),
            'inspection_charge' => $request->get('inspection_charge'),
            'is_active' => !empty($request->get('is_active'))?1:0,
            'is_optional' =>!empty($request->get('is_optional')) ?1:0,
            'service_image' => $filename,
        ]);

        $service->save();
        return redirect(route('services.index'))->with(['success' => true, 'success.message' => 'Service Created Successfully']);


    }

    /**
     * Display the specified resource.
     *
     * @param \App\Service $service
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $service = Service::withoutGlobalScopes(['status'])->whereId($id)->first();
        return $service;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Service $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $service = Service::withoutGlobalScopes(['status'])->whereId($id)->first();
        return view('admin.service.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Service $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $service = Service::withoutGlobalScopes(['status'])->whereId($id)->first();
        $validatedData = $request->validate([
            'name' => ['required', 'unique:services,name,'.$id, 'max:191'],
            'service_id' => ['required','unique:services,service_id,'.$id,'max:191'],
            'inspection_charge' => ['required', 'numeric'],
            'service_image' => ['dimensions:width=100,height=100'],
        ]);


        $filename = null;
        if (!empty($request->file('service_image'))) {
            $image = $request->file('service_image');
            $destinationPath = 'frontEnd/icon/';
            $filename = $image->getClientOriginalName();
            $image->move($destinationPath, $filename);
        }


        $service->name = $request->name;
        $service->service_id = $request->service_id;
        $service->inspection_charge = $request->inspection_charge;
        $service->is_active = !empty($request->is_active)?1:0;
        $service->service_image =$filename;
        $service->is_optional =!empty($request->is_optional)?1:0;
        $service->save();

        return redirect(route('services.index'))->with(['success' => true, 'success.message' => 'service Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Service $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $service = Service::withoutGlobalScopes(['status'])->whereId($id)->first();
        $mapping = Item_Mapping::whereServiceId($service->id)->first();
        if (!empty($mapping) && $mapping->count() > 0) {
            return 'false';
        }
        $service->delete();
        return 'true';
    }

//    public
//    function tableData(Request $request)
//    {
//        $order_by = $request->order;
//        $search = $request->search['value'];
//        $start = $request->start;
//        $length = $request->length;
//        $order_by_str = $order_by[0]['dir'];
//
//        $columns = ['id', 'service_id', 'name', 'is_active', 'inspection_charge', 'is_optional'];
//        $order_column = $columns[$order_by[0]['column']];
//
//        $data[][] = array();
//
//        $summary = Service::tableData($order_column, $order_by_str, $start, $length);
//        $totalCount = $summary->count();
//
//        if (!empty($search)) {
//            $summary->searchData($search);
//        }
//
//        $query = $summary->get();
//        $queryCount = $query->count();
//
//        $i = 0;
//        $edit_btn = null;
//        $delete_btn = null;
//
//        foreach ($query as $key => $item) {
//            $edit_btn = "<a href=admin\"{{route('items.edit',[$item->id])}}\"><i class=\"fas fa-pencil-alt btn-icon-append \"> </i></a>";
//            $delete_btn = "<a data-id='{{$item->id}' class='button delete-confirm ml-2'><i style='color: #e52d27;' class='remove ml-2 fa fa-times-circle'> </i></a>";
//            $data[$i] = array(
//                $item->id,
//                $item->service_id,
//                $item->name,
//                ($item->is_active) ? "<button class='btn btn-xs btn-success'>active</button>" : "<button class='btn btn-xs btn-warning'>in-active</button>",
//                number_format($item->inspection_charge, '2', '.', ','),
//                ($item->is_optional) ? "<button class='btn btn-xs btn-info'>optional</button>" : "<button class='btn btn-xs btn-danger'>required</button>",
//                $edit_btn . $delete_btn,
//            );
//            $i++;
//        }
//
//        if ($queryCount == 0) {
//            $data = [];
//        }
//
//        $json_data = [
//            "draw" => intval($_REQUEST['draw']),
//            "recordsTotal" => intval($totalCount),
//            "recordsFiltered" => intval($queryCount),
//            "data" => $data
//        ];
//
//        return json_encode($json_data);
//    }
}
