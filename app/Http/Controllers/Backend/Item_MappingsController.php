<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Category;
use App\IssueMapping;
use App\Itemtype;
use App\Service;
use App\Item_Mapping;
use Illuminate\Http\Request;

class Item_MappingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = Item_Mapping::withoutGlobalScopes(['status'])->get();
        return view('admin.item_mappings.index', compact('data'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $itemstype = Itemtype::pluck('itemtypeid', 'id');
        $category = Category::pluck('category_name', 'id');
        $service = Service::pluck('name', 'id');


        return view('admin.item_mappings.create', compact('category', 'service', 'itemstype'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->issue_id == 0) {
            $current = IssueMapping::whereItemMappingId($request->item_mapping_id)->whereIssueId(0)->first();
            if (!empty($current) && $current->count() > 0) {
                return redirect(route('item_mapping.index'))->with(['error' => true, 'error.title' =>'Sorry !','error.message' => 'Cant Create Item mapping']);
            }
        } else {
            $current = IssueMapping::whereItemMappingId($request->item_mapping_id)->where('issue_id', '<>', 0)->first();
            if (!empty($current) && $current->count() > 0) {
                return redirect(route('item_mapping.index'))->with(['error' => true,'error.title' =>'Sorry !', 'error.message' => 'Cant Create Item mapping']);
            }
        }
        $item_mapping = Item_Mapping::create($request->all());
        return redirect(route('item_mapping.index'))->with(['success' => true, 'success.message' => 'Item Mapping Created Successfully']);

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Item_Mapping $item_Mapping
     * @return \Illuminate\Http\Response
     */
    public function show(Item_Mapping $item)
    {
        return Item_Mapping::with('service')->where('id', '=', $item->id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Item_Mapping $item_Mapping
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $item_mapping_id = $request->item_mapping;
        $item_mapping = Item_Mapping::withoutGlobalScopes(['status'])->whereId($item_mapping_id)->first();
        $category = Category::pluck('category_name', 'id');
        $service = Service::pluck('name', 'id');
        $itemstype = Itemtype::pluck('itemtypeid', 'id');

        return view('admin.item_mappings.edit', compact('item_mapping', 'category', 'service', 'itemstype'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Item_Mapping $item_Mapping
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $item_mapping_id = $request->item_mapping;
        $item_mapping = Item_Mapping::withoutGlobalScopes(['status'])->whereId($item_mapping_id)->first();
        $item_mapping->category_id = $request->category_id;
        $item_mapping->service_id = $request->service_id;
        $item_mapping->itemtype_id = $request->itemtype_id;
        $item_mapping->price = $request->price;
        $item_mapping->status = $request->status;

        $item_mapping->save();

        return redirect(route('item_mapping.index'))->with(['success' => true, 'success.message' => 'Item Mapping Updated Successfully']);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Item_Mapping $item_Mapping
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item_Mapping $item_mapping)
    {
        $issue = IssueMapping::whereItemMappingId($item_mapping->id)->get();
        if (!empty($issue) && $issue->count()>0){
            return 'false';
        }
        $item_mapping->delete();
        return 'true';
        //
    }
}
