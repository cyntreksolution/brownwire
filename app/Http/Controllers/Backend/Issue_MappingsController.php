<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Issue;
use App\Item_Mapping;


use App\IssueMapping;
use Illuminate\Http\Request;

class Issue_MappingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = IssueMapping::all();

        return view('admin.issue_mappings.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item_mapping = Item_Mapping::all();
        $issue = Issue::pluck('name', 'id');
        $issue->prepend('SKIP', '0');

        return view('admin.issue_mappings.create', compact('item_mapping', 'issue'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, IssueMapping $issue_mapping)
    {
        if ($request->issue_id == 0) {
            $maps=  IssueMapping::whereItemMappingId($request->item_mapping_id)->get();
            if (!empty($maps) && $maps->count()>0){
                return redirect(route('issue_mapping.index'))->with(['error' => true, 'error.message' => 'Cant Skip.Issues Already Assigned']);
            }
        }else{
            $maps=  IssueMapping::whereItemMappingId($request->item_mapping_id)->whereIssueId(0)->get();
            if (!empty($maps) && $maps->count()>0){
                return redirect(route('issue_mapping.index'))->with(['error' => true, 'error.message' => 'Skipped Mapping']);
            }
        }
        $issue_mapping->item_mapping_id = $request->item_mapping_id;
        $issue_mapping->issue_id = $request->issue_id;
        $issue_mapping->save();
        return redirect(route('issue_mapping.index'))->with(['success' => true, 'success.message' => 'Issue Mapping Created Successfully']);
    }
    //


    /**
     * Display the specified resource.
     *
     * @param \App\IssueMapping $issueMapping
     * @return \Illuminate\Http\Response
     */
    public function show(IssueMapping $issueMapping)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\IssueMapping $issueMapping
     * @return \Illuminate\Http\Response
     */
    public function edit(IssueMapping $issueMapping)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\IssueMapping $issueMapping
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IssueMapping $issueMapping)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\IssueMapping $issueMapping
     * @return \Illuminate\Http\Response
     */
    public function destroy(IssueMapping $issue_mapping)
    {

        $issue_mapping->delete();
        return 'true';
        ////
    }
}
