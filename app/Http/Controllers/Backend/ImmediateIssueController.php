<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\ImmediateIssue;
use Illuminate\Http\Request;

class ImmediateIssueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data = ImmediateIssue::all();
        return view('admin.immediateissues.index', compact('data'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.immediateissues.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $immediateissue = ImmediateIssue::create($request->all());
        if ($immediateissue) {
            return redirect(route('immediate_issue.index'))->with(['success' => true, 'success.message' => 'ImmediateIssue Created Successfully']);
        }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ImmediateIssue  $immediateIssue
     * @return \Illuminate\Http\Response
     */
    public function show(ImmediateIssue $immediateIssue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ImmediateIssue  $immediateIssue
     * @return \Illuminate\Http\Response
     */
    public function edit(ImmediateIssue $immediateIssue)
    {
         return view('admin.immediateissues.edit', compact('immediateIssue'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ImmediateIssue  $immediateIssue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ImmediateIssue $immediateIssue)
    {

         $immediateIssue->addon_price = $request->addon_price;
        
       
        $immediateIssue->save();
       
        return redirect(route('immediate_issue.index'))->with(['success' => true, 'success.message' => 'Immediate Issue Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ImmediateIssue  $immediateIssue
     * @return \Illuminate\Http\Response
     */
    public function destroy(ImmediateIssue $immediateIssue)
    {
         $immediateIssue->delete();
        return 'true';
        //
    }
}
