<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Item_Mapping;
use DemeterChain\C;
use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\File;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::withoutGlobalScopes(['status'])->get();
        return view('admin.categories.index', compact('categories'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'category_id' => ['required', 'unique:categories', 'max:191'],
            'category_name' => ['required','unique:categories','max:191'],
            'category_image' => ['required', 'dimensions:width=512,height=512'],
        ]);

        $filename = null;
        if (!empty($request->file('category_image'))) {
            $image = $request->file('category_image');
            $destinationPath = 'upload/frontEnd/icon/';
            $filename = $image->getClientOriginalName();
            $image->move($destinationPath, $filename);
        }

        $category = new Category();
        $category->category_id = $request->category_id;
        $category->category_name = $request->category_name;
        $category->is_active = $request->is_active;
        $category->category_image = $filename;
        $category->save();

        return redirect(route('category.index'))->with(['success' => true, 'success.message' => 'Category Created Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $category = Category::withoutGlobalScopes(['status'])->whereId($id)->first();
        return $category;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $category = Category::withoutGlobalScopes(['status'])->whereId($id)->first();
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::withoutGlobalScopes(['status'])->whereId($id)->first();

        $validatedData = $request->validate([
            'category_id' => ['required', 'unique:categories,category_id,'.$category->id, 'max:191'],
            'category_name' => ['required','unique:categories,category_name,'.$category->id,'max:191'],
            'category_image' => ['dimensions:width=512,height=512'],
        ]);

        $category->category_id = $request->category_id;
        $category->category_name = $request->category_name;
        $category->is_active = $request->is_active;

        $file = $request->file('category_image');

        if (!empty($file)) {
            $destinationPath = 'upload/frontEnd/icon/';
            File::delete($destinationPath . $category->category_image);//delete current image from storage
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $category->category_image = $filename;
        }

        $category->save();
        return redirect(route('category.index'))->with(['success' => true, 'success.message' => 'Item Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $category = Category::withoutGlobalScopes(['status'])->whereId($id)->first();
        $mapping = Item_Mapping::whereCategoryId($category->id)->first();
        if (!empty($mapping) && $mapping->count() > 0) {
            return 'false';
        }
        $category->delete();
        return 'true';
    }
}
