<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Location;
use App\Description;
use App\LocationDescription;
use Illuminate\Http\Request;

class LocationDescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data = LocationDescription::all();
       return view('admin.locationdescriptions.index', compact('data'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $description =Description::pluck('description_id','id');
          $location =Location::pluck('location_id','id');
         
         return view('admin.locationdescriptions.create',compact('description','location'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $locationdescription = LocationDescription::create($request->all());
        if ($locationdescription) {
            return redirect(route('location_description.index'))->with(['success' => true, 'success.message' => 'Location Description Created Successfully']);
        }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LocationDescription  $locationDescription
     * @return \Illuminate\Http\Response
     */
    public function show(LocationDescription $locationDescription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LocationDescription  $locationDescription
     * @return \Illuminate\Http\Response
     */
    public function edit(LocationDescription $locationDescription)
    {
         $description =Description::pluck('description_id','id');
         $location =Location::pluck('location_id','id');
       return view('admin.locationdescriptions.edit', compact('locationDescription','description','location'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LocationDescription  $locationDescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LocationDescription $locationDescription)
    {
         $locationDescription->description_id = $request->description_id;
        $locationDescription->location_id = $request->location_id;
       
        $locationDescription->save();
       
        return redirect(route('location_description.index'))->with(['success' => true, 'success.message' => 'Location description Updated Successfully']);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LocationDescription  $locationDescription
     * @return \Illuminate\Http\Response
     */
    public function destroy(LocationDescription $locationDescription)
    {
        $locationDescription->delete();
        return 'true';
        //
    }
}
