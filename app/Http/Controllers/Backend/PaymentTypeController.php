<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\PaymentType;
use Illuminate\Http\Request;

class PaymentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data = PaymentType::all();
        return view('admin.paymenttypes.index', compact('data'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.paymenttypes.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $paymenttypes = PaymentType::create($request->all());
        if ($paymenttypes) {
            return redirect(route('payment_type.index'))->with(['success' => true, 'success.message' => 'Payment Type Created Successfully']);
        }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentType $paymentType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentType $paymentType)
    {
         return view('admin.paymenttypes.edit', compact('paymentType'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentType $paymentType)
    {
        
        $paymentType->payment_type_id = $request->payment_type_id;
        $paymentType->payment_type_name = $request->payment_type_name;
        $paymentType->payment_optional_icon = $request->payment_optional_icon;
       
        $paymentType->save();
       
        return redirect(route('payment_type.index'))->with(['success' => true, 'success.message' => 'Payment Type Updated Successfully']);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentType $paymentType)
    {
        
        $paymentType->delete();
        return 'true';
        //
    }
}
