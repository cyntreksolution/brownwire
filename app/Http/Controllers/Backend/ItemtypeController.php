<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Item;
use App\Itemtype;
use Illuminate\Http\Request;

class ItemtypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itemstype = Itemtype::all();
        return view('admin.itemstype.index', compact('itemstype'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.itemstype.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'itemtypeid' => ['required', 'unique:itemstype', 'max:191'],
        ]);

        $itemtype = new Itemtype([
            'itemtypeid' => $request->get('itemtypeid'),
        ]);

        $itemtype->save();
        return redirect(route('itemtype.index'))->with(['success' => true, 'success.message' => 'Item Type Created Successfully']);

        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Itemtype $itemtype
     * @return \Illuminate\Http\Response
     */
    public function show(Itemtype $item_type)
    {
        return $item_type;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Itemtype $itemtype
     * @return \Illuminate\Http\Response
     */
    public function edit(Itemtype $itemtype)
    {
        return view('admin.itemstype.edit', compact('itemtype'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Itemtype $itemtype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Itemtype $itemtype)
    {
        $validatedData = $request->validate([
            'itemtypeid' => ['required', 'unique:itemstype,itemtypeid,'.$itemtype->id, 'max:191'],
        ]);

        $itemtype->itemtypeid = $request->itemtypeid;
        $itemtype->save();

        return redirect(route('itemtype.index'))->with(['success' => true, 'success.message' => 'Item Type Updated Successfully']);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Itemtype $itemtype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Itemtype $itemtype)
    {
        $items = Item::whereItemtypeId($itemtype->id)->first();
        if (!empty($items) && $items->count() > 0) {
            return 'false';
        }
        $itemtype->delete();
        return 'true';
        //
    }
}
