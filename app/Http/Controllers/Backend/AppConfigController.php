<?php

namespace App\Http\Controllers\Backend;

use App\AppConfig;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AppConfigController extends Controller
{
    public function index(){
        $data = AppConfig::first();
        return view('admin.config.edit',compact('data'));
    }

    public function update(Request  $request){
        $data = AppConfig::first();
        $data->description = $request->description;
        $data->email = $request->email;
        $data->contact = $request->contact;
        $data->home_url = $request->home_url;
        $data->save();
        return redirect(route('admin.dashboard'))->with(['success' => true, 'success.message' => 'Saved Successfully']);

    }
}
