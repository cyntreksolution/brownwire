<?php

namespace App\Http\Controllers\Backend;

use App\Cart;
use App\Http\Controllers\Controller;

use App\Item_Mapping;
use App\Itemtype;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Item::all();
        return view('admin.items.index', compact('data'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $itemstype = Itemtype::pluck('itemtypeid', 'id');
        return view('admin.items.create', compact('itemstype'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'item_id' => ['required', 'unique:items', 'max:191'],
            'price' => ['required','numeric'],
            'itemtype_id' => ['required'],
            'image' => ['required', 'dimensions:width=100,height=100'],
        ]);

        if (!empty($request->file('image'))) {
            $image = $request->file('image');
            $destinationPath = 'uploads/items/';
            $filename = $image->getClientOriginalName();
            $image->move($destinationPath, $filename);
        }

        $item = new Item([
            'item_id' => $request->get('item_id'),
            'price' => $request->get('price'),
            'itemtype_id' => $request->get('itemtype_id'),
            'image' => $filename,
        ]);

        $item->save();

        return redirect(route('item.index'))->with(['success' => true, 'success.message' => 'Item Created Successfully']);


    }

    /**
     * Display the specified resource.
     *
     * @param \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return $item;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $itemstype = itemtype::pluck('itemtypeid', 'id');
        return view('admin.items.edit', compact('item', 'itemstype'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $validatedData = $request->validate([
            'item_id' => ['required', 'unique:items,item_id,'.$item->id, 'max:191'],
            'price' => ['required','numeric'],
            'itemtype_id' => ['required'],
            'image' => ['dimensions:width=100,height=100'],
        ]);

        $item->item_id = $request->item_id;
        $item->price = $request->price;
        $item->itemtype_id = $request->itemtype_id;

        $file = $request->file('image');
        if (!empty($file)) {
            $destinationPath = 'uploads/items/';
            File::delete($destinationPath . $item->image);//delete current image from storage
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $item->image = $filename;
        }
        $item->save();

        return redirect(route('item.index'))->with(['success' => true, 'success.message' => 'Item Updated Successfully']);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $mapping = Cart::whereItemId($item->id)->first();
        if (!empty($mapping) && $mapping->count() > 0) {
            return 'false';
        }
        $item->delete();
        return 'true';
    }
}
