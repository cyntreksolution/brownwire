<?php

namespace App\Http\Controllers\Backend;

use App\Cart;
use App\Http\Controllers\Controller;

use App\TimeSlot;
use Illuminate\Http\Request;

class TimeSlotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = TimeSlot::orderBy('time_slot_day')->orderBy('start_time')->get();
       return view('admin.timeslots.index', compact('data'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $timeslot =TimeSlot::pluck('time_slot_day','id');
         return view('admin.timeslots.create',compact('timeslot'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $timeslot = TimeSlot::create($request->all());
        if ($timeslot) {
            return redirect(route('time_slot.index'))->with(['success' => true, 'success.message' => 'Time Slot Created Successfully']);
        }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TimeSlot  $timeSlot
     * @return \Illuminate\Http\Response
     */
    public function show(TimeSlot $timeSlot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TimeSlot  $timeSlot
     * @return \Illuminate\Http\Response
     */
    public function edit(TimeSlot $timeSlot)
    {

         return view('admin.timeslots.edit', compact('timeSlot'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TimeSlot  $timeSlot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TimeSlot $timeSlot)
    {
        $timeSlot->time_slots_id = $request->time_slots_id;
        $timeSlot->is_week_day = $request->is_week_day;
        $timeSlot->is_after_office = $request->is_after_office;

         $timeSlot->save();
         return redirect(route('time_slot.index'))->with(['success' => true, 'success.message' => 'Time Slot Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TimeSlot  $timeSlot
     * @return \Illuminate\Http\Response
     */
    public function destroy(TimeSlot $timeSlot)
    {
        $mapping = Cart::whereTimeslotId($timeSlot->id)->first();
        if (!empty($mapping) && $mapping->count() > 0) {
            return 'false';
        }
        $timeSlot->delete();
        return 'true';
    }
}
