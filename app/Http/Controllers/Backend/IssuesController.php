<?php

namespace App\Http\Controllers\Backend;

use App\Cart;
use App\Http\Controllers\Controller;

use App\Issue;
use App\IssueMapping;
use Illuminate\Http\Request;

class IssuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $issues = Issue::all();
        return view('admin.issues.index', compact('issues'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('admin.issues.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => ['required', 'unique:issues', 'max:191'],
        ]);

        $issue = Issue::create($request->all());
        if ($issue) {
            return redirect(route('issue.index'))->with(['success' => true, 'success.message' => 'Issue Created Successfully']);
        }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Issue  $issue
     * @return \Illuminate\Http\Response
     */
    public function show(Issue $issue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Issue  $issue
     * @return \Illuminate\Http\Response
     */
    public function edit(Issue $issue)
    {
        return view('admin.issues.edit', compact('issue')); //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Issue  $issue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Issue $issue)
    {
        $validatedData = $request->validate([
            'name' => ['required', 'unique:issues,name,'.$issue->id, 'max:191'],
        ]);

        $issue->name = $request->name;
        $issue->save();

        return redirect(route('issue.index'))->with(['success' => true, 'success.message' => 'Issue Updated Successfully']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Issue  $issue
     * @return \Illuminate\Http\Response
     */
    public function destroy(Issue $issue)
    {
        $mapping = IssueMapping::whereIssueId($issue->id)->first();
        if (!empty($mapping) && $mapping->count() > 0) {
            return 'false';
        }
        $issue->delete();
        return 'true';
        //
    }
}
