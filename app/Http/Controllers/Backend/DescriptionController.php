<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use DB;
use App\Service;
use App\Item;
use App\Description;
use Illuminate\Http\Request;

class DescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data = Description::all();
       return view('admin.descriptions.index', compact('data'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $service =Service::pluck('service_id','id');
          $item =Item::pluck('item_id','id');
         
         return view('admin.descriptions.create',compact('service','item'));//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $description = Description::create($request->all());
        if ($description) {
            return redirect(route('description.index'))->with(['success' => true, 'success.message' => 'Description Created Successfully']);
        }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Description  $description
     * @return \Illuminate\Http\Response
     */
    public function show(Description $description)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Description  $description
     * @return \Illuminate\Http\Response
     */
    public function edit(Description $description)
    {
         $service =Service::pluck('service_id','id');
         $item =Item::pluck('item_id','id');
       return view('admin.descriptions.edit', compact('description','service','item'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Description  $description
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Description $description)
    {
        
        $description->description_id = $request->description_id;
        $description->description_name = $request->description_name;
        $description->is_active = $request->is_active;
        $description->service_id = $request->service_id;
        $description->item_id = $request->item_id;
        $description->save();
       
        return redirect(route('description.index'))->with(['success' => true, 'success.message' => 'description Updated Successfully']);

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Description  $description
     * @return \Illuminate\Http\Response
     */
    public function destroy(Description $description)
    {
        $description->delete();
        return 'true';
        //
    }
}
