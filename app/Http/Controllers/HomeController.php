<?php

namespace App\Http\Controllers;

use App\Address;
use App\Service;
use App\Category;
use App\Item_Mapping;
use App\IssueMapping;
use App\Item;
use App\TimeSlot;
use App\Cart;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Category::all();
        $service = Service::all();
        return view('index', compact('data', 'service'));
    }

    public function SelectService(Request $request)
    {
        $selected = $request->service;
        $data = Category::all();
        $slots = TimeSlot::all();
        return view('plumbing2', compact('data', 'slots', 'selected'));
    }

    public function category(Request $request, $category)
    {
        $services = Item_Mapping::whereCategoryId($category)
            ->groupBy('service_id')
            ->with('service')
            ->get();
        // $services = Item_Mapping::whereCategoryId($category)->whereServiceId($service)->with('service')->get();
        return $services;
    }

    public function service(Request $request, $service, $category)
    {

        $services = Item_Mapping::whereCategoryId($category)
            ->whereServiceId($service)
            ->with('itemtype')
            ->get();
        return $services;
    }

    public function itemtype(Request $request, $itemmapping)
    {

        $itemtype = IssueMapping::whereItemMappingId($itemmapping)
            ->with('issue')
            ->get();
        return $itemtype;
    }

    public function item(Request $request, $item)
    {
        $itemtype = Item::whereItemtypeId($item)
            ->get();
        return $itemtype;
    }

    public function proceedTransaction(Request $request)
    {
        $cart = Cart::create($request->all());
//        return redirect(route('cart.user'))->with(['cart'=>$cart]);
        return redirect()->action('HomeController@OrderAddress', [$cart->id]);


        if ($cart) {
            // return view('plumbing2')->with(['success' => true, 'success.message' => 'Cart Created Successfully']);
            $data = Category::all();
            $slots = TimeSlot::all();

            //return view('plumbing2', compact('data','slots'))->with(['success' => true, 'success.message' => 'Cart Created Successfully']);
        }
    }


    public function getTimeSlot(Request $request)
    {
        $timeslots = TimeSlot::whereTimeSlotDay($request->day)->get();
        $ts = collect();
        foreach ($timeslots as $timeslot) {
            $dbtimestamp = strtotime($timeslot->end_time);
            if ($request->day == date('l')) {
                if (!(time() - $dbtimestamp > 30 * 60)) {
                    $ts->push($timeslot);
                }
            } else {
                $ts->push($timeslot);
            }
        }
        return $ts;
    }

    public function OrderAddress(Request $request, $cart)
    {
        $address = Auth::user()->address()->latest()->first();
        $cart_data = Cart::find($cart);
        return view('cart2', compact('address', 'cart','cart_data'));
    }

    public function submitOrderAddress(Request $request)
    {
        $cart = Cart::find($request->cart_id);

        $address = Address::updateOrCreate(['user_id' => Auth::id(),
            'postal_code' => $request->postal_code,
            'address_1' => $request->address1,
            'address_2' => $request->address2,
            'address_3' => $request->address3,
        ]);


        $cart->address_id = $address->id;
        $cart->directions_for_technician = $request->directions_for_technician;
        $cart->save();

        return redirect(route('order.summary', ['cart' => $cart]));
    }

    public function orderSummary(Request $request, $id)
    {
        $cart = Cart::find($id);
        return view('summary', compact('cart'));
    }

    public function confirmOrder(Request $request)
    {
        $cart = Cart::find($request->cart);
        $cart->status = 'confirmed';
        $cart->user_id = Auth::id();
        $cart->job_id = $this->generateJobNumber();
        $cart->save();
        return view('confirm', compact('cart'));
    }

    public function generateJobNumber()
    {
        $cart = Cart::where('status','<>','pending')->whereNotNull('job_id')->latest()->first();
        $id = (!empty($cart) && !empty($cart->job_id)) ? (int)substr($cart->job_id, 11) : 1012;
        $job_id = 'REQ-002-20-' . ++$id;
        return $job_id;
    }

    public function account()
    {
        $user = Auth::user();
        return view('account', compact('user'));
    }

    public function security()
    {
        $user = Auth::user();
        return view('security', compact('user'));
    }

    public function accountupdate(Request $request)
    {
        echo 'ghg';

    }


    public function activity()
    {
        $activities = Cart::whereUserId(Auth::id())->get();
        return view('activity', compact('activities'));
    }

    public function cancelOrder(Request $request)
    {
        $cart = $request->cart;
        $cart = Cart::find($cart);
        if (Auth::id() == $cart->user_id) {
            $cart->status = 'cancelled';
            $cart->cancelled_at = Carbon::now();
            $cart->save();
        }
        return redirect(route('user.activity'));
    }

    public function changeStatus(Request $request)
    {
        $cart = $request->cart;
        $status = $request->status;
        $cart = Cart::find($cart);
        $cart->status = $status;
        $cart->save();

        return 'true';
    }
}
