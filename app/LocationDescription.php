<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationDescription extends Model
{
    protected $fillable = [
   		'id',
        'description_id',
        'location_id',
        ];
    //

        public function description(){
        	return $this->belongsTo(Description::class);
        }
        public function location(){
        	return $this->belongsTo(Location::class);
        }
    //
}
