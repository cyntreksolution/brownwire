<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Itemtype extends Model
{
    use SoftDeletes;
    protected $table = 'itemstype';

    protected $fillable = [
        'id',
        'itemtypeid',
    ];
}
