<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'id',
        'item_id',
        'price',
        'itemtype_id',
        'image'
    ];

    public function itemtype()
    {
        return $this->belongsTo(Itemtype::class);
    }
}
