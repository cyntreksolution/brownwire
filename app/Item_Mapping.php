<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item_Mapping extends Model
{
    use SoftDeletes;
    protected $table = 'item_mappings';
    protected $fillable = [
        'id',
        'category_id',
        'service_id',
        'itemtype_id',
        'status',
        'price'

    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('status', function (Builder $builder) {
            $builder->where('status', '=', 1);
        });
    }

    public function item()
    {
        return $this->belongsTo(Item::class)->withoutGlobalScopes(['status']);
    }

    public function category()
    {
        return $this->belongsTo(Category::class)->withoutGlobalScopes(['status']);
    }

    public function service()
    {
        return $this->belongsTo(Service::class)->withoutGlobalScopes(['status']);
    }

    public function itemtype()
    {
        return $this->belongsTo(Itemtype::class)->withoutGlobalScopes(['status']);
    }
    //
}
