<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Product Option
 */
class Product extends Model
{
    use SoftDeletes;
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'item_master';
    protected $fillable = [
        'code',
        'models',
        'name',
        'description',
        'specification',
        'slug',
        'category_id',
        'brand_id',
        'is_in_store_pickup',
        'user_id',
        'status_id'
    ];



    public function getProductPrice()
    {
        return $this->belongsTo('App\ProductPrice', 'code', 'item_code');
    }

}
