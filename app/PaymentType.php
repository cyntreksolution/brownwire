<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
     protected $fillable = [
   		'id',
        'payment_type_id',
        'payment_type_name',
        'payment_optional_icon'
        ];
    //
}
