<?php

namespace App;;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Order Details
 */
class OrderDetails extends Model
{
    use SoftDeletes;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'order_details';

    protected $fillable = [
        'order_id',
        'promotion_id',
        'item_price_id',
        'deal_price',
        'discounted_price',
        'discount',
        'quantity',
        'invoice_no',
        'backend_user_id',
        'backend_user_ip',
        'status_id'
    ];

    public function getProductPrice()
    {
        return $this->belongsTo('App\ProductPrice', 'item_price_id', 'id');
    }
}
