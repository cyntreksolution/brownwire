<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    use SoftDeletes;
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('status', function (Builder $builder) {
            $builder->where('is_active', '=', 1);
        });
    }

    protected $fillable = ['id', 'service_id', 'name', 'is_active', 'inspection_charge', 'is_optional', 'service_image', 'description', 'deleted_at', 'created_at', 'updated_at'];

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('cumulative_foreign', 'like', "%" . $term . "%")
            ->orWhere('cumulative_local', 'like', "%" . $term . "%")
            ->orWhere('treatment_local', 'like', "%" . $term . "%")
            ->orWhere('treatment_foreign', 'like', "%" . $term . "%");
    }
}
