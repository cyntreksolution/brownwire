<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
   		'id',
        'location_id',
        'location_name',
        'is_active',
        'space_id'
        ];
    //

        public function space(){
        	return $this->belongsTo(Space::class);
        }
}
