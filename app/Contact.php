<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'service',
        'status',
        'message'
    ];

    public function services(){
        return $this->belongsTo(Service::class,'service')->withTrashed();
    }
}
