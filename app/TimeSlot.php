<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimeSlot extends Model
{
    use SoftDeletes;
    protected $fillable = [
   		'id',
        'time_slot_day',
        'start_time',
        'end_time',
        'is_week_day',
        'is_after_office',
        ];
    //
}
