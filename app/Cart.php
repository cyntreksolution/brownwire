<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'category_id',
        'service_id',
        'itemtype_id',
        'issue_id',
        'isNeedItem',
        'item_id',
        'selectedDate',
        'timeslot_id',
        'instruction',
        'directions_for_technician',
        'status',
        'inspection_charge',
        'service_charge',
        'material_charge',
        'estimate',
        'job_id'
    ];

    public function item_mapping(){
        return $this->belongsTo(Item_Mapping::class)->withTrashed();
    }

    public function user(){
        return $this->belongsTo(User::class)->withTrashed();
    }
    public function item(){
        return $this->belongsTo(Item::class)->withTrashed();
    }
    public function category(){
        return $this->belongsTo(Category::class)->withTrashed();
    }
    public function service(){
        return $this->belongsTo(Service::class)->withTrashed();
    }
    public function timeslot(){
        return $this->belongsTo(TimeSlot::class)->withTrashed();
    }
    public function itemtype(){
        return $this->belongsTo(Itemtype::class)->withTrashed();
    }
    public function issue(){
        return $this->belongsTo(Issue::class)->withTrashed();
    }
    public function address(){
        return $this->belongsTo(Address::class)->withTrashed();
    }
}
