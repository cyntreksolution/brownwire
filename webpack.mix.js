const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .sass('resources/sass/main.scss', 'public/css');

mix.copyDirectory('node_modules/bootstrap/dist','public/plugins/bootstrap');
mix.copy('node_modules/jquery/dist/jquery.min.js','public/plugins/jquery');
mix.copy('node_modules/jquery/dist/jquery.min.map','public/plugins/jquery');
mix.copy('node_modules/popper.js/dist/umd/popper.min.js', 'public/plugins/popper');
mix.copy('node_modules/popper.js/dist/umd/popper.min.js.map', 'public/plugins/popper');
mix.copyDirectory('node_modules/font-awesome','public/plugins/font-awesome');
mix.copyDirectory('resources/fonts/','public/fonts');
mix.copyDirectory('resources/images','public/images');

mix.js('resources/js/admin/app.js', 'public/js/admin')
   .sass('resources/sass/admin/app.scss', 'public/css/admin');